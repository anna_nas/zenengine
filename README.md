## Welcome to the ICT397 Game Engine Project Repo

Here are some guidelines for the repo:

1. Branch from master when creating new code.
2. Make sure you commit changes to your branch regularly.
3. Don't merge your branch into master! (pull master into your branch first and resolve any conflicts)

---

Remember to follow the style guide.

---

##Log any issues to fix here:

1. There is no game engine