/**
 * @file	setup.h
 * @author	Sava Markovic
 * @version	1.0
 * @brief Initial setup tools.
 */

#ifndef	MATH_INCLUDE_CORE_SETUP_H_
#define MATH_INCLUDE_CORE_SETUP_H_

/**
 * @brief Length type.
 * All length functions return a length_t type.
 */
typedef int length_t;

/**
 * @brief Unsigned int type.
 */
typedef unsigned int uint;

#endif // MATH_INCLUDE_CORE_SETUP_H_