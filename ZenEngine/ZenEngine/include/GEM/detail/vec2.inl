namespace gem
{
	// Implicit Basic Constructors

	template<typename T>
	inline constexpr vec<2, T>::vec()
		: x(static_cast<T>(0)), y(static_cast<T>(0))
	{}

	template<typename T>
	inline constexpr vec<2, T>::vec(vec<2, T> const& v)
		: x(v.x), y(v.y)
	{}

	// Explicit Basic Constructors

	template<typename T>
	inline constexpr vec<2, T>::vec(T scalar)
		: x(scalar), y(scalar)
	{}

	template<typename T>
	inline constexpr vec<2, T>::vec(T _x, T _y)
		: x(_x), y(_y)
	{}

	// Scalar Conversion Constructors

	template<typename T>
	template<typename X, typename Y>
	inline constexpr vec<2, T>::vec(X _x, Y _y)
		: x(static_cast<T>(_x))
		, y(static_cast<T>(_y))
	{}

	// Vector Conversion Constructors

	template<typename T>
	template<typename U>
	inline constexpr vec<2, T>::vec(vec<2, U> const& v)
		: x(static_cast<T>(v.x))
		, y(static_cast<T>(v.y))
	{}

	template<typename T>
	template<typename U>
	inline constexpr vec<2, T>::vec(vec<3, U> const& v)
		: x(static_cast<T>(v.x))
		, y(static_cast<T>(v.y))
	{}

	template<typename T>
	template<typename U>
	inline constexpr vec<2, T>::vec(vec<4, U> const& v)
		: x(static_cast<T>(v.x))
		, y(static_cast<T>(v.y))
	{}

	// Component Access

	template<typename T>
	inline T& vec<2, T>::operator[](typename vec<2, T>::length_type i)
	{
		assert(i >= 0 && i < this->length());
		return (&x)[i];
	}

	template<typename T>
	inline const T& vec<2, T>::operator[](typename vec<2, T>::length_type i) const
	{
		assert(i >= 0 && i < this->length());
		return (&x)[i];
	}

	// Unary Arithmetic Operators

	template<typename T>
	inline vec<2, T>& vec<2, T>::operator=(vec<2, T> const& v)
	{
		this->x = v.x;
		this->y = v.y;
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator=(vec<2, U> const& v)
	{
		this->x = static_cast<T>(v.x);
		this->y = static_cast<T>(v.y);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator+=(U scalar)
	{
		this->x += static_cast<T>(scalar);
		this->y += static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator+=(vec<2, U> const& v)
	{
		this->x += static_cast<T>(v.x);
		this->y += static_cast<T>(v.y);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator-=(U scalar)
	{
		this->x -= static_cast<T>(scalar);
		this->y -= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator-=(vec<2, U> const& v)
	{
		this->x -= static_cast<T>(v.x);
		this->y -= static_cast<T>(v.y);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator*=(U scalar)
	{
		this->x *= static_cast<T>(scalar);
		this->y *= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator*=(vec<2, U> const& v)
	{
		this->x *= static_cast<T>(v.x);
		this->y *= static_cast<T>(v.y);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator/=(U scalar)
	{
		this->x /= static_cast<T>(scalar);
		this->y /= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator/=(vec<2, U> const& v)
	{
		this->x /= static_cast<T>(v.x);
		this->y /= static_cast<T>(v.y);
		return *this;
	}

	// Increment and Decrement Operators --

	template<typename T>
	inline vec<2, T>& vec<2, T>::operator++()
	{
		++this->x;
		++this->y;
		return *this;
	}

	template<typename T>
	inline vec<2, T>& vec<2, T>::operator--()
	{
		--this->x;
		--this->y;
		return *this;
	}

	template<typename T>
	inline vec<2, T> vec<2, T>::operator++(int)
	{
		vec<2, T> result(*this);
		++*this;
		return result;
	}

	template<typename T>
	inline vec<2, T> vec<2, T>::operator--(int)
	{
		vec<2, T> result(*this);
		--*this;
		return result;
	}

	// Unary Bitwise Operators

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator%=(U scalar)
	{
		this->x %= static_cast<T>(scalar);
		this->y %= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator%=(vec<2, U> const& v)
	{
		this->x %= static_cast<T>(v.x);
		this->y %= static_cast<T>(v.y);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator&=(U scalar)
	{
		this->x &= static_cast<T>(scalar);
		this->y &= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator&=(vec<2, U> const& v)
	{
		this->x &= static_cast<T>(v.x);
		this->y &= static_cast<T>(v.y);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator|=(U scalar)
	{
		this->x |= static_cast<T>(scalar);
		this->y |= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator|=(vec<2, U> const& v)
	{
		this->x |= static_cast<T>(v.x);
		this->y |= static_cast<T>(v.y);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator^=(U scalar)
	{
		this->x ^= static_cast<T>(scalar);
		this->y ^= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator^=(vec<2, U> const& v)
	{
		this->x ^= static_cast<T>(v.x);
		this->y ^= static_cast<T>(v.y);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator<<=(U scalar)
	{
		this->x <<= static_cast<T>(scalar);
		this->y <<= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator<<=(vec<2, U> const& v)
	{
		this->x <<= static_cast<T>(v.x);
		this->y <<= static_cast<T>(v.y);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator>>=(U scalar)
	{
		this->x >>= static_cast<T>(scalar);
		this->y >>= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<2, T>& vec<2, T>::operator>>=(vec<2, U> const& v)
	{
		this->x >>= static_cast<T>(v.x);
		this->y >>= static_cast<T>(v.y);
		return *this;
	}

	// Unary Operators

	template<typename T>
	inline vec<2, T> operator+(vec<2, T> const& v)
	{
		return v;
	}

	template<typename T>
	inline vec<2, T> operator-(vec<2, T> const& v)
	{
		return vec<2, T>(
			-v.x,
			-v.y);
	}

	// Binary Operators 

	template<typename T>
	inline vec<2, T> operator+(vec<2, T> const& v, T scalar)
	{
		return vec<2, T>(
			v.x + scalar,
			v.y + scalar);
	}

	template<typename T>
	inline vec<2, T> operator+(T scalar, vec<2, T> const& v)
	{
		return vec<2, T>(
			scalar + v.x,
			scalar + v.y);
	}

	template<typename T>
	inline vec<2, T> operator+(vec<2, T> const& v1, vec<2, T> const& v2)
	{
		return vec<2, T>(
			v1.x + v2.x,
			v1.y + v2.y);
	}

	template<typename T>
	inline vec<2, T> operator-(vec<2, T> const& v, T scalar)
	{
		return vec<2, T>(
			v.x - scalar,
			v.y - scalar);
	}

	template<typename T>
	inline vec<2, T> operator-(T scalar, vec<2, T> const& v)
	{
		return vec<2, T>(
			scalar - v.x,
			scalar - v.y);
	}

	template<typename T>
	inline vec<2, T> operator-(vec<2, T> const& v1, vec<2, T> const& v2)
	{
		return vec<2, T>(
			v1.x - v2.x,
			v1.y - v2.y);
	}

	template<typename T>
	inline vec<2, T> operator*(vec<2, T> const& v, T scalar)
	{
		return vec<2, T>(
			v.x * scalar,
			v.y * scalar);
	}

	template<typename T>
	inline vec<2, T> operator*(T scalar, vec<2, T> const& v)
	{
		return vec<2, T>(
			scalar * v.x,
			scalar * v.y);
	}

	template<typename T>
	inline vec<2, T> operator*(vec<2, T> const& v1, vec<2, T> const& v2)
	{
		return vec<2, T>(
			v1.x * v2.x,
			v1.y * v2.y);
	}

	template<typename T>
	inline vec<2, T> operator/(vec<2, T> const& v, T scalar)
	{
		return vec<2, T>(
			v.x / scalar,
			v.y / scalar);
	}

	template<typename T>
	inline vec<2, T> operator/(T scalar, vec<2, T> const& v)
	{
		return vec<2, T>(
			scalar / v.x,
			scalar / v.y);
	}

	template<typename T>
	inline vec<2, T> operator/(vec<2, T> const& v1, vec<2, T> const& v2)
	{
		return vec<2, T>(
			v1.x / v2.x,
			v1.y / v2.y);
	}

	template<typename T>
	inline vec<2, T> operator%(vec<2, T> const& v, T scalar)
	{
		return vec<2, T>(
			v.x % scalar,
			v.y % scalar);
	}

	template<typename T>
	inline vec<2, T> operator%(T scalar, vec<2, T> const& v)
	{
		return vec<2, T>(
			scalar % v.x,
			scalar % v.y);
	}

	template<typename T>
	inline vec<2, T> operator%(vec<2, T> const& v1, vec<2, T> const& v2)
	{
		return vec<2, T>(
			v1.x % v2.x,
			v1.y % v2.y);
	}

	template<typename T>
	inline vec<2, T> operator&(vec<2, T> const& v, T scalar)
	{
		return vec<2, T>(
			v.x & scalar,
			v.y & scalar);
	}

	template<typename T>
	inline vec<2, T> operator&(T scalar, vec<2, T> const& v)
	{
		return vec<2, T>(
			scalar & v.x,
			scalar & v.y);
	}

	template<typename T>
	inline vec<2, T> operator&(vec<2, T> const& v1, vec<2, T> const& v2)
	{
		return vec<2, T>(
			v1.x & v2.x,
			v1.y & v2.y);
	}

	template<typename T>
	inline vec<2, T> operator|(vec<2, T> const& v, T scalar)
	{
		return vec<2, T>(
			v.x | scalar,
			v.y | scalar);
	}

	template<typename T>
	inline vec<2, T> operator|(T scalar, vec<2, T> const& v)
	{
		return vec<2, T>(
			scalar | v.x,
			scalar | v.y);
	}

	template<typename T>
	inline vec<2, T> operator|(vec<2, T> const& v1, vec<2, T> const& v2)
	{
		return vec<2, T>(
			v1.x | v2.x,
			v1.y | v2.y);
	}

	template<typename T>
	inline vec<2, T> operator^(vec<2, T> const& v, T scalar)
	{
		return vec<2, T>(
			v.x ^ scalar,
			v.y ^ scalar);
	}

	template<typename T>
	inline vec<2, T> operator^(T scalar, vec<2, T> const& v)
	{
		return vec<2, T>(
			scalar ^ v.x,
			scalar ^ v.y);
	}

	template<typename T>
	inline vec<2, T> operator^(vec<2, T> const& v1, vec<2, T> const& v2)
	{
		return vec<2, T>(
			v1.x ^ v2.x,
			v1.y ^ v2.y);
	}

	template<typename T>
	inline vec<2, T> operator<<(vec<2, T> const& v, T scalar)
	{
		return vec<2, T>(
			v.x << scalar,
			v.y << scalar);
	}

	template<typename T>
	inline vec<2, T> operator<<(T scalar, vec<2, T> const& v)
	{
		return vec<2, T>(
			scalar << v.x,
			scalar << v.y);
	}

	template<typename T>
	inline vec<2, T> operator<<(vec<2, T> const& v1, vec<2, T> const& v2)
	{
		return vec<2, T>(
			v1.x << v2.x,
			v1.y << v2.y);
	}

	template<typename T>
	inline vec<2, T> operator >> (vec<2, T> const& v, T scalar)
	{
		return vec<2, T>(
			v.x >> scalar,
			v.y >> scalar);
	}

	template<typename T>
	inline vec<2, T> operator >> (T scalar, vec<2, T> const& v)
	{
		return vec<2, T>(
			scalar >> v.x,
			scalar >> v.y);
	}

	template<typename T>
	inline vec<2, T> operator >> (vec<2, T> const& v1, vec<2, T> const& v2)
	{
		return vec<2, T>(
			v1.x >> v2.x,
			v1.y >> v2.y);
	}

	template<typename T>
	inline vec<2, T> operator~(vec<2, T> const& v)
	{
		return vec<2, T>(
			~v.x,
			~v.y);
	}

	// Boolean Operators

	template<typename T>
	inline bool operator==(vec<2, T> const& v1, vec<2, T> const& v2)
	{
		return (v1.x == v2.x) && (v1.y == v2.y);
	}

	template<typename T>
	inline bool operator!=(vec<2, T> const& v1, vec<2, T> const& v2)
	{
		return !(v1 == v2);
	}

	template<typename T>
	inline vec<2, bool> operator&&(vec<2, bool> const& v1, vec<2, bool> const& v2)
	{
		return vec<2, bool>(v1.x && v2.x, v1.y && v2.y);
	}

	template<typename T>
	inline vec<2, bool> operator||(vec<2, bool> const& v1, vec<2, bool> const& v2)
	{
		return vec<2, bool>(v1.x || v2.x, v1.y || v2.y);
	}

} // namespace glm