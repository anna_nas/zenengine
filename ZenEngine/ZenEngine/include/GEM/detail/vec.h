/**
 * @file	vec.h
 * @author	Sava Markovic
 * @version	1.0
 * @brief Templated vector type declaration.
 */

#ifndef MATH_INCLUDE_CORE_VEC_H_
#define MATH_INCLUDE_CORE_VEC_H_

#include "setup.h"

namespace gem 
{
	template<length_t L, typename T> struct vec;

	/**
	 * @brief 2 component vector alias template.
	 * @note Specializations should be prefered.
	 */
	template<typename T> using tvec2 = vec<2, T>;

	/**
	 * @brief 3 component vector alias template.
	 * @note Specializations should be prefered.
	 */
	template<typename T> using tvec3 = vec<3, T>;

	/**
	 * @brief 4 component vector alias template.
	 * @note Specializations should be prefered.
	 */
	template<typename T> using tvec4 = vec<4, T>;

	typedef vec<2, float>	vec2f; // 2 component vector of floating-point numbers. 
	typedef vec<2, double>	vec2d; // 2 component vector of double-precision floating-point numbers. 
	typedef vec<2, int>		vec2i; // 2 component vector of integer numbers. 
	typedef vec<2, uint>	vec2u; // 2 component vector of unisgned integer numbers. 
	typedef vec<2, bool>	vec2b; // 2 component vector of boolean. 

	typedef vec<3, float>	vec3f; // 3 component vector of floating-point numbers. 
	typedef vec<3, double>	vec3d; // 3 component vector of double-precision floating-point numbers. 
	typedef vec<3, int>		vec3i; // 3 component vector of integer numbers. 
	typedef vec<3, uint>	vec3u; // 3 component vector of unisgned integer numbers.
	typedef vec<3, bool>	vec3b; // 3 component vector of boolean. 

	typedef vec<4, float>	vec4f; // 4 component vector of floating-point numbers. 
	typedef vec<4, double>	vec4d; // 4 component vector of double-precision floating-point numbers. 
	typedef vec<4, int>		vec4i; // 4 component vector of integer numbers. 
	typedef vec<4, uint>	vec4u; // 4 component vector of unisgned integer numbers. 
	typedef vec<4, bool>	vec4b; // 4 component vector of boolean. 

} // namespace gem

#endif // MATH_SRC_CORE_VEC_H_