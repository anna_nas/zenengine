#ifndef MATH_INCLUDE_CORE_VECFUNC_H_
#define MATH_INCLUDE_CORE_VECFUNC_H_

#include <cmath>
#include <limits>

#include "setup.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"

namespace gem 
{
	/**
	 * @brief Returns the magnitude of v.
	 *
	 * @tparam L an integer between 1 and 4 inclusive that qualifies the dimensions of the vector.
	 * @tparam T a floating-point scalar type.
	 */
	template<length_t L, typename T>
	T magnitude(vec<L, T> const& v);

	/**
	 * @brief Returns the magnitude squared of v.
	 *
	 * This function can be used for more efficient magnitude comparisons.
	 * @tparam L an integer between 1 and 4 inclusive that qualifies the dimensions of the vector.
	 * @tparam T a floating-point scalar type.
	 */
	template<length_t L, typename T>
	T magnitudeSq(vec<L, T> const& v);

	/**
	 * @brief Returns the distance between p0 and p1.
	 *
	 * @tparam L an integer between 1 and 4 inclusive that qualifies the dimensions of the vector.
	 * @tparam T a floating-point scalar type.
	 */
	template<length_t L, typename T>
	T distance(vec<L, T> const& p0, vec<L, T> const& p1);

	/**
	 * @brief Returns the distance squared between p0 and p1.
	 * This function can be used for more efficient distance comparisons.
	 *
	 * @tparam L an integer between 1 and 4 inclusive that qualifies the dimensions of the vector.
	 * @tparam T a floating-point scalar type.
	 */
	template<length_t L, typename T>
	T distanceSq(vec<L, T> const& p0, vec<L, T> const& p1);

	/**
	 * @brief Returns the dot product of v1 and v2 (v1 * v2).
	 *
	 * @tparam L an integer between 1 and 4 inclusive that qualifies the dimensions of the vector.
	 * @tparam T a floating-point scalar type.
	 */
	template<length_t L, typename T>
	T dot(vec<L, T> const& v1, vec<L, T> const& v2);

	/**
	 * @brief Returns the cross product of v1 and v2 (v1 x v2).
	 *
	 * @tparam T a floating-point scalar type.
	 */
	template<typename T>
	vec<3, T> cross(vec<3, T> const& v1, vec<3, T> const& v2);

	/**
	 * @brief Returns a vector with length 1 in the same direction as v.
	 *
	 * @tparam L an integer between 1 and 4 inclusive that qualifies the dimensions of the vector.
	 * @tparam T a floating-point scalar type.
	 */
	template<length_t L, typename T>
	vec<L, T> normalize(vec<L, T> const& v);

	/**
	 * @brief Returns a vector oriented to point away from a surface defined by its normal Nref.
	 * If dot(Nref, I) < 0.0, return N, otherwise, return -N.
	 *
	 * @tparam L an integer between 1 and 4 inclusive that qualifies the dimensions of the vector.
	 * @tparam T a floating-point scalar type.
	 *
	 * @param N the vector to orient.
	 * @param I the incident vector.
	 * @param Nref the reference vector.
	 */
	template<length_t L, typename T>
	vec<L, T> faceforward(
		vec<L, T> const& N,
		vec<L, T> const& I,
		vec<L, T> const& Nref);

	/**
	 * @brief Returns the reflection direction for a given incident vector I and surface normal N.
	 * Calculated by I - 2.0 * dot(N, I) * N.
	 *
	 * @pre N should be normalized for expected results.
	 * @tparam L an integer between 1 and 4 inclusive that qualifies the dimensions of the vector.
	 * @tparam T a floating-point scalar type.
	 *
	 * @param I the incident vector.
	 * @param N the normal vector.
	 */
	template<length_t L, typename T>
	vec<L, T> reflect(
		vec<L, T> const& I,
		vec<L, T> const& N);

	/**
	 * @brief Returns the refraction vector for a given incident vector I, surface normal N, 
	 * and ratio of indices of refraction, eta.
	 *
	 * @tparam L an integer between 1 and 4 inclusive that qualifies the dimensions of the vector.
	 * @tparam T a floating-point scalar type.
	 *
	 * @param I the incident vector.
	 * @param N the normal vector.
	 * @param eta 
	 */
	template<length_t L, typename T>
	vec<L, T> refract(
		vec<L, T> const& I,
		vec<L, T> const& N,
		T eta);

} // namespace gem

#include "vecFunc.inl"

#endif // MATH_INCLUDE_CORE_VECFUNC_H_