//Changed the implimentation of:
//	inline static vec<3, T> cross(vec<3, T> const& v1, vec<3, T> const& v2)
//	
//	FROM:
//		return vec<3, T>(
//			v1.y * v2.z + v2.y * v1.z,
//			v1.x * v2.z + v2.x * v1.z,
//			v1.x * v2.y + v2.x * v1.y);
//
//	TO:
//		return vec<3, T>(
//			v1.y * v2.z - v2.y * v1.z,
//			v1.z * v2.x - v2.z * v1.x,
//			v1.x * v2.y - v2.x * v1.y);
//
//	AS the previous formula was incorrect.
//			see: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/cross.xhtml
//		Ben 27/4/18

namespace gem 
{
	namespace detail
	{
		template<typename V, typename T>
		struct calc_dot {};

		template<typename T>
		struct calc_dot<vec<2, T>, T>
		{
			inline static T call(vec<2, T> const& v1, vec<2, T> const& v2)
			{
				vec<2, T> tmp(v1 * v2);
				return tmp.x + tmp.y;
			}
		};

		template<typename T>
		struct calc_dot<vec<3, T>, T>
		{
			inline static T call(vec<3, T> const& v1, vec<3, T> const& v2)
			{
				vec<3, T> tmp(v1 * v2);
				return tmp.x + tmp.y + tmp.z;
			}
		};

		template<typename T>
		struct calc_dot<vec<4, T>, T>
		{
			inline static T call(vec<4, T> const& v1, vec<4, T> const& v2)
			{
				vec<4, T> tmp(v1 * v2);
				return (tmp.x + tmp.y) + (tmp.z + tmp.w);
			}
		};
	} // namespace detail

	template<length_t L, typename T>
	inline static T magnitude(vec<L, T> const& v)
	{
		static_assert(std::numeric_limits<T>::is_iec559, "'magnitude' only accepts floating-point inputs");

		return std::sqrt(dot(v, v));
	};

	template<length_t L, typename T>
	inline static T magnitudeSq(vec<L, T> const& v)
	{
		static_assert(std::numeric_limits<T>::is_iec559, "'magnitudeSq' only accepts floating-point inputs");

		return dot(v, v);
	};

	template<length_t L, typename T>
	inline static T distance(vec<L, T> const& p0, vec<L, T> const& p1)
	{
		static_assert(std::numeric_limits<T>::is_iec559, "'distance' only accepts floating-point inputs");

		return magnitude(p1 - p0);
	};

	template<length_t L, typename T>
	inline static T distanceSq(vec<L, T> const& p0, vec<L, T> const& p1)
	{
		static_assert(std::numeric_limits<T>::is_iec559, "'distanceSq' only accepts floating-point inputs");

		return magnitudeSq(p1 - p0);
	};

	template<length_t L, typename T>
	inline static T dot(vec<L, T> const& v1, vec<L, T> const& v2)
	{
		static_assert(std::numeric_limits<T>::is_iec559, "'dot' only accepts floating-point inputs");

		return detail::calc_dot<vec<L, T>, T>::call(v1, v2);
	}

	template<typename T>
	inline static vec<3, T> cross(vec<3, T> const& v1, vec<3, T> const& v2)
	{
		static_assert(std::numeric_limits<T>::is_iec559, "'cross' only accepts floating-point inputs");

		return vec<3, T>(
			v1.y * v2.z - v2.y * v1.z,
			v1.z * v2.x - v2.z * v1.x,
			v1.x * v2.y - v2.x * v1.y);
	};


	template<length_t L, typename T>
	inline static vec<L, T> normalize(vec<L, T> const& v)
	{
		static_assert(std::numeric_limits<T>::is_iec559, "'normalize' only accepts floating-point inputs");

		return v / magnitude(v);
	};


	template<length_t L, typename T>
	inline static vec<L, T> faceforward(vec<L, T> const& N, vec<L, T> const& I, vec<L, T> const& Nref)
	{
		static_assert(std::numeric_limits<T>::is_iec559, "'faceforward' only accepts floating-point inputs");

		return dot(Nref, I) < static_cast<T>(0) ? N : -N;
	};

	template<length_t L, typename T>
	inline static vec<L, T> reflect(vec<L, T> const& I, vec<L, T> const& N)
	{
		return I - N * dot(N, I) * static_cast<T>(2);
	};

	template<length_t L, typename T>
	inline static vec<L, T> refract(vec<L, T> const& I, vec<L, T> const& N, T eta)
	{
		T const dotVal(dot(N, I));
		T const k(static_cast<T>(1) - eta * eta * (static_cast<T>(1) - dotVal * dotVal));
		return (eta * I - (eta * dotValue + std::sqrt(k)) * N) * static_cast<T>(k >= static_cast<T>(0));
	};

} // namespace gem

