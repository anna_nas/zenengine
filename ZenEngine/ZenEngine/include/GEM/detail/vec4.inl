namespace gem
{
	// Implicit Basic Constructors

	template<typename T>
	inline constexpr vec<4, T>::vec()
		: x(static_cast<T>(0))
		, y(static_cast<T>(0))
		, z(static_cast<T>(0))
		, w(static_cast<T>(0))
	{}

	template<typename T>
	inline constexpr vec<4, T>::vec(vec<4, T> const& v)
		: x(v.x), y(v.y), z(v.z), w(v.w)
	{}

	// Explicit Basic Constructors

	template<typename T>
	inline constexpr vec<4, T>::vec(T scalar)
		: x(scalar), y(scalar), z(scalar), w(scalar)
	{}

	template<typename T>
	inline constexpr vec<4, T>::vec(T _x, T _y, T _z, T _w)
		: x(_x), y(_y), z(_z), w(_w)
	{}

	// Scalar Conversion Constructors

	template<typename T>
	template<typename X, typename Y, typename Z, typename W>
	inline constexpr vec<4, T>::vec(X _x, Y _y, Z _z, W _w)
		: x(static_cast<T>(_x))
		, y(static_cast<T>(_y))
		, z(static_cast<T>(_z))
		, w(static_cast<T>(_w))
	{}

	// Vector Conversion Constructors

	template<typename T>
	template<typename A, typename B, typename C>
	inline constexpr vec<4, T>::vec(vec<2, A> const& _xy, B _z, C _w)
		: x(static_cast<T>(_xy.x))
		, y(static_cast<T>(_xy.y))
		, z(static_cast<T>(_z))
		, w(static_cast<T>(_w))
	{}

	template<typename T>
	template<typename A, typename B, typename C>
	inline constexpr vec<4, T>::vec(A _x, vec<2, B> const& _yz, C _w)
		: x(static_cast<T>(_x))
		, y(static_cast<T>(_yz.x))
		, z(static_cast<T>(_yz.y))
		, w(static_cast<T>(_w))
	{}

	template<typename T>
	template<typename A, typename B, typename C>
	inline constexpr vec<4, T>::vec(A _x, B _y, vec<2, C> const& _zw)
		: x(static_cast<T>(_x))
		, y(static_cast<T>(_y))
		, z(static_cast<T>(_zw.x))
		, w(static_cast<T>(_zw.y))
	{}

	template<typename T>
	template<typename A, typename B>
	inline constexpr vec<4, T>::vec(vec<3, A> const& _xyz, B _w)
		: x(static_cast<T>(_xyz.x))
		, y(static_cast<T>(_xyz.y))
		, z(static_cast<T>(_xyz.z))
		, w(static_cast<T>(_w))
	{}

	template<typename T>
	template<typename A, typename B>
	inline constexpr vec<4, T>::vec(A _x, vec<3, B> const& _yzw)
		: x(static_cast<T>(_x))
		, y(static_cast<T>(_yzw.x))
		, z(static_cast<T>(_yzw.y))
		, w(static_cast<T>(_yzw.z))
	{}

	template<typename T>
	template<typename A, typename B>
	inline constexpr vec<4, T>::vec(vec<2, A> const& _xy, vec<2, B> const& _zw)
		: x(static_cast<T>(_xy.x))
		, y(static_cast<T>(_xy.y))
		, z(static_cast<T>(_zw.x))
		, w(static_cast<T>(_zw.y))
	{}

	template<typename T>
	template<typename U>
	inline constexpr vec<4, T>::vec(vec<4, U> const& v)
		: x(static_cast<T>(v.x))
		, y(static_cast<T>(v.y))
		, z(static_cast<T>(v.z))
		, w(static_cast<T>(v.w))
	{}

	// Component Access

	template<typename T>
	inline T& vec<4, T>::operator[](typename vec<4, T>::length_type i)
	{
		assert(i >= 0 && i < this->length());
		return (&x)[i];
	}

	template<typename T>
	inline const T& vec<4, T>::operator[](typename vec<4, T>::length_type i) const
	{
		assert(i >= 0 && i < this->length());
		return (&x)[i];
	}

	// Unary Arithmetic Operators

	template<typename T>
	inline vec<4, T>& vec<4, T>::operator=(vec<4, T> const& v)
	{
		this->x = v.x;
		this->y = v.y;
		this->z = v.z;
		this->w = v.w;
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator=(vec<4, U> const& v)
	{
		this->x = static_cast<T>(v.x);
		this->y = static_cast<T>(v.y);
		this->z = static_cast<T>(v.z);
		this->w = static_cast<T>(v.w);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator+=(U scalar)
	{
		this->x += static_cast<T>(scalar);
		this->y += static_cast<T>(scalar);
		this->z += static_cast<T>(scalar);
		this->w += static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator+=(vec<4, U> const& v)
	{
		this->x += static_cast<T>(v.x);
		this->y += static_cast<T>(v.y);
		this->z += static_cast<T>(v.z);
		this->w += static_cast<T>(v.w);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator-=(U scalar)
	{
		this->x -= static_cast<T>(scalar);
		this->y -= static_cast<T>(scalar);
		this->z -= static_cast<T>(scalar);
		this->w -= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator-=(vec<4, U> const& v)
	{
		this->x -= static_cast<T>(v.x);
		this->y -= static_cast<T>(v.y);
		this->z -= static_cast<T>(v.z);
		this->w -= static_cast<T>(v.w);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator*=(U scalar)
	{
		this->x *= static_cast<T>(scalar);
		this->y *= static_cast<T>(scalar);
		this->z *= static_cast<T>(scalar);
		this->w *= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator*=(vec<4, U> const& v)
	{
		this->x *= static_cast<T>(v.x);
		this->y *= static_cast<T>(v.y);
		this->z *= static_cast<T>(v.z);
		this->w *= static_cast<T>(v.w);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator/=(U scalar)
	{
		this->x /= static_cast<T>(scalar);
		this->y /= static_cast<T>(scalar);
		this->z /= static_cast<T>(scalar);
		this->w /= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator/=(vec<4, U> const& v)
	{
		this->x /= static_cast<T>(v.x);
		this->y /= static_cast<T>(v.y);
		this->z /= static_cast<T>(v.z);
		this->w /= static_cast<T>(v.w);
		return *this;
	}

	// Increment and Decrement Operators

	template<typename T>
	inline vec<4, T>& vec<4, T>::operator++()
	{
		++this->x;
		++this->y;
		++this->z;
		++this->w;
		return *this;
	}

	template<typename T>
	inline vec<4, T>& vec<4, T>::operator--()
	{
		--this->x;
		--this->y;
		--this->z;
		--this->w;
		return *this;
	}

	template<typename T>
	inline vec<4, T> vec<4, T>::operator++(int)
	{
		vec<4, T> result(*this);
		++*this;
		return result;
	}

	template<typename T>
	inline vec<4, T> vec<4, T>::operator--(int)
	{
		vec<4, T> result(*this);
		--*this;
		return result;
	}

	// Unary Bitwise Operators

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator%=(U scalar)
	{
		this->x %= static_cast<T>(scalar);
		this->y %= static_cast<T>(scalar);
		this->z %= static_cast<T>(scalar);
		this->w %= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator%=(vec<4, U> const& v)
	{
		this->x %= static_cast<T>(v.x);
		this->y %= static_cast<T>(v.y);
		this->z %= static_cast<T>(v.z);
		this->w %= static_cast<T>(v.w);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator&=(U scalar)
	{
		this->x &= static_cast<T>(scalar);
		this->y &= static_cast<T>(scalar);
		this->z &= static_cast<T>(scalar);
		this->w &= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator&=(vec<4, U> const& v)
	{
		this->x &= static_cast<T>(v.x);
		this->y &= static_cast<T>(v.y);
		this->z &= static_cast<T>(v.z);
		this->w &= static_cast<T>(v.w);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator|=(U scalar)
	{
		this->x |= static_cast<T>(scalar);
		this->y |= static_cast<T>(scalar);
		this->z |= static_cast<T>(scalar);
		this->w |= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator|=(vec<4, U> const& v)
	{
		this->x |= static_cast<T>(v.x);
		this->y |= static_cast<T>(v.y);
		this->z |= static_cast<T>(v.z);
		this->w |= static_cast<T>(v.w);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator^=(U scalar)
	{
		this->x ^= static_cast<T>(scalar);
		this->y ^= static_cast<T>(scalar);
		this->z ^= static_cast<T>(scalar);
		this->w ^= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator^=(vec<4, U> const& v)
	{
		this->x ^= static_cast<T>(v.x);
		this->y ^= static_cast<T>(v.y);
		this->z ^= static_cast<T>(v.z);
		this->w ^= static_cast<T>(v.w);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator<<=(U scalar)
	{
		this->x <<= static_cast<T>(scalar);
		this->y <<= static_cast<T>(scalar);
		this->z <<= static_cast<T>(scalar);
		this->w <<= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator<<=(vec<4, U> const& v)
	{
		this->x <<= static_cast<T>(v.x);
		this->y <<= static_cast<T>(v.y);
		this->z <<= static_cast<T>(v.z);
		this->w <<= static_cast<T>(v.w);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator>>=(U scalar)
	{
		this->x >>= static_cast<T>(scalar);
		this->y >>= static_cast<T>(scalar);
		this->z >>= static_cast<T>(scalar);
		this->w >>= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<4, T>& vec<4, T>::operator>>=(vec<4, U> const& v)
	{
		this->x >>= static_cast<T>(v.x);
		this->y >>= static_cast<T>(v.y);
		this->z >>= static_cast<T>(v.z);
		this->w >>= static_cast<T>(v.w);
		return *this;
	}

	// Unary arithmetic operators

	template<typename T>
	inline vec<4, T> operator+(vec<4, T> const& v)
	{
		return v;
	}

	template<typename T>
	inline vec<4, T> operator-(vec<4, T> const& v)
	{
		return vec<4, T>(
			-v.x,
			-v.y,
			-v.z,
			-v.w);
	}

	// Binary Operators

	template<typename T>
	inline vec<4, T> operator+(vec<4, T> const& v, T scalar)
	{
		return vec<4, T>(
			v.x + scalar,
			v.y + scalar,
			v.z + scalar,
			v.w + scalar);
	}

	template<typename T>
	inline vec<4, T> operator+(T scalar, vec<4, T> const& v)
	{
		return vec<4, T>(
			scalar + v.x,
			scalar + v.y,
			scalar + v.z,
			scalar + v.w);
	}

	template<typename T>
	inline vec<4, T> operator+(vec<4, T> const& v1, vec<4, T> const& v2)
	{
		return vec<4, T>(
			v1.x + v2.x,
			v1.y + v2.y,
			v1.z + v2.z,
			v1.w + v2.w);
	}

	template<typename T>
	inline vec<4, T> operator-(vec<4, T> const& v, T scalar)
	{
		return vec<4, T>(
			v.x - scalar,
			v.y - scalar,
			v.z - scalar,
			v.w - scalar);
	}

	template<typename T>
	inline vec<4, T> operator-(T scalar, vec<4, T> const& v)
	{
		return vec<4, T>(
			scalar - v.x,
			scalar - v.y,
			scalar - v.z,
			scalar - v.w);
	}

	template<typename T>
	inline vec<4, T> operator-(vec<4, T> const& v1, vec<4, T> const& v2)
	{
		return vec<4, T>(
			v1.x - v2.x,
			v1.y - v2.y,
			v1.z - v2.z,
			v1.w - v2.w);
	}

	template<typename T>
	inline vec<4, T> operator*(vec<4, T> const& v, T scalar)
	{
		return vec<4, T>(
			v.x * scalar,
			v.y * scalar,
			v.z * scalar,
			v.w * scalar);
	}

	template<typename T>
	inline vec<4, T> operator*(T scalar, vec<4, T> const& v)
	{
		return vec<4, T>(
			scalar * v.x,
			scalar * v.y,
			scalar * v.z,
			scalar * v.w);
	}

	template<typename T>
	inline vec<4, T> operator*(vec<4, T> const& v1, vec<4, T> const& v2)
	{
		return vec<4, T>(
			v1.x * v2.x,
			v1.y * v2.y,
			v1.z * v2.z,
			v1.w * v2.w);
	}

	template<typename T>
	inline vec<4, T> operator/(vec<4, T> const& v, T scalar)
	{
		return vec<4, T>(
			v.x / scalar,
			v.y / scalar,
			v.z / scalar,
			v.w / scalar);
	}

	template<typename T>
	inline vec<4, T> operator/(T scalar, vec<4, T> const& v)
	{
		return vec<4, T>(
			scalar / v.x,
			scalar / v.y,
			scalar / v.z,
			scalar / v.w);
	}

	template<typename T>
	inline vec<4, T> operator/(vec<4, T> const& v1, vec<4, T> const& v2)
	{
		return vec<4, T>(
			v1.x / v2.x,
			v1.y / v2.y,
			v1.z / v2.z,
			v1.w / v2.w);
	}

	template<typename T>
	inline vec<4, T> operator%(vec<4, T> const& v, T scalar)
	{
		return vec<4, T>(
			v.x % scalar,
			v.y % scalar,
			v.z % scalar,
			v.w % scalar);
	}

	template<typename T>
	inline vec<4, T> operator%(T scalar, vec<4, T> const& v)
	{
		return vec<4, T>(
			scalar % v.x,
			scalar % v.y,
			scalar % v.z,
			scalar % v.w);
	}

	template<typename T>
	inline vec<4, T> operator%(vec<4, T> const& v1, vec<4, T> const& v2)
	{
		return vec<4, T>(
			v1.x % v2.x,
			v1.y % v2.y,
			v1.z % v2.z,
			v1.w % v2.w);
	}

	template<typename T>
	inline vec<4, T> operator&(vec<4, T> const& v, T scalar)
	{
		return vec<4, T>(
			v.x & scalar,
			v.y & scalar,
			v.z & scalar,
			v.w & scalar);
	}

	template<typename T>
	inline vec<4, T> operator&(T scalar, vec<4, T> const& v)
	{
		return vec<4, T>(
			scalar & v.x,
			scalar & v.y,
			scalar & v.z,
			scalar & v.w);
	}

	template<typename T>
	inline vec<4, T> operator&(vec<4, T> const& v1, vec<4, T> const& v2)
	{
		return vec<4, T>(
			v1.x & v2.x,
			v1.y & v2.y,
			v1.z & v2.z,
			v1.w & v2.w);
	}

	template<typename T>
	inline vec<4, T> operator|(vec<4, T> const& v, T scalar)
	{
		return vec<4, T>(
			v.x | scalar,
			v.y | scalar,
			v.z | scalar,
			v.w | scalar);
	}

	template<typename T>
	inline vec<4, T> operator|(T scalar, vec<4, T> const& v)
	{
		return vec<4, T>(
			scalar | v.x,
			scalar | v.y,
			scalar | v.z,
			scalar | v.w);
	}

	template<typename T>
	inline vec<4, T> operator|(vec<4, T> const& v1, vec<4, T> const& v2)
	{
		return vec<4, T>(
			v1.x | v2.x,
			v1.y | v2.y,
			v1.z | v2.z,
			v1.w | v2.w);
	}

	template<typename T>
	inline vec<4, T> operator^(vec<4, T> const& v, T scalar)
	{
		return vec<4, T>(
			v.x ^ scalar,
			v.y ^ scalar,
			v.z ^ scalar,
			v.w ^ scalar);
	}

	template<typename T>
	inline vec<4, T> operator^(T scalar, vec<4, T> const& v)
	{
		return vec<4, T>(
			scalar ^ v.x,
			scalar ^ v.y,
			scalar ^ v.z,
			scalar ^ v.w);
	}

	template<typename T>
	inline vec<4, T> operator^(vec<4, T> const& v1, vec<4, T> const& v2)
	{
		return vec<4, T>(
			v1.x ^ v2.x,
			v1.y ^ v2.y,
			v1.z ^ v2.z,
			v1.w ^ v2.w);
	}

	template<typename T>
	inline vec<4, T> operator<<(vec<4, T> const& v, T scalar)
	{
		return vec<4, T>(
			v.x << scalar,
			v.y << scalar,
			v.z << scalar,
			v.w << scalar);
	}

	template<typename T>
	inline vec<4, T> operator<<(T scalar, vec<4, T> const& v)
	{
		return vec<4, T>(
			scalar << v.x,
			scalar << v.y,
			scalar << v.z,
			scalar << v.w);
	}

	template<typename T>
	inline vec<4, T> operator<<(vec<4, T> const& v1, vec<4, T> const& v2)
	{
		return vec<4, T>(
			v1.x << v2.x,
			v1.y << v2.y,
			v1.z << v2.z,
			v1.w << v2.w);
	}

	template<typename T>
	inline vec<4, T> operator >> (vec<4, T> const& v, T scalar)
	{
		return vec<4, T>(
			v.x >> scalar,
			v.y >> scalar,
			v.z >> scalar,
			v.w >> scalar);
	}

	template<typename T>
	inline vec<4, T> operator >> (T scalar, vec<4, T> const& v)
	{
		return vec<4, T>(
			scalar >> v.x,
			scalar >> v.y,
			scalar >> v.z,
			scalar >> v.w);
	}

	template<typename T>
	inline vec<4, T> operator >> (vec<4, T> const& v1, vec<4, T> const& v2)
	{
		return vec<4, T>(
			v1.x >> v2.x,
			v1.y >> v2.y,
			v1.z >> v2.z,
			v1.w >> v2.w);
	}

	template<typename T>
	inline vec<4, T> operator~(vec<4, T> const& v)
	{
		return vec<4, T>(
			~v.x,
			~v.y,
			~v.z,
			~v.w);
	}

	// Boolean Operators

	template<typename T>
	inline bool operator==(vec<4, T> const& v1, vec<4, T> const& v2)
	{
		return (v1.x == v2.x) && (v1.y == v2.y)
			&& (v1.z == v2.z) && (v1.w == v2.w);
	}

	template<typename T>
	inline bool operator!=(vec<4, T> const& v1, vec<4, T> const& v2)
	{
		return !(v1 == v2);
	}

	template<typename T>
	inline vec<4, bool> operator&&(vec<4, bool> const& v1, vec<4, bool> const& v2)
	{
		return vec<4, bool>(
			v1.x && v2.x,
			v1.y && v2.y,
			v1.z && v2.z,
			v1.w && v2.w);
	}

	template<typename T>
	inline vec<4, bool> operator||(vec<4, bool> const& v1, vec<4, bool> const& v2)
	{
		return vec<4, bool>(
			v1.x || v2.x,
			v1.y || v2.y,
			v1.z || v2.z,
			v1.w || v2.w);
	}

} // namespace glm
