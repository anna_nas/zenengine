namespace gem
{
	// Implicit Basic Constructors 

	template<typename T>
	inline constexpr vec<3, T>::vec()
		: x(static_cast<T>(0)), y(static_cast<T>(0)), z(static_cast<T>(0))
	{}

	template<typename T>
	inline constexpr vec<3, T>::vec(vec<3, T> const& v)
		: x(v.x), y(v.y), z(v.z)
	{}

	// Explicit Basic Constructors 

	template<typename T>
	inline constexpr vec<3, T>::vec(T scalar)
		: x(scalar), y(scalar), z(scalar)
	{}

	template<typename T>
	inline constexpr vec<3, T>::vec(T _x, T _y, T _z)
		: x(_x), y(_y), z(_z)
	{}

	// Scalar Conversion Constructors

	template<typename T>
	template<typename X, typename Y, typename Z>
	inline constexpr vec<3, T>::vec(X _x, Y _y, Z _z)
		: x(static_cast<T>(_x))
		, y(static_cast<T>(_y))
		, z(static_cast<T>(_z))
	{}

	// Vector Conversion Constructors 

	template<typename T>
	template<typename A, typename B>
	inline constexpr vec<3, T>::vec(vec<2, A> const& _xy, B _z)
		: x(static_cast<T>(_xy.x))
		, y(static_cast<T>(_xy.y))
		, z(static_cast<T>(_z))
	{}

	template<typename T>
	template<typename A, typename B>
	inline constexpr vec<3, T>::vec(A _x, vec<2, B> const& _yz)
		: x(static_cast<T>(_x))
		, y(static_cast<T>(_yz.x))
		, z(static_cast<T>(_yz.y))
	{}

	template<typename T>
	template<typename U>
	inline constexpr vec<3, T>::vec(vec<3, U> const& v)
		: x(static_cast<T>(v.x))
		, y(static_cast<T>(v.y))
		, z(static_cast<T>(v.z))
	{}

	template<typename T>
	template<typename U>
	inline constexpr vec<3, T>::vec(vec<4, U> const& v)
		: x(static_cast<T>(v.x))
		, y(static_cast<T>(v.y))
		, z(static_cast<T>(v.z))
	{}

	// Component Access

	template<typename T>
	inline T& vec<3, T>::operator[](typename vec<3, T>::length_type i)
	{
		assert(i >= 0 && i < this->length());
		return (&x)[i];
	}

	template<typename T>
	inline const T& vec<3, T>::operator[](typename vec<3, T>::length_type i) const
	{
		assert(i >= 0 && i < this->length());
		return (&x)[i];
	}

	// Unary Arithmetic Operators

	template<typename T>
	inline vec<3, T>& vec<3, T>::operator=(vec<3, T> const& v)
	{
		this->x = v.x;
		this->y = v.y;
		this->z = v.z;
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator=(vec<3, U> const& v)
	{
		this->x = static_cast<T>(v.x);
		this->y = static_cast<T>(v.y);
		this->z = static_cast<T>(v.z);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator+=(U scalar)
	{
		this->x += static_cast<T>(scalar);
		this->y += static_cast<T>(scalar);
		this->z += static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator+=(vec<3, U> const& v)
	{
		this->x += static_cast<T>(v.x);
		this->y += static_cast<T>(v.y);
		this->z += static_cast<T>(v.z);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator-=(U scalar)
	{
		this->x -= static_cast<T>(scalar);
		this->y -= static_cast<T>(scalar);
		this->z -= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator-=(vec<3, U> const& v)
	{
		this->x -= static_cast<T>(v.x);
		this->y -= static_cast<T>(v.y);
		this->z -= static_cast<T>(v.z);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator*=(U scalar)
	{
		this->x *= static_cast<T>(scalar);
		this->y *= static_cast<T>(scalar);
		this->z *= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator*=(vec<3, U> const& v)
	{
		this->x *= static_cast<T>(v.x);
		this->y *= static_cast<T>(v.y);
		this->z *= static_cast<T>(v.z);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator/=(U scalar)
	{
		this->x /= static_cast<T>(scalar);
		this->y /= static_cast<T>(scalar);
		this->z /= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator/=(vec<3, U> const& v)
	{
		this->x /= static_cast<T>(v.x);
		this->y /= static_cast<T>(v.y);
		this->z /= static_cast<T>(v.z);
		return *this;
	}

	// Increment and Decrement Operators 

	template<typename T>
	inline vec<3, T>& vec<3, T>::operator++()
	{
		++this->x;
		++this->y;
		++this->z;
		return *this;
	}

	template<typename T>
	inline vec<3, T>& vec<3, T>::operator--()
	{
		--this->x;
		--this->y;
		--this->z;
		return *this;
	}

	template<typename T>
	inline vec<3, T> vec<3, T>::operator++(int)
	{
		vec<3, T> result(*this);
		++*this;
		return result;
	}

	template<typename T>
	inline vec<3, T> vec<3, T>::operator--(int)
	{
		vec<3, T> result(*this);
		--*this;
		return result;
	}

	// Unary Bitwise Operators 

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator%=(U scalar)
	{
		this->x %= static_cast<T>(scalar);
		this->y %= static_cast<T>(scalar);
		this->z %= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator%=(vec<3, U> const& v)
	{
		this->x %= static_cast<T>(v.x);
		this->y %= static_cast<T>(v.y);
		this->z %= static_cast<T>(v.z);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator&=(U scalar)
	{
		this->x &= static_cast<T>(scalar);
		this->y &= static_cast<T>(scalar);
		this->z &= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator&=(vec<3, U> const& v)
	{
		this->x &= static_cast<T>(v.x);
		this->y &= static_cast<T>(v.y);
		this->z &= static_cast<T>(v.z);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator|=(U scalar)
	{
		this->x |= static_cast<T>(scalar);
		this->y |= static_cast<T>(scalar);
		this->z |= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator|=(vec<3, U> const& v)
	{
		this->x |= static_cast<T>(v.x);
		this->y |= static_cast<T>(v.y);
		this->z |= static_cast<T>(v.z);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator^=(U scalar)
	{
		this->x ^= static_cast<T>(scalar);
		this->y ^= static_cast<T>(scalar);
		this->z ^= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator^=(vec<3, U> const& v)
	{
		this->x ^= static_cast<T>(v.x);
		this->y ^= static_cast<T>(v.y);
		this->z ^= static_cast<T>(v.z);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator<<=(U scalar)
	{
		this->x <<= static_cast<T>(scalar);
		this->y <<= static_cast<T>(scalar);
		this->z <<= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator<<=(vec<3, U> const& v)
	{
		this->x <<= static_cast<T>(v.x);
		this->y <<= static_cast<T>(v.y);
		this->z <<= static_cast<T>(v.z);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator>>=(U scalar)
	{
		this->x >>= static_cast<T>(scalar);
		this->y >>= static_cast<T>(scalar);
		this->z >>= static_cast<T>(scalar);
		return *this;
	}

	template<typename T>
	template<typename U>
	inline vec<3, T>& vec<3, T>::operator>>=(vec<3, U> const& v)
	{
		this->x >>= static_cast<T>(v.x);
		this->y >>= static_cast<T>(v.y);
		this->z >>= static_cast<T>(v.z);
		return *this;
	}

	// Unary Operators 

	template<typename T>
	inline vec<3, T> operator+(vec<3, T> const& v)
	{
		return v;
	}

	template<typename T>
	inline vec<3, T> operator-(vec<3, T> const& v)
	{
		return vec<3, T>(
			-v.x,
			-v.y,
			-v.z);
	}

	// Binary Operators 

	template<typename T>
	inline vec<3, T> operator+(vec<3, T> const& v, T scalar)
	{
		return vec<3, T>(
			v.x + scalar,
			v.y + scalar,
			v.z + scalar);
	}

	template<typename T>
	inline vec<3, T> operator+(T scalar, vec<3, T> const& v)
	{
		return vec<3, T>(
			scalar + v.x,
			scalar + v.y,
			scalar + v.z);
	}

	template<typename T>
	inline vec<3, T> operator+(vec<3, T> const& v1, vec<3, T> const& v2)
	{
		return vec<3, T>(
			v1.x + v2.x,
			v1.y + v2.y,
			v1.z + v2.z);
	}

	template<typename T>
	inline vec<3, T> operator-(vec<3, T> const& v, T scalar)
	{
		return vec<3, T>(
			v.x - scalar,
			v.y - scalar,
			v.z - scalar);
	}

	template<typename T>
	inline vec<3, T> operator-(T scalar, vec<3, T> const& v)
	{
		return vec<3, T>(
			scalar - v.x,
			scalar - v.y,
			scalar - v.z);
	}

	template<typename T>
	inline vec<3, T> operator-(vec<3, T> const& v1, vec<3, T> const& v2)
	{
		return vec<3, T>(
			v1.x - v2.x,
			v1.y - v2.y,
			v1.z - v2.z);
	}

	template<typename T>
	inline vec<3, T> operator*(vec<3, T> const& v, T scalar)
	{
		return vec<3, T>(
			v.x * scalar,
			v.y * scalar,
			v.z * scalar);
	}

	template<typename T>
	inline vec<3, T> operator*(T scalar, vec<3, T> const& v)
	{
		return vec<3, T>(
			scalar * v.x,
			scalar * v.y,
			scalar * v.z);
	}

	template<typename T>
	inline vec<3, T> operator*(vec<3, T> const& v1, vec<3, T> const& v2)
	{
		return vec<3, T>(
			v1.x * v2.x,
			v1.y * v2.y,
			v1.z * v2.z);
	}

	template<typename T>
	inline vec<3, T> operator/(vec<3, T> const& v, T scalar)
	{
		return vec<3, T>(
			v.x / scalar,
			v.y / scalar,
			v.z / scalar);
	}

	template<typename T>
	inline vec<3, T> operator/(T scalar, vec<3, T> const& v)
	{
		return vec<3, T>(
			scalar / v.x,
			scalar / v.y,
			scalar / v.z);
	}

	template<typename T>
	inline vec<3, T> operator/(vec<3, T> const& v1, vec<3, T> const& v2)
	{
		return vec<3, T>(
			v1.x / v2.x,
			v1.y / v2.y,
			v1.z / v2.z);
	}

	template<typename T>
	inline vec<3, T> operator%(vec<3, T> const& v, T scalar)
	{
		return vec<3, T>(
			v.x % scalar,
			v.y % scalar,
			v.z % scalar);
	}

	template<typename T>
	inline vec<3, T> operator%(T scalar, vec<3, T> const& v)
	{
		return vec<3, T>(
			scalar % v.x,
			scalar % v.y,
			scalar % v.z);
	}

	template<typename T>
	inline vec<3, T> operator%(vec<3, T> const& v1, vec<3, T> const& v2)
	{
		return vec<3, T>(
			v1.x % v2.x,
			v1.y % v2.y,
			v1.z % v2.z);
	}

	template<typename T>
	inline vec<3, T> operator&(vec<3, T> const& v, T scalar)
	{
		return vec<3, T>(
			v.x & scalar,
			v.y & scalar,
			v.z & scalar);
	}

	template<typename T>
	inline vec<3, T> operator&(T scalar, vec<3, T> const& v)
	{
		return vec<3, T>(
			scalar & v.x,
			scalar & v.y,
			scalar & v.z);
	}

	template<typename T>
	inline vec<3, T> operator&(vec<3, T> const& v1, vec<3, T> const& v2)
	{
		return vec<3, T>(
			v1.x & v2.x,
			v1.y & v2.y,
			v1.z & v2.z);
	}

	template<typename T>
	inline vec<3, T> operator|(vec<3, T> const& v, T scalar)
	{
		return vec<3, T>(
			v.x | scalar,
			v.y | scalar,
			v.z | scalar);
	}

	template<typename T>
	inline vec<3, T> operator|(T scalar, vec<3, T> const& v)
	{
		return vec<3, T>(
			scalar | v.x,
			scalar | v.y,
			scalar | v.z);
	}

	template<typename T>
	inline vec<3, T> operator|(vec<3, T> const& v1, vec<3, T> const& v2)
	{
		return vec<3, T>(
			v1.x | v2.x,
			v1.y | v2.y,
			v1.z | v2.z);
	}

	template<typename T>
	inline vec<3, T> operator^(vec<3, T> const& v, T scalar)
	{
		return vec<3, T>(
			v.x ^ scalar,
			v.y ^ scalar,
			v.z ^ scalar);
	}

	template<typename T>
	inline vec<3, T> operator^(T scalar, vec<3, T> const& v)
	{
		return vec<3, T>(
			scalar ^ v.x,
			scalar ^ v.y,
			scalar ^ v.z);
	}

	template<typename T>
	inline vec<3, T> operator^(vec<3, T> const& v1, vec<3, T> const& v2)
	{
		return vec<3, T>(
			v1.x ^ v2.x,
			v1.y ^ v2.y,
			v1.z ^ v2.z);
	}

	template<typename T>
	inline vec<3, T> operator<<(vec<3, T> const& v, T scalar)
	{
		return vec<3, T>(
			v.x << scalar,
			v.y << scalar,
			v.z << scalar);
	}

	template<typename T>
	inline vec<3, T> operator<<(T scalar, vec<3, T> const& v)
	{
		return vec<3, T>(
			scalar << v.x,
			scalar << v.y,
			scalar << v.z);
	}

	template<typename T>
	inline vec<3, T> operator<<(vec<3, T> const& v1, vec<3, T> const& v2)
	{
		return vec<3, T>(
			v1.x << v2.x,
			v1.y << v2.y,
			v1.z << v2.z);
	}

	template<typename T>
	inline vec<3, T> operator >> (vec<3, T> const& v, T scalar)
	{
		return vec<3, T>(
			v.x >> scalar,
			v.y >> scalar,
			v.z >> scalar);
	}

	template<typename T>
	inline vec<3, T> operator >> (T scalar, vec<3, T> const& v)
	{
		return vec<3, T>(
			scalar >> v.x,
			scalar >> v.y,
			scalar >> v.z);
	}

	template<typename T>
	inline vec<3, T> operator >> (vec<3, T> const& v1, vec<3, T> const& v2)
	{
		return vec<3, T>(
			v1.x >> v2.x,
			v1.y >> v2.y,
			v1.z >> v2.z);
	}

	template<typename T>
	inline vec<3, T> operator~(vec<3, T> const& v)
	{
		return vec<3, T>(
			~v.x,
			~v.y,
			~v.z);
	}

	// Boolean Operators

	template<typename T>
	inline bool operator==(vec<3, T> const& v1, vec<3, T> const& v2)
	{
		return (v1.x == v2.x) && (v1.y == v2.y) && (v1.z == v2.z);
	}

	template<typename T>
	inline bool operator!=(vec<3, T> const& v1, vec<3, T> const& v2)
	{
		return !(v1 == v2);
	}

	template<typename T>
	inline vec<3, bool> operator&&(vec<3, bool> const& v1, vec<3, bool> const& v2)
	{
		return vec<3, bool>(v1.x && v2.x, v1.y && v2.y, v1.z && v2.z);
	}

	template<typename T>
	inline vec<3, bool> operator||(vec<3, bool> const& v1, vec<3, bool> const& v2)
	{
		return vec<3, bool>(v1.x || v2.x, v1.y || v2.y, v1.z || v2.z);
	}

} // namespace glm