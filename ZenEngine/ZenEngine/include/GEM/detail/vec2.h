/**
 * @file	vec2.h
 * @author	Sava Markovic
 * @version	1.0
 * @brief 2 component vector template specialization.
 */

#ifndef MATH_INCLUDE_CORE_VEC2_H_
#define MATH_INCLUDE_CORE_VEC2_H_

#include "vec.h"

namespace gem 
{
	template<typename T>
	struct vec<2, T>
	{
		// Implementation Details
		 
		typedef T value_type;			// Vector component type to enable external referencing.
		typedef vec type;				// Vector template specialization type to enable external referencing.
		typedef vec<2, bool> bool_type; // Vector Boolean specialization type to enable external referencing.

		// Data

		T x, y;

		// Component Access

		typedef length_t length_type; // Vector length type to enable external referencing.

		/**
		 * @brief Returns the number of components in the vector.
		 */
		static constexpr length_type length() { return 2; }

		T& operator[](length_type i); 
		const T& operator[](length_type i) const;

		// Implicit Basic Constructors

		constexpr vec();
		constexpr vec(vec const& v);

		// Explicit Basic Constructors

		constexpr explicit vec(T scalar);
		constexpr vec(T _x, T _y);

		// Scalar Conversion Constructors

		template<typename X, typename Y>
		constexpr vec(X _x, Y _y);

		// Vector Conversion Constructors

		template<typename U>
		constexpr explicit vec(vec<2, U> const& v);

		template<typename U>
		constexpr explicit vec(vec<3, U> const& v);

		template<typename U>
		constexpr explicit vec(vec<4, U> const& v);

		// Unary Arithmetic Operators

		vec& operator=(vec const& v);

		template<typename U>
		vec& operator=(vec<2, U> const& v);

		template<typename U>
		vec& operator+=(U scalar);

		template<typename U>
		vec& operator+=(vec<2, U> const& v);

		template<typename U>
		vec& operator-=(U scalar);

		template<typename U>
		vec& operator-=(vec<2, U> const& v);

		template<typename U>
		vec& operator*=(U scalar);

		template<typename U>
		vec& operator*=(vec<2, U> const& v);

		template<typename U>
		vec& operator/=(U scalar);

		template<typename U>
		vec& operator/=(vec<2, U> const& v);

		// Increment and Decrement Operators 

		vec& operator++();

		vec& operator--();

		vec operator++(int);

		vec operator--(int);

		// Unary Bitwise Operators

		template<typename U>
		vec& operator%=(U scalar);

		template<typename U>
		vec& operator%=(vec<2, U> const& v);

		template<typename U>
		vec& operator&=(U scalar);

		template<typename U>
		vec& operator&=(vec<2, U> const& v);

		template<typename U>
		vec& operator|=(U scalar);

		template<typename U>
		vec& operator|=(vec<2, U> const& v);

		template<typename U>
		vec& operator^=(U scalar);

		template<typename U>
		vec& operator^=(vec<2, U> const& v);

		template<typename U>
		vec& operator<<=(U scalar);

		template<typename U>
		vec& operator<<=(vec<2, U> const& v);

		template<typename U>
		vec& operator>>=(U scalar);

		template<typename U>
		vec& operator>>=(vec<2, U> const& v);

	};

	// Unary Operators 

	template<typename T>
	vec<2, T> operator+(vec<2, T> const& v);

	template<typename T>
	vec<2, T> operator-(vec<2, T> const& v);

	// Binary Operators 

	template<typename T>
	vec<2, T> operator+(vec<2, T> const& v, T scalar);

	template<typename T>
	vec<2, T> operator+(T scalar, vec<2, T> const& v);

	template<typename T>
	vec<2, T> operator+(vec<2, T> const& v1, vec<2, T> const& v2);

	template<typename T>
	vec<2, T> operator-(vec<2, T> const& v, T scalar);

	template<typename T>
	vec<2, T> operator-(T scalar, vec<2, T> const& v);

	template<typename T>
	vec<2, T> operator-(vec<2, T> const& v1, vec<2, T> const& v2);

	template<typename T>
	vec<2, T> operator*(vec<2, T> const& v, T scalar);

	template<typename T>
	vec<2, T> operator*(T scalar, vec<2, T> const& v);

	template<typename T>
	vec<2, T> operator*(vec<2, T> const& v1, vec<2, T> const& v2);

	template<typename T>
	vec<2, T> operator/(vec<2, T> const& v, T scalar);

	template<typename T>
	vec<2, T> operator/(T scalar, vec<2, T> const& v);

	template<typename T>
	vec<2, T> operator/(vec<2, T> const& v1, vec<2, T> const& v2);

	template<typename T>
	vec<2, T> operator%(vec<2, T> const& v, T scalar);

	template<typename T>
	vec<2, T> operator%(T scalar, vec<2, T> const& v);

	template<typename T>
	vec<2, T> operator%(vec<2, T> const& v1, vec<2, T> const& v2);

	template<typename T>
	vec<2, T> operator&(vec<2, T> const& v, T scalar);

	template<typename T>
	vec<2, T> operator&(T scalar, vec<2, T> const& v);

	template<typename T>
	vec<2, T> operator&(vec<2, T> const& v1, vec<2, T> const& v2);

	template<typename T>
	vec<2, T> operator|(vec<2, T> const& v, T scalar);

	template<typename T>
	vec<2, T> operator|(T scalar, vec<2, T> const& v);

	template<typename T>
	vec<2, T> operator|(vec<2, T> const& v1, vec<2, T> const& v2);

	template<typename T>
	vec<2, T> operator^(vec<2, T> const& v, T scalar);

	template<typename T>
	vec<2, T> operator^(T scalar, vec<2, T> const& v);

	template<typename T>
	vec<2, T> operator^(vec<2, T> const& v1, vec<2, T> const& v2);

	template<typename T>
	vec<2, T> operator>>(vec<2, T> const& v, T scalar);

	template<typename T>
	vec<2, T> operator>>(T scalar, vec<2, T> const& v);

	template<typename T>
	vec<2, T> operator>>(vec<2, T> const& v1, vec<2, T> const& v2);

	template<typename T>
	vec<2, T> operator<<(vec<2, T> const& v, T scalar);

	template<typename T>
	vec<2, T> operator<<(T scalar, vec<2, T> const& v);

	template<typename T>
	vec<2, T> operator<<(vec<2, T> const& v1, vec<2, T> const& v2);

	template<typename T>
	vec<2, T> operator~(vec<2, T> const& v);

	// Boolean Operators

	template<typename T>
	bool operator==(vec<2, T> const& v1, vec<2, T> const& v2);

	template<typename T>
	bool operator!=(vec<2, T> const& v1, vec<2, T> const& v2);

	template<typename T>
	vec<2, bool> operator&&(vec<2, bool> const& v1, vec<2, bool> const& v2);

	template<typename T>
	vec<2, bool> operator||(vec<2, bool> const& v1, vec<2, bool> const& v2);

} // namespace gem

#include "vec2.inl"

#endif // MATH_INCLUDE_CORE_VEC2_H_
