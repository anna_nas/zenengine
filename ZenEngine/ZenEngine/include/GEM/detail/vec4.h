/**
 * @file	vec4.h
 * @author	Sava Markovic
 * @version	1.0
 * @brief 4 component vector template specialization.
 */

#ifndef MATH_INCLUDE_CORE_VEC4_H_
#define MATH_INCLUDE_CORE_VEC4_H_

#include "vec.h"

namespace gem 
{
	template<typename T>
	struct vec<4, T>
	{
		// Implementation Detail

		typedef T value_type;			// Vector component type to enable external referencing.
		typedef vec type; 				// Vector template specialization type to enable external referencing.
		typedef vec<4, bool> bool_type; // Vector Boolean specialization type to enable external referencing.

		// Data 

		T x, y, z, w;

		// Component Access 

		typedef length_t length_type; // Vector length type to enable external referencing.

		/**
		 * @brief Returns the number of components in the vector.
		 */
		static constexpr length_type length() { return 4; }

		T& operator[](length_type i);
		const T& operator[](length_type i) const;

		// Implicit Basic Constructors

		constexpr vec();
		constexpr vec(vec<4, T> const& v);

		// Explicit Basic Constructors

		constexpr explicit vec(T scalar);
		constexpr vec(T _x, T _y, T _z, T _w);

		// Scalar Conversion Constructors

		template<typename X, typename Y, typename Z, typename W>
		constexpr vec(X _x, Y _y, Z _z, W _w);

		// Vector Conversion Constructors

		template<typename A, typename B, typename C>
		constexpr vec(vec<2, A> const& xy, B z, C w);

		template<typename A, typename B, typename C>
		constexpr vec(A x, vec<2, B> const& yz, C w);

		template<typename A, typename B, typename C>
		constexpr vec(A x, B y, vec<2, C> const& zw);

		template<typename A, typename B>
		constexpr vec(vec<3, A> const& xyz, B w);

		template<typename A, typename B>
		constexpr vec(A x, vec<3, B> const& yzw);

		template<typename A, typename B>
		constexpr vec(vec<2, A> const& xy, vec<2, B> const& zw);

		template<typename U>
		constexpr explicit vec(vec<4, U> const& v);

		// Unary Arithmetic Operators

		vec& operator=(vec const& v);

		template<typename U>
		vec& operator=(vec<4, U> const& v);

		template<typename U>
		vec& operator+=(U scalar);

		template<typename U>
		vec& operator+=(vec<4, U> const& v);

		template<typename U>
		vec& operator-=(U scalar);

		template<typename U>
		vec& operator-=(vec<4, U> const& v);

		template<typename U>
		vec& operator*=(U scalar);

		template<typename U>
		vec& operator*=(vec<4, U> const& v);

		template<typename U>
		vec& operator/=(U scalar);

		template<typename U>
		vec& operator/=(vec<4, U> const& v);

		// Increment and Decrement Operators

		vec& operator++();

		vec& operator--();

		vec operator++(int);

		vec operator--(int);

		// Unary Bitwise Operators

		template<typename U>
		vec& operator%=(U scalar);

		template<typename U>
		vec& operator%=(vec<4, U> const& v);

		template<typename U>
		vec& operator&=(U scalar);

		template<typename U>
		vec& operator&=(vec<4, U> const& v);

		template<typename U>
		vec& operator|=(U scalar);

		template<typename U>
		vec& operator|=(vec<4, U> const& v);

		template<typename U>
		vec& operator^=(U scalar);

		template<typename U>
		vec& operator^=(vec<4, U> const& v);

		template<typename U>
		vec& operator<<=(U scalar);

		template<typename U>
		vec& operator<<=(vec<4, U> const& v);

		template<typename U>
		vec& operator>>=(U scalar);

		template<typename U>
		vec& operator>>=(vec<4, U> const& v);

	};

	// Unary Operators

	template<typename T>
	vec<4, T> operator+(vec<4, T> const& v);

	template<typename T>
	vec<4, T> operator-(vec<4, T> const& v);

	// Binary Operators

	template<typename T>
	vec<4, T> operator+(vec<4, T> const& v, T scalar);

	template<typename T>
	vec<4, T> operator+(T scalar, vec<4, T> const& v);

	template<typename T>
	vec<4, T> operator+(vec<4, T> const& v1, vec<4, T> const& v2);

	template<typename T>
	vec<4, T> operator-(vec<4, T> const& v, T scalar);

	template<typename T>
	vec<4, T> operator-(T scalar, vec<4, T> const& v);

	template<typename T>
	vec<4, T> operator-(vec<4, T> const& v1, vec<4, T> const& v2);

	template<typename T>
	vec<4, T> operator*(vec<4, T> const& v, T scalar);

	template<typename T>
	vec<4, T> operator*(T scalar, vec<4, T> const& v);

	template<typename T>
	vec<4, T> operator*(vec<4, T> const& v1, vec<4, T> const& v2);

	template<typename T>
	vec<4, T> operator/(vec<4, T> const& v, T scalar);

	template<typename T>
	vec<4, T> operator/(T scalar, vec<4, T> const& v);

	template<typename T>
	vec<4, T> operator/(vec<4, T> const& v1, vec<4, T> const& v2);

	template<typename T>
	vec<4, T> operator%(vec<4, T> const& v, T scalar);

	template<typename T>
	vec<4, T> operator%(T scalar, vec<4, T> const& v);

	template<typename T>
	vec<4, T> operator%(vec<4, T> const& v1, vec<4, T> const& v2);

	template<typename T>
	vec<4, T> operator&(vec<4, T> const& v, T scalar);

	template<typename T>
	vec<4, T> operator&(T scalar, vec<4, T> const& v);

	template<typename T>
	vec<4, T> operator&(vec<4, T> const& v1, vec<4, T> const& v2);

	template<typename T>
	vec<4, T> operator|(vec<4, T> const& v, T scalar);

	template<typename T>
	vec<4, T> operator|(T scalar, vec<4, T> const& v);

	template<typename T>
	vec<4, T> operator|(vec<4, T> const& v1, vec<4, T> const& v2);

	template<typename T>
	vec<4, T> operator^(vec<4, T> const& v, T scalar);

	template<typename T>
	vec<4, T> operator^(T scalar, vec<4, T> const& v);

	template<typename T>
	vec<4, T> operator^(vec<4, T> const& v1, vec<4, T> const& v2);

	template<typename T>
	vec<4, T> operator>>(vec<4, T> const& v, T scalar);

	template<typename T>
	vec<4, T> operator>>(T scalar, vec<4, T> const& v);

	template<typename T>
	vec<4, T> operator>>(vec<4, T> const& v1, vec<4, T> const& v2);

	template<typename T>
	vec<4, T> operator<<(vec<4, T> const& v, T scalar);

	template<typename T>
	vec<4, T> operator<<(T scalar, vec<4, T> const& v);

	template<typename T>
	vec<4, T> operator<<(vec<4, T> const& v1, vec<4, T> const& v2);

	template<typename T>
	vec<4, T> operator~(vec<4, T> const& v);

	// Boolean Operators

	template<typename T>
	bool operator==(vec<4, T> const& v1, vec<4, T> const& v2);

	template<typename T>
	bool operator!=(vec<4, T> const& v1, vec<4, T> const& v2);

	template<typename T>
	vec<4, bool> operator&&(vec<4, bool> const& v1, vec<4, bool> const& v2);

	template<typename T>
	vec<4, bool> operator||(vec<4, bool> const& v1, vec<4, bool> const& v2);

} // namespace gem

#include "vec4.inl"

#endif // MATH_INCLUDE_CORE_VEC4_H_


