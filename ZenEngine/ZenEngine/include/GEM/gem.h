#ifndef	MATH_SRC_GEM_H_
#define MATH_SRC_GEM_H_

#include "detail\setup.h"

#include "detail\vec2.h"
#include "detail\vec3.h"
#include "detail\vec4.h"
#include "detail\vecFunc.h"

#endif // MATH_SRC_GEM_H_