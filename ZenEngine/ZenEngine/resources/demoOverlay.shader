#shader vertex
#version 330 core

layout(location = 0) in vec3 VertexPosition_ModelSpace;
layout(location = 1) in vec2 VertexUV;

out vec2 v_VertexUV;

void main() {
    gl_Position = vec4(VertexPosition_ModelSpace, 1.0f);
    v_VertexUV = VertexUV;
}

#shader fragment
#version 330 core

in vec2 v_VertexUV;
out vec4 color;
uniform sampler2D u_Texture0;

void main() {
    color = vec4(texture(u_Texture0, v_VertexUV).rgb, 1.0f);
}