--terrain.lua
Type = "Terrain";
SubType = "FaultLine";
Texture = "Texture resources/terrain/gravel.jpg resources/terrain/grass.jpg resources/terrain/inconsistent_grass.jpg resources/terrain/leaves.jpg resources/terrain/marble.jpg resources/terrain/granite.jpg"
Position = "0 0 0";
Rotation = "0 0 0";
Scale = "1 1 1";
AI = "None";
