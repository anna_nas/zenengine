--PatrolState.lua
Patrol = State.new();

Patrol.Enter = function(obj)
	obj:SetPatrol(obj:Position(), 5, 100);
end;

Patrol.Execute = function(obj)
	obj:Patrol();
end;

Patrol.Exit = function(obj)

end;

RegisterState("Patrol2", Patrol);