--PatrolState.lua
Patrol = State.new();

Patrol.Enter = function(obj)
	obj:SetPatrol(obj:Position(), -25, 50);
end;

Patrol.Execute = function(obj)
	obj:Patrol();
end;

Patrol.Exit = function(obj)

end;

RegisterState("Patrol1", Patrol);