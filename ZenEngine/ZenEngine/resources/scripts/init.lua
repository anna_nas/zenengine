-- init.lua

--registerAI
RegisterScript("ai", "resources/scripts/PatrolState.lua");
RegisterScript("ai", "resources/scripts/PatrolState1.lua");
RegisterScript("ai", "resources/scripts/PatrolState2.lua");

--register terrain
RegisterScript("object", "resources/scripts/terrain.lua");

--rocks
RegisterScript("object", "resources/scripts/rockA0.lua");
RegisterScript("object", "resources/scripts/rockA1.lua");
RegisterScript("object", "resources/scripts/rockA2.lua");
RegisterScript("object", "resources/scripts/rockA3.lua");
RegisterScript("object", "resources/scripts/rockA4.lua");
RegisterScript("object", "resources/scripts/rockA5.lua");
RegisterScript("object", "resources/scripts/rockA6.lua");
RegisterScript("object", "resources/scripts/rockA7.lua");
RegisterScript("object", "resources/scripts/rockA8.lua");
RegisterScript("object", "resources/scripts/rockA9.lua");
RegisterScript("object", "resources/scripts/rockA10.lua");
RegisterScript("object", "resources/scripts/rockB0.lua");
RegisterScript("object", "resources/scripts/rockB1.lua");
RegisterScript("object", "resources/scripts/rockB2.lua");
RegisterScript("object", "resources/scripts/rockB3.lua");
RegisterScript("object", "resources/scripts/rockB4.lua");
RegisterScript("object", "resources/scripts/rockB5.lua");
RegisterScript("object", "resources/scripts/rockB6.lua");
RegisterScript("object", "resources/scripts/rockB7.lua");
RegisterScript("object", "resources/scripts/rockB8.lua");
RegisterScript("object", "resources/scripts/rockB9.lua");
RegisterScript("object", "resources/scripts/rockB10.lua");

--trees
RegisterScript("object", "resources/scripts/tree0.lua");
RegisterScript("object", "resources/scripts/tree1.lua");
RegisterScript("object", "resources/scripts/tree2.lua");
RegisterScript("object", "resources/scripts/tree3.lua");
RegisterScript("object", "resources/scripts/tree4.lua");
RegisterScript("object", "resources/scripts/tree5.lua");
RegisterScript("object", "resources/scripts/tree6.lua");
RegisterScript("object", "resources/scripts/tree7.lua");
RegisterScript("object", "resources/scripts/tree8.lua");
RegisterScript("object", "resources/scripts/tree9.lua");
RegisterScript("object", "resources/scripts/tree10.lua");
RegisterScript("object", "resources/scripts/tree11.lua");
RegisterScript("object", "resources/scripts/tree12.lua");
RegisterScript("object", "resources/scripts/tree13.lua");
RegisterScript("object", "resources/scripts/tree14.lua");
RegisterScript("object", "resources/scripts/tree15.lua");
RegisterScript("object", "resources/scripts/tree16.lua");
RegisterScript("object", "resources/scripts/tree17.lua");
RegisterScript("object", "resources/scripts/tree18.lua");
RegisterScript("object", "resources/scripts/tree19.lua");
RegisterScript("object", "resources/scripts/tree20.lua");
RegisterScript("object", "resources/scripts/tree21.lua");
RegisterScript("object", "resources/scripts/tree22.lua");
RegisterScript("object", "resources/scripts/tree23.lua");
RegisterScript("object", "resources/scripts/tree24.lua");
RegisterScript("object", "resources/scripts/tree25.lua");
RegisterScript("object", "resources/scripts/tree26.lua");
RegisterScript("object", "resources/scripts/tree27.lua");
RegisterScript("object", "resources/scripts/tree28.lua");
RegisterScript("object", "resources/scripts/tree29.lua");
RegisterScript("object", "resources/scripts/tree30.lua");

--army of bunnies
RegisterScript("object", "resources/scripts/bunny1.lua");
RegisterScript("object", "resources/scripts/bunny2.lua");
RegisterScript("object", "resources/scripts/bunny3.lua");
RegisterScript("object", "resources/scripts/bunny4.lua");
RegisterScript("object", "resources/scripts/bunny5.lua");
RegisterScript("object", "resources/scripts/bunny6.lua");
RegisterScript("object", "resources/scripts/bunny7.lua");
RegisterScript("object", "resources/scripts/bunny8.lua");
RegisterScript("object", "resources/scripts/bunny9.lua");
RegisterScript("object", "resources/scripts/bunny10.lua");
RegisterScript("object", "resources/scripts/bunny11.lua");
RegisterScript("object", "resources/scripts/bunny12.lua");
RegisterScript("object", "resources/scripts/bunny13.lua");
RegisterScript("object", "resources/scripts/bunny14.lua");
RegisterScript("object", "resources/scripts/bunny15.lua");
