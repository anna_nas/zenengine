//
//  ZenEngine.h
//  ZenEngine
//
//  Created by //TODO: Team Name
//

#ifndef ZENENGINE_H_
#define ZENENGINE_H_

#include "rendering/render_detail/Shader.h"
#include "rendering/render_detail/Renderer.h"
#include "window/window.h"
#include "physics\BulletPhysics.h"
//#include "camera/camera.h"

#include "scripting/ScriptManager.h"
#include "game_object/ObjectManager.h"

//#include "utilities\singleton\Singletons.h"


typedef rendering::Renderer Renderer;
#endif /* ZENENGINE_H_ */