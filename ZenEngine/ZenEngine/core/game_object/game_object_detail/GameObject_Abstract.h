//
//  GameObject_Abstract.h
//  GameAssetDetail
//
//  Created by Brianna Whitcher
//
#ifdef TEST
#ifndef GAMEOBJECT_ABSTRACT_H_
#define GAMEOBJECT_ABSTRACT_H_

#include "../../rendering/render.h"

namespace gameobject {
	class GameObject {
	protected:
		gem::vec3f m_Position;
		gem::vec3f m_Rotation;
		gem::vec3f m_Scale;

		VertexArray		    m_VertexArray;
		VertexBuffer	    m_VertexBuffer;
		VertexBufferLayout  m_VertexBufferLayout;
		
		TextureBundle m_Texture;
		Shader m_Shader;

		std::vector<gem::vec3f>  m_VertexPositionData;

	public:

			/*
			 * @brief Constructor
			 */

		GameObject() {
			m_Position = gem::vec3f(0.0f, 0.0f, 0.0f);
			m_Rotation = gem::vec3f(0.0f, 0.0f, 0.0f);
			m_Scale = gem::vec3f(1.0f, 1.0f, 1.0f);
		}

			/*
			 * @brief Renders the object.
			 * @param[in] Projection A projection matrix for the rendering.
			 * @param[in] View A view matrix for the rendering.
			 * @return N/a.
			 */
		virtual void Draw(glm::mat4 Projection, glm::mat4 View) = 0;

			/*
			 * @brief Renders the object in debug mode. (wireframe)
			 * @param[in] Projection A projection matrix for the rendering.
			 * @param[in] View A view matrix for the rendering.
			 * @return N/a.
			 */
		virtual void DebugDraw(glm::mat4 Projection, glm::mat4 View) = 0;

		//virtual glm::mat4 GetModelMatrix() = 0;
		
			/*
			 * @brief Returns the VERTEX position of the object.
			 * @return A vec3 representing the object's position in space.
			 */
		virtual std::vector<gem::vec3f> & GetPositionData() { return m_VertexPositionData; }

			/*
			 * @brief Returns the object's textures.
			 * @return A collection of textures (TextureBundle).
			 */
		virtual TextureBundle & GetTextures() { return m_Texture; }

			/*
			 * @brief Returns the position of the object as a whole.
			 * @return A vec3 representing the object's position in space.
			 */
		virtual gem::vec3f GetPosition() { return m_Position; }

			/*
			 * @brief Returns the object's rotation.
			 * @return A vec3 representing the object's rotation in space.
			 */
		virtual gem::vec3f GetRotation() { return m_Rotation; }

			/*
			 * @brief Returns the object's size scaling.
			 * @return A collection of the scaling for each axis of the object.
			 */
		virtual gem::vec3f GetScale()    { return m_Scale; }

			/*
			 * @brief Sets the position of the object
			 * @param[in] positon A new position that the object will take.
			 * @return N/a.
			 */
		virtual void SetPosition(gem::vec3f position) { m_Position = position; }

			/*
			 * @brief Sets the position of the object, without the use of a vec3 object.
			 * @param[in] x A positon for the the object to take along the x-axis.
			 * @param[in] y A positon for the the object to take along the y-axis.
			 * @param[in] z A positon for the the object to take along the z-axis.
			 * @return N/a.
			 */
		virtual void SetPosition(float x, float y, float z) { m_Position = gem::vec3f(x, y, z); }

			/*
			 * @brief Sets the position of the object along the x axis.
			 * @param[in] x A positon for the the object to take along the x-axis.
			 * @return N/a.
			 */
		virtual void SetPositionX(float x) { m_Position.x = x; }

			/*
			 * @brief Sets the position of the object along the x axis.
			 * @param[in] y A positon for the the object to take along the y-axis.
			 * @return N/a.
			 */
		virtual void SetPositionY(float y) { m_Position.y = y; }

			/*
			 * @brief Sets the position of the object along the x axis.
			 * @param[in] z A positon for the the object to take along the z-axis.
			 * @return N/a.
			 */
		virtual void SetPositionZ(float z) { m_Position.z = z; }

			/*
			 * @brief Sets the rotation of an object.
			 * @param[in] rotation A vec3 that stores the rotations to be set.
			 * @return N/a
			 */
		virtual void SetRotation(gem::vec3f rotation) { m_Rotation = rotation; }

			/*
			 * @brief Sets the rotation of the object, without the use of a vec3 object.
			 * @param[in] x A rotation for the the object to take along the x-axis.
			 * @param[in] y A rotation for the the object to take along the y-axis.
			 * @param[in] z A rotation for the the object to take along the z-axis.
			 * @return N/a.
			 */
		virtual void SetRotation(float x, float y, float z) { m_Rotation = gem::vec3f(x, y, z); }

			/*
			 * @brief Sets the rotation of the object along the x axis.
			 * @param[in] x A rotation for the the object to take along the x-axis.
			 * @return N/a.
			 */
		virtual void SetRotationX(float x) { m_Rotation.x = x; }

			/*
			 * @brief Sets the rotation of the object along the y axis.
			 * @param[in] y A rotation for the the object to take along the y-axis.
			 * @return N/a.
			 */
		virtual void SetRotationY(float y) { m_Rotation.y = y; }

			/*
			 * @brief Sets the rotation of the object along the z axis.
			 * @param[in] z A rotation for the the object to take along the z-axis.
			 * @return N/a.
			 */
		virtual void SetRotationZ(float z) { m_Rotation.z = z; }

			/*
			 * @brief Sets the scaling of an object.
			 * @param[in] scale A vec3 that stores the scalings to be set.
			 * @return N/a
			 */
		virtual void SetScale(gem::vec3f scale) { m_Scale = scale; }

			/*
			 * @brief Sets the scaling of the object, without the use of a vec3 object.
			 * @param[in] x A scaling for the the object to take along the x-axis.
			 * @param[in] y A scaling for the the object to take along the y-axis.
			 * @param[in] z A scaling for the the object to take along the z-axis.
			 * @return N/a.
			 */
		virtual void SetScale(float x, float y, float z) { m_Scale = gem::vec3f(x, y, z); }

			/*
			 * @brief Sets the scaling of the object along the x axis.
			 * @param[in] x A scaling for the the object to take along the x-axis.
			 * @return N/a.
			 */
		virtual void SetScaleX(float x) { m_Scale.x = x; }

			/*
			 * @brief Sets the scaling of the object along the y axis.
			 * @param[in] y A scaling for the the object to take along the y-axis.
			 * @return N/a.
			 */
		virtual void SetScaleY(float y) { m_Scale.y = y; }

			/*
			 * @brief Sets the scaling of the object along the z axis.
			 * @param[in] z A scaling for the the object to take along the z-axis.
			 * @return N/a.
			 */
		virtual void SetScaleZ(float z) { m_Scale.z = z; }

			/*
			 * @brief Returns the height of an object.
			 * @param[in] position A vec3 position in 3 dimensional space.
			 * @return A float representation of the height.
			 * @note This is temporary!
			 */
		virtual float GetHeightAt(gem::vec3f position) = 0;

		// Physics Interface here
	};
};
#endif	/* GAMEOBJECT_ABSTRACT_H_ */
#endif