//
//  GameAssetFactory_Terrain.h
//  AssetFactory
//
//  Created by Brianna Whitcher
//

#ifndef ZENENGINE_CORE_ASSET_FACTORY_GAMEASSETFACTORY_TERRAIN_H_
#define ZENENGINE_CORE_ASSET_FACTORY_GAMEASSETFACTORY_TERRAIN_H_

#include "GameAssetFactory.h"

namespace gameobject {
	class TerrainFactory : public GameAssetFactory {
	public:
			/*
			 * @brief Creates a terrain object.
			 * @param[in] filepath A string that stores the relative location of a file for the terrain to be generated from.
			 * @param[in] texture An existing bundle of textures for the terrain.
			 * @param[in] shader An existing shader for the rendering.
			 * @return A unique pointer to the newly created terrain object.
			 * @bug Does not create any terrain or use any of the provided inputs, and only returns 'nullptr'.
			 */
		std::unique_ptr<GameObject> Create(std::string filepath, TextureBundle & texture, Shader & shader) { return nullptr; }

			/*
			 * @brief Creates a terrain object.
			 * @param[in] texture An existing bundle of textures for the terrain.
			 * @param[in] shader An existing shader for the rendering.
			 * @return A unique pointer to the newly created terrain object.
			 */
		std::unique_ptr<GameObject> Create(TextureBundle & texture, Shader & shader);
	};
};
#endif