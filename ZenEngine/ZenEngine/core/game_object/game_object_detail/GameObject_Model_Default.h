//
//  Model_Abstract.h
//  AssetFactory
//
//  Created by Brianna Whitcher
//

#ifndef ZENENGINE_CORE_ASSET_FACTORY_MODEL_DETAIL_MODEL_DEFAULT_H_
#define ZENENGINE_CORE_ASSET_FACTORY_MODEL_DETAIL_MODEL_DEFAULT_H_

#include "GameObject_Model_Abstract.h"

#include <string>
#include <map>

namespace gameobject {
	class DefaultModel : public Model {
	protected:
		using Model::m_Transform;

		// using Model::m_Position;
		// using Model::m_Rotation;
		// using Model::m_Scale;

		using Model::m_VertexArray;
		using Model::m_VertexBuffer;
		using Model::m_VertexBufferLayout;

		using Model::m_Shader;
		using Model::m_Texture;

	public:
		virtual void CreatePhysicsBody();
			/*
			 * @brief Default class constructor for the model, requiring user provided values for initialization.
			 * @param[in] filepath String representation of the relative file location for a model location.
			 * @param[in] texture An existing bundle of textures, passed by reference.
			 * @param[in] shader An existing shader, passed by reference.
			 * @return N/a.
			 */
		DefaultModel(std::string filepath, TextureBundle & texture, Shader & shader);

			/*
			 * @brief Draws the model.
			 * @param[in] Projection A mat4 object for the rendering projection.
			 * @param[in] View A mat4 object for the rendering view.
			 * @return N/a
			 */
		virtual void Draw(glm::mat4 Projection, glm::mat4 View, gem::vec3f camera);

			/*
			 * @brief Draws the model in debug mode. (wireframe mode)
			 * @param[in] Projection A mat4 object for the rendering projection.
			 * @param[in] View A mat4 object for the rendering view.
			 * @return N/a
			 */
		virtual void DebugDraw(glm::mat4 Projection, glm::mat4 View, gem::vec3f camera);

			/*
			 * @brief Returns the height of the object's position.
			 * @param[in] position A position in a 3-dimensional space.
			 * @return N/a
			 * @bug Does not use the position provided at all.
			 */
		virtual float GetHeightAt(gem::vec3f position = gem::vec3f(0.0f, 0.0f, 0.0f));
	};
};

#endif /* ZENENGINE_CORE_ASSET_FACTORY_MODEL_MODEL_H_ */