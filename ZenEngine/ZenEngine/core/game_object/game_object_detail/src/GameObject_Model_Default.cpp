
#include "../GameObject_Model_Default.h"
#include "../../ModelAndImageryCache.h"

#include "glm.hpp"
#include "gtc/type_ptr.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtx/transform.hpp"

#include "gem.h"
#include "../../../physics/colliders/BoxCollider.h" // added by Sava

#include <iostream>
namespace gameobject {
	DefaultModel::DefaultModel(std::string filepath, TextureBundle & texture, Shader & shader) {
		
		ModelCache m_ModelCache;

		const std::vector<Vertex> * vertices = m_ModelCache.GetModel(filepath);
		
		m_Texture = texture;
		m_Shader = shader;

		m_VertexArray.Bind();
		m_VertexBuffer.Bind();

		m_VertexBuffer.SetBufferData(&vertices->at(0), sizeof(Vertex) * vertices->size());
		m_VertexBufferLayout.Push<float>(3);
		m_VertexBufferLayout.Push<float>(2);
		m_VertexBufferLayout.Push<float>(3);

		m_VertexArray.AddBuffer(m_VertexBuffer, m_VertexBufferLayout);

		m_VertexArray.Unbind();
		m_VertexBuffer.Unbind();

//Is this required		delete vertices;
	}

	void DefaultModel::CreatePhysicsBody() {
		physics::ZenCollider* shape = new physics::BoxCollider(m_Transform.GetScale());
		
		m_Body = new physics::RigidBody(m_Transform, 0.0/*2f*/, shape, physics::ObjectType::STATIC);// DYNAMIC);
	}

	static bool flag = false;

	void DefaultModel::Draw(glm::mat4 Projection, glm::mat4 View, gem::vec3f camera) {
		m_Shader.Bind();
		m_Texture.Bind();
		//m_Texture.SetTextureUniforms(&m_Shader, 1);

		glm::mat4 Model = glm::mat4(1.0f);
		Model = glm::translate(Model, glm::vec3(m_Transform.GetPosition().x, m_Transform.GetPosition().y, m_Transform.GetPosition().z));
		Model = glm::rotate	(Model, glm::radians(m_Transform.GetRotation().x), glm::vec3(1, 0, 0));
		Model = glm::rotate	(Model, glm::radians(m_Transform.GetRotation().y), glm::vec3(0, 1, 0));
		Model = glm::rotate	(Model, glm::radians(m_Transform.GetRotation().z), glm::vec3(0, 0, 1));
		//glm::mat4 Model = m_Body->GetModelMatrix(); // added by Sava

		//Remove this from final, for testing purposes
		if (flag) {
			std::cout << "==========MODEL==========" << std::endl;
			std::cout << "Model Matrix: " << std::endl;
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					std::cout << Model[i][j] << " ";
				}
				std::cout << std::endl;
			}
			flag = false;
		}

		glm::mat4 mvp = Projection * View * Model;

		m_Shader.SetUniformMat4fv("u_MVPMatrix", glm::value_ptr(mvp));
		m_Shader.SetUniformMat4fv("u_ModelMatrix", glm::value_ptr(Model));
		m_Shader.SetUniformMat4fv("u_ViewMatrix", glm::value_ptr(View));
		m_Shader.SetUniform3f("u_PlayerPosition", camera.x, camera.y, camera.z);
		m_Shader.SetUniform1f("u_ScaleUV", 1);

		m_VertexArray.Bind();
		m_VertexBuffer.Draw();

		m_VertexArray.Unbind();
		m_Shader.Unbind();
		m_Texture.Unbind();
	}

	void DefaultModel::DebugDraw(glm::mat4 Projection, glm::mat4 View, gem::vec3f camera) {
		m_Shader.Bind();
		m_Texture.Bind();
		//m_Texture.SetTextureUniforms(&m_Shader, 1);

		//glm::mat4 Model = glm::mat4(1.0f);
		//Model = glm::translate(Model, glm::vec3(m_Position.x, m_Position.y, m_Position.z));
		//Model = glm::rotate	(Model, glm::radians(m_Rotation.x), glm::vec3(1, 0, 0));
		//Model = glm::rotate	(Model, glm::radians(m_Rotation.y), glm::vec3(0, 1, 0));
		//Model = glm::rotate	(Model, glm::radians(m_Rotation.z), glm::vec3(0, 0, 1));
		// Check this ^^^^^ eventually eplace with quaternions to make everyone's life easier. 
		// This is most likely not producing correct behaviour. Maybe don't use this.
		glm::mat4 Model = m_Body->GetModelMatrix(); // added by Sava

		glm::mat4 mvp = Projection * View * Model;

		m_Shader.SetUniformMat4fv("u_MVPMatrix", glm::value_ptr(mvp));
		m_Shader.SetUniformMat4fv("u_ModelMatrix", glm::value_ptr(Model));
		m_Shader.SetUniformMat4fv("u_ViewMatrix", glm::value_ptr(View));
		m_Shader.SetUniform3f("u_PlayerPosition", camera.x, camera.y, camera.z);
		m_Shader.SetUniform1f("u_ScaleUV", 1);

		m_VertexArray.Bind();
		m_VertexBuffer.DebugDraw();

		m_VertexArray.Unbind();
		m_Shader.Unbind();
		m_Texture.Unbind();
	}

	float DefaultModel::GetHeightAt(gem::vec3f position) {
		// return m_Position.y;
		return m_Transform.GetPosition().y; // added by Sava
	}
}; /* model */

/*

mat4 transform;

GetModelMatrix()

Abstract Object Factory
	Abstract Geometry Factory
				Concrete Terrain Factory
							BruteForce Terrain
							FaultLine Terrain
				Concrete Model Factory
							Default Model
							Animated Model
	Abstract Physics Component Factory -> Not GameObject/RigidBody
				Concrete Collider Factory
							Mesh Collider	 (triangle mesh)
								Scaled Immovable
								Immovable
								Movable
							Terrain Collider (heightfield)
							Box
							Sphere
							Plane
							Capsule Collider (Capsule)
							Cone
							Cylinder
							Convex
							Concave???? If exists
							//Compound shapes
							//All others
				Concrete CollisionObject Factory 
							RigidBody
							SoftBody
							??

factory[CollisionObject]->Create<RigidBody>(Parameters * p)
factory[Terrain]->Create<BruteForceTerrain>(Parameters * p)


struct Parameters {
	int paramcount;

	Parameters() 
}

struct RigibBodyParameters : Parameters {
	using Parameters::paramcount;


	Transform
	CollisionObject *


}

class Transform : btMotionState, btTransform {
	using MotionState::member

}







class RigidBody {
	Transform  m_Transform;
	Collider * m_Collider
	float	   m_Mass

	RigidBody(position, rotation, scale, mass) {
		m_Transform.SetPosition(position);



	}

}


PhysicsWorld {
protected:


public:
	~PhysicsWorld() {
		//Dealloc here
	}

	Update() {


	}

	Add

}


class RigidBody {


}



class Transform {
protected:
	btMotionState *
	btTransform
	vec3 m_Scale;

public:
	Transform() : ???? { 
		give motionstate the transform
	}

	Set*(vector) {
		mat4
	}

	mat4 GetModelMatrix() {
		glm::mat4 scale = glm::Scale(m_scale);
		return tranfrom.GetOPenGLMatrix() * scale;
	}
}






Texture Factory
Shader Factory


Abstract Game Object Factory
	Collision
	Texture
	Shader
	GameObject 


GameObject


GeometryData	m_VertexBuffer				-> Terrain (indexed)
	Init()											m_IndexBuffer
	GenerateBuffers()
	Draw()
	DebugDraw()								-> Model3D (unindexed)

RigidBody -> GameObject					-> StaticGameObject
				- Geometry *			-> DynamicGameObject
				- Other					-> KinematicGameObject


					if(Kinematic) {

					}

					if(dynamic) {

					}

					else {



					Friction

					Restitution

					Mass & Centre of mass. Only change COM if compond

MotionState

CollisionShape
	Sphere
	Box
	Heightfield
	Plane
	Convex hull
	Concave hull
	Triangle Mesh
	Cone
	Capsule
	Compound Shape

RigidBody

					SoftBody




Terrain
Model3D










}
*/