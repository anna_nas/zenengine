//
//  Terrain_FaultLine.h
//  GameAssetFactory
//
//  Created by Brianna Whitcher
//

#include "../GameObject_Terrain_FaultLine.h"

#include <memory>
#include <chrono>
#include <random>
#include <iostream>

#include "gtc/type_ptr.hpp"

#include "../../../physics/colliders/HeightfieldCollider.h" // added by Sava
#include "../../../physics/colliders/StaticPlaneCollider.h" // added by Sava

namespace gameobject {

	FaultLineTerrain::FaultLineTerrain(TextureBundle & texture, Shader & shader,
		float height_scale, float length_meters, unsigned int steps) {

		m_Texture = texture;
		m_Shader = shader;

		m_HeightScale = height_scale;
		m_Length = length_meters;
		m_VertexCount = steps;
	
		m_Step = m_Length / static_cast<float>(m_VertexCount);
	
		GenerateVertexBuffer();
		GenerateIndexBuffer();

		m_VertexArray.Unbind();
		m_VertexBuffer.Unbind();
		m_IndexBuffer.Unbind();
	}

	void FaultLineTerrain::CreatePhysicsBody() {
		//physics::ZenCollider* shape = new physics::HeightfieldCollider(
		//	static_cast<int>(m_VertexCount+1), static_cast<int>(m_VertexCount+1),
		//	&m_HeightFieldf, m_HeightScale, 0.0f, 255.0f, 1, physics::ScalarType::FLOAT);
		physics::ZenCollider* shape = new physics::StaticPlaneCollider(gem::vec3f(0.0, 1.0, 0.0), -400.0f);
		m_Body = new physics::RigidBody(m_Transform, 0.0f, shape, physics::ObjectType::STATIC);
	}

	static bool flag = false;

	void FaultLineTerrain::Draw(glm::mat4 Projection, glm::mat4 View, gem::vec3f camera) {
		m_Shader.Bind();
		m_Texture.Bind();
		m_Texture.SetTextureUniforms(&m_Shader, 6);

		//glm::mat4 Model = glm::mat4(1.0f);
		glm::mat4 Model = m_Body->GetModelMatrix(); // added by Sava
		/*
		if (flag) {
			std::cout << "==========HEIGHTFIELD==========" << std::endl;
			std::cout << "Vertex Count: " << m_VertexCount << std::endl;
			std::cout << "Height Scale: " << m_HeightScale << std::endl;
			std::cout << "Min Height: " << m_MinHeight << std::endl;
			std::cout << "Max Height: " << m_MaxHeight << std::endl;
			std::cout << "Model Matrix: " << std::endl;
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					std::cout << Model[i][j] << " ";
				}
				std::cout << std::endl;
			}
			flag = false;
		}
		*/
		glm::mat4 mvp = Projection * View * Model;

		m_Shader.SetUniformMat4fv("u_MVPMatrix",   glm::value_ptr(mvp));
		m_Shader.SetUniformMat4fv("u_ModelMatrix", glm::value_ptr(Model));
		m_Shader.SetUniformMat4fv("u_ViewMatrix",  glm::value_ptr(View));
		m_Shader.SetUniform3f("u_PlayerPosition", camera.x, camera.y, camera.z);
		m_Shader.SetUniform1f("u_ScaleUV", 4.0f);

		m_VertexArray.Bind();
		m_IndexBuffer.Draw();

		m_VertexArray.Unbind();
		m_Shader.Unbind();
		m_Texture.Unbind();
	}

	void FaultLineTerrain::DebugDraw(glm::mat4 Projection, glm::mat4 View, gem::vec3f camera) {
		m_Shader.Bind();
		m_Texture.Bind();
		m_Texture.SetTextureUniforms(&m_Shader, 6);

		//glm::mat4 Model = glm::mat4(1.0f);
		glm::mat4 Model = m_Body->GetModelMatrix(); // added by Sava
		glm::mat4 mvp = Projection * View * Model;

		m_Shader.SetUniformMat4fv("u_MVPMatrix", glm::value_ptr(mvp));
		m_Shader.SetUniformMat4fv("u_ModelMatrix", glm::value_ptr(Model));
		m_Shader.SetUniformMat4fv("u_ViewMatrix", glm::value_ptr(View));
		m_Shader.SetUniform3f("u_PlayerPosition", camera.x, camera.y, camera.z);
		m_Shader.SetUniform1f("u_ScaleUV", 4.0f);

		m_VertexArray.Bind();
		m_IndexBuffer.DebugDraw();

		m_VertexArray.Unbind();
		m_Shader.Unbind();
		m_Texture.Unbind();
	}

	float FaultLineTerrain::GetHeightAt(gem::vec3f position) {

		float xf = (position.x - m_Transform.GetPosition().x) / m_Step; // edited by Sava
		float zf = (position.z - m_Transform.GetPosition().z) / m_Step; // edited by Sava
		
		int x = static_cast<int>(xf);
		int z = static_cast<int>(zf);

		xf -= x;
		zf -= z;

		if ((x >= 0 && x + 1 <= static_cast<int>(m_VertexCount - 1)) &&
			(z >= 0 && z + 1 <= static_cast<int>(m_VertexCount - 1))) {

		//	.___.
		//	|  /|		x percent
		//	| / |		z percent
		//	|/  |		line equation: c = y - x*m
		//	.___.
		//

			float corner00 = SampleHeightField(x + 0, z + 0);
			float corner01 = SampleHeightField(x + 0, z + 1);
			float corner10 = SampleHeightField(x + 1, z + 0);
			float corner11 = SampleHeightField(x + 1, z + 1);

			if (x > z) {
				return corner00 +
					xf * (corner10 - corner00) +
					zf * (corner11 - corner10);
			}						
			else {					
				return corner00 +
					xf * (corner11 - corner01) +
					zf * (corner01 - corner00);
			}
		}

		else { return m_Transform.GetPosition().y; } // edited by Sava // m_Position.y; }
		
		
		/*
		int x = static_cast<int>((position.x - m_Position.x) / static_cast<int>(m_Step));
		int z = static_cast<int>((position.z - m_Position.z) / static_cast<int>(m_Step));

		if ((x >= 0 && x + 1 <= static_cast<int>(m_VertexCount - 1)) &&
			(z >= 0 && z + 1 <= static_cast<int>(m_VertexCount - 1))) {
			
			float height =
				SampleHeightField(x,	 z	  ) +
				SampleHeightField(x,	 z + 1) +
				SampleHeightField(x + 1, z	  ) +
				SampleHeightField(x + 1, z + 1);
				
			height /= 4.0f;
			
			return height;
		}
		
		else { return m_Position.y; }
		*/
	}

	void FaultLineTerrain::GenerateVertexBuffer() {
		//GeneratePositions(m_VertexPositionData);
		//GenerateUVs(uvs);
		//GenerateNormals(normals);
		GenerateHeightField();
		std::vector<Vertex> vertices;

		float u, v;

		//Positions
		for (unsigned int z = 0; z < m_VertexCount; z++) {
			if (z % 8 == 0) {
				v = 0;
			}
			else if (z % 8 == 1 || z % 8 == 7) {
				v = 0.25f;
			}
			else if (z % 8 == 2 || z % 8 == 6) {
				v = 0.5f;
			}
			else if (z % 8 == 3 || z % 8 == 5) {
				v = 0.75f;
			}
			else if (z % 8 == 4) {
				v = 1;
			}
		  
			for (unsigned int x = 0; x < m_VertexCount; x++) {
				if (x % 8 == 0) {
					u = 0;
				}
				else if (x % 8 == 1 || x % 8 == 7) {
					u = 0.25f;
				}
				else if (x % 8 == 2 || x % 8 == 6) {
					u = 0.5f;
				}
				else if (x % 8 == 3 || x % 8 == 5) {
					u = 0.75f;
				}
				else if (x % 8 == 4) {
					u = 1;
				}

				vertices.emplace_back(
					gem::vec3f(x * m_Step, m_HeightField[x + z * m_VertexCount], z * m_Step),
					gem::vec2f(u, v),
					gem::vec3f(0.0f, 1.0f, 0.0f));
			}
		}
		
		float L, R, D, U;
		//Normals
		for (unsigned int z = 0; z < m_VertexCount; z++) {

			for (unsigned int x = 0; x < m_VertexCount; x++) {
				L = GetHeightAt(gem::vec3f(x*m_Step - 0.2f, 0, z*m_Step));
				R = GetHeightAt(gem::vec3f(x*m_Step + 0.2f, 0, z*m_Step));
				D = GetHeightAt(gem::vec3f(x*m_Step, 0, z*m_Step - 0.2f));
				U = GetHeightAt(gem::vec3f(x*m_Step, 0, z*m_Step + 0.2f));

				vertices[z * m_VertexCount + x].Normal.x = L - R;
				vertices[z * m_VertexCount + x].Normal.y = D - U;
				vertices[z * m_VertexCount + x].Normal.z = 2.0f;

				vertices[z * m_VertexCount + x].Normal = gem::normalize(vertices[z * m_VertexCount + x].Normal);


			}
		}
		
		m_VertexArray.Bind();
		m_VertexBuffer.Bind();

		m_VertexBuffer.SetBufferData(&vertices.front(), sizeof(Vertex) * vertices.size());
		m_VertexBufferLayout.Push<float>(3);
		m_VertexBufferLayout.Push<float>(2);
		m_VertexBufferLayout.Push<float>(3);

		m_VertexArray.AddBuffer(m_VertexBuffer, m_VertexBufferLayout);

		//m_Position.x += (m_Length / 2.0f);
		//m_Position.z += (m_Length / 2.0f);
	}

	void FaultLineTerrain::GenerateIndexBuffer() {
		std::vector<unsigned int> indices;

		int offset = 0;
		for (unsigned int i = 0; i < (m_VertexCount + 1) * (m_VertexCount - 1); i++) {
			if (i == (m_VertexCount + 1) * (m_VertexCount - 1) - 1) {
				offset++;
				indices.push_back(i + m_VertexCount - offset);
				indices.push_back(i + m_VertexCount - offset);
			}
			else if (i % (m_VertexCount + 1) >= m_VertexCount) {
				offset++;
				indices.push_back(i + m_VertexCount - offset);
				indices.push_back(i - offset + 1);
			}
			else {
				indices.push_back(i - offset);
				indices.push_back(i + m_VertexCount - offset);
			}
		}

		m_IndexBuffer.Bind();

		m_IndexBuffer.SetBufferData(&indices.front(), indices.size());

		m_VertexArray.Unbind();
		m_VertexBuffer.Unbind();
		m_IndexBuffer.Unbind();
	}

	void FaultLineTerrain::GeneratePositions(std::vector<gem::vec3f> & positions) {
		/*
		for (unsigned int z = 0; z < m_VertexCount; z++) {
			for (unsigned int x = 0; x < m_VertexCount; x++) {
				m_Vertices.emplace_back(
					m_Position.x + m_Step * x,
					m_Position.y,
					m_Position.z + m_Step * z,

					(x % 2),
					(z % 2),

					0.0f, 1.0f, 0.0f);
			}
		}
		

		GenerateHeightField();


		for (unsigned int i = 0; i < m_VertexCount * m_VertexCount; i++) {
			m_Vertices.emplace_back(
				m_Position.x + m_Step * (i % m_VertexCount),
				m_Position.y, 
				m_Position.z + m_Step * (i - i % m_VertexCount) / m_VertexCount,

				(i % m_VertexCount) % 2,
				((i - i % m_VertexCount) / m_VertexCount) % 2,

				0.0f, 1.0f, 0.0f);
		}

		for (unsigned int i = 0; i < m_VertexCount * m_VertexCount; i++) {
			m_Vertices[i].Position.y = SampleHeightField(gem::vec3f(i % m_VertexCount, 0.0f, (i - i % m_VertexCount) / m_VertexCount));
		}

		*/
	}

	void FaultLineTerrain::GenerateUVs(std::vector<gem::vec2f> & uvs) {

	}

	void FaultLineTerrain::GenerateNormals(std::vector<gem::vec3f> & normals) {
		
	}

	void FaultLineTerrain::GenerateTexture() {
	
	}

	void FaultLineTerrain::GenerateHeightField() {
		std::vector<float> raw_height_data;
		
		for (unsigned int z = 0; z < m_VertexCount; z++) {
			for (unsigned int x = 0; x < m_VertexCount; x++) {
				raw_height_data.push_back(0.0f);
			}
		}

		int x1, x2, z1, z2;
		int iterations = 64;
		float displacement;

		std::random_device rd;
		std::mt19937 mersenne_twister(rd());

		std::uniform_int_distribution<> x_rng(0, m_VertexCount);
		std::uniform_int_distribution<> z_rng(0, m_VertexCount);

		for (int j = 0; j < iterations; j++) {
			displacement = 255.0f - 255.0f * (j / static_cast<float>(iterations));
			x1 = x_rng(mersenne_twister);
			z1 = z_rng(mersenne_twister);

			do {
				x2 = x_rng(mersenne_twister);
				z2 = z_rng(mersenne_twister);
			} while (x2 == x1 || z2 == z1);

			for (unsigned int z = 0; z < m_VertexCount; z++) {
				for (unsigned int x = 0; x < m_VertexCount; x++) {

					int yn = (x2 - x1) * (z - z1) - (z2 - z1) * (x - x1);

					if (yn > 0) { 
						raw_height_data[z * m_VertexCount + x] += displacement;
					}

					else { 
						raw_height_data[z * m_VertexCount + x] -= displacement;
					}
				}
			}

			AddFilter(raw_height_data, 0.5f);
		}

		NormaliseRawHeightData(raw_height_data);
		
		for (unsigned int i = 0; i < raw_height_data.size(); i++) {
			m_HeightField.push_back(static_cast<unsigned char>(raw_height_data[i]));
			m_HeightFieldf.push_back(raw_height_data[i]); // added by Sava
		}

		m_MaxHeight = m_MinHeight = m_HeightFieldf[0];
		for (unsigned int i = 0; i < raw_height_data.size(); i++) {
			if (m_HeightFieldf[i] > m_MaxHeight) { m_MaxHeight = m_HeightFieldf[i]; }
			if (m_HeightFieldf[i] < m_MinHeight) { m_MinHeight = m_HeightFieldf[i]; }
		}

		m_HeightField.shrink_to_fit();
	}

	float FaultLineTerrain::SampleHeightField(unsigned int x, unsigned int z) {
		return static_cast<float>(m_HeightField[z * m_VertexCount + x]) * m_HeightScale;
	}

	void FaultLineTerrain::NormaliseRawHeightData(std::vector<float> & raw_height_data) {
		float max_height = raw_height_data[0];
		float min_height = raw_height_data[0];
		max_height = min_height = raw_height_data[0];

		for (unsigned int i = 0; i < raw_height_data.size(); i++) {
			if (raw_height_data[i] > max_height) { max_height = raw_height_data[i]; }
			if (raw_height_data[i] < min_height) { min_height = raw_height_data[i]; }
		}

		float height_range = max_height - min_height;

		for (unsigned int i = 0; i < raw_height_data.size(); i++) {
			raw_height_data[i] = (raw_height_data[i] - min_height) / height_range * 255;
		}
	}

	void FaultLineTerrain::AddFilter(std::vector<float> & raw_height_data, float weight) {
		float previous_height = raw_height_data[0];

		for (int z = 0; z < 128; z++) {
			for (int x = 1; x < 128; x++) {
				raw_height_data[z * 128 + x] = weight * previous_height + 
					(1 - weight) * raw_height_data[z * 128 + x];
				previous_height = raw_height_data[z * 128 + x];
			}
		}

		for (int z = 0; z < 128; z++) {
			for (int x = 128 - 2; x >= 0; x--) {
				raw_height_data[z * 128 + x] = weight * previous_height + 
					(1 - weight) * raw_height_data[z * 128 + x];
				previous_height = raw_height_data[z * 128 + x];
			}
		}

		for (int x = 0; x < 128; x++) {
			for (int z = 1; z < 128; z++) {
				raw_height_data[z * 128 + x] = weight * previous_height + 
					(1 - weight) * raw_height_data[z * 128 + x];
				previous_height = raw_height_data[z * 128 + x];
			}
		}

		for (int x = 0; x < 128; x++) {
			for (int z = 128 - 2; z >= 0; z--) {
				raw_height_data[z * 128 + x] = weight * previous_height + 
					(1 - weight) * raw_height_data[z * 128 + x];
				previous_height = raw_height_data[z * 128 + x];
			}
		}
	}

	gem::vec3f FaultLineTerrain::GetPosition() {


		return gem::vec3f(m_Transform.GetPosition().x + m_Length/2.0f, 
			m_Transform.GetPosition().y, 
			m_Transform.GetPosition().z + m_Length / 2.0f);
	}

};