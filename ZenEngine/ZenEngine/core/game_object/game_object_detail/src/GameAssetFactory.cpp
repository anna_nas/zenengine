
#include "../GameAssetFactory.h"
#include "../GameAssetFactory_Model.h"
#include "../GameAssetFactory_Terrain.h"

namespace gameobject {
	std::unique_ptr<GameAssetFactory> GameAssetFactory::CreateFactory(AssetType type) {
		if (type == Terrain) {
			return std::unique_ptr<GameAssetFactory>(new TerrainFactory());
		}

		else {
			return std::unique_ptr<GameAssetFactory>(new ModelFactory());
		}
	}
};