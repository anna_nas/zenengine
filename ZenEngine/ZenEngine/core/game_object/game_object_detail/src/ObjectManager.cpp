#include "../../ObjectManager.h"

namespace gameobject {

	GameObjectManager::GameObjectManager(std::string basic_shader_filepath, std::string texture_shader_filepath) {
		basic_shader.SetSource(basic_shader_filepath);
		terrain_shader.SetSource(texture_shader_filepath);
	}

	void GameObjectManager::DrawObjs(glm::mat4 Projection, glm::mat4 View, gem::vec3f camera) {
		for (unsigned i = 0; i < objs.size(); i++) {
			objs[i]->Draw(Projection, View, camera);
		}
	}

	void GameObjectManager::DebugDrawObjs(glm::mat4 Projection, glm::mat4 View, gem::vec3f camera) {
		for (unsigned i = 0; i < objs.size(); i++) {
			objs[i]->DebugDraw(Projection, View, camera);
		}
	}

	void GameObjectManager::CreateObjects(std::vector<scripting::obj>& objDetails) {
		for (unsigned i = 0; i < objDetails.size(); i++) {
			CreateObject(objDetails[i]);
		}
	}

		//This isnt part of the class, its a space delimited string splitter
	std::vector<std::string> split(std::string& source) {
		std::vector<std::string> results;
		unsigned delimPos;
		while ((delimPos = source.find(" ")) != std::string::npos) {
			results.push_back(source.substr(0, delimPos));
			source.erase(0, delimPos + 1);
		}
		results.push_back(source);

		return results;
	}
	
		//This isnt part of the class, its a str - float converter
	float toFloat(const std::string& num) {
		return std::stof(num);
	}
	
	void GameObjectManager::CreateObject(scripting::obj& objDetail){
		std::vector<std::string> args;
		TextureBundle texture;

		// work out texture details
		args = split(objDetail.texture);
		if (args[0] == "Texture") {
			//any args after 0 is path
			for (unsigned i = 1; i < args.size(); i++) {
				texture.AddTexture(args[i]);
			}
		}
		else if (args[0] == "Color") {
			//args[1-3] are RGBA, use toFloat()
		}

		// work out position, rotation, scale
		args = split(objDetail.position);
		gem::vec3f pos(toFloat(args[0]), toFloat(args[1]), toFloat(args[2])); //this is the object position
		
		args = split(objDetail.rotation);
		gem::vec3f rot(toFloat(args[0]), toFloat(args[1]), toFloat(args[2])); //this is the object rotation
		
		args = split(objDetail.scale);
		gem::vec3f scale(toFloat(args[0]), toFloat(args[1]), toFloat(args[2])); //this is the object scale
		
		// determine what type/subtype of object to render
		args = split(objDetail.type);
		if (args[0] == "Terrain") {
			objs.push_back(GameAssetFactory::CreateFactory(GameAssetFactory::Terrain)->Create(texture, terrain_shader));

			objs[objs.size() - 1]->CreatePhysicsBody(); // added by Sava
		}
		else if (args[0] == "Model") {
			//args[1] is filepath
			objs.push_back(GameAssetFactory::CreateFactory(GameAssetFactory::Model)->Create(args[1], texture, terrain_shader));
			
			objs[objs.size() - 1]->SetPosition(pos.x, GetTerrainHeightAt(pos) + pos.y, pos.z);
			objs[objs.size() - 1]->SetRotation(rot);
			objs[objs.size() - 1]->SetScale(scale);
			
			objs[objs.size() - 1]->CreatePhysicsBody(); // added by Sava
		}
		if (objDetail.AI != "None") {
			objs[objs.size() - 1]->SetState(objDetail.AI);
		}
	}

	void GameObjectManager::DoAI() {
		gem::vec3f tempPos;
		for (unsigned i = 0; i < objs.size(); i++) {
			objs[i]->DoAI(0.02f);
		}
		for (unsigned i = 1; i < objs.size(); i++) {
			objs[i]->SetPositionY(GetTerrainHeightAt(objs[i]->GetPosition()));
		}
	}

} // namespace gameobject
