
#include "../GameAssetFactory_Model.h"
#include "../GameObject_Model_Default.h"

namespace gameobject {
	std::unique_ptr<GameObject> ModelFactory::Create(std::string filepath, TextureBundle & texture, Shader & shader) {
		return std::unique_ptr<GameObject>(new DefaultModel(filepath, texture, shader));
	}
};