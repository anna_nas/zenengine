
#include "../../ModelAndImageryCache.h"

#include <iostream>

#include "../ObjLoader.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

namespace gameobject {
	std::unordered_map<std::string, std::vector<Vertex>> ModelAndImageryCache::s_ModelCache;
	std::unordered_map<std::string, unsigned int> ModelAndImageryCache::s_TextureCache;
	
	const std::vector<Vertex> * ModelAndImageryCache::GetModel(std::string filepath) {
		auto iterator = s_ModelCache.find(filepath);
		if (iterator == s_ModelCache.end()) {
			std::vector<Vertex> vertices;
			vertices.reserve(4096);
	
			if (!LoadOBJ(filepath, &vertices, BRIEF)) {
				std::cout << "Warning: " << filepath << " could not be loaded. Please check the path and try again\n";
			}
	
			iterator = s_ModelCache.insert(std::make_pair(filepath, vertices)).first;
		}
	
		return &iterator->second;
	}
	
	unsigned int ModelAndImageryCache::GetTexture(std::string filepath,
		int * width, int * height, int * channels) {
		
		auto iterator = s_TextureCache.find(filepath);
		if (iterator == s_TextureCache.end()) {
			unsigned char * texture_buffer;// [3] = { 255, 0, 255 };
			
			std::cout << "Reading File: " << filepath << std::endl;
			
			stbi_set_flip_vertically_on_load(1);
			texture_buffer = stbi_load(filepath.c_str(), width, height, channels, STBI_rgb_alpha);
			
			if (!texture_buffer) {
				std::cout << "\n\n\n\n\nWarning: " << filepath << " could not be loaded. Please check the path and try again\n";
			}

			else {
				std::cout << "File Summary: "	<< filepath		<< "\n"
					<< "  * Size:\t\t"			<< "TBA"		<< "\n"
					<< "  * Width:\t\t"			<< *width		<< "\n"
					<< "  * Height:\t\t"		<< *height		<< "\n"
					<< "  * Bits Per Pixel:\t"	<< *channels	<< "\n";
			}

			unsigned int ID = 0;

			glGenTextures(1, &ID);

			glBindTexture(GL_TEXTURE_2D, ID);

			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 64);
			//glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);		
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, GL_LINEAR);

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, *width, *height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_buffer);
			glGenerateMipmap(GL_TEXTURE_2D);

			glBindTexture(GL_TEXTURE_2D, 0);

			iterator = s_TextureCache.insert(std::make_pair(filepath, ID)).first;
		}

		return iterator->second;
	}
};
