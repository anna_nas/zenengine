#include "../GameAssetFactory_Terrain.h"
#include "../GameObject_Terrain_FaultLine.h"

#include <memory>

namespace gameobject {
	std::unique_ptr<GameObject> TerrainFactory::Create(TextureBundle & texture, Shader & shader) {

		return std::unique_ptr<GameObject>(new FaultLineTerrain(texture, shader));
	}
};