//
//  GameAssetFactory.h
//  AssetFactory
//
//  Created by Brianna Whitcher
//

#ifndef ZENENGINE_CORE_ASSET_FACTORY_GAMEASSETFACTORY_H_
#define ZENENGINE_CORE_ASSET_FACTORY_GAMEASSETFACTORY_H_

#include <memory>

// #include "GameObject_Abstract.h"
#include "GameObject_Abstract_Test.h" // added by Sava

namespace gameobject {
	class GameAssetFactory {
	public: 
		enum AssetType { Terrain, Model };

			/*
			 * @brief Creates a game object.
			 * @param[in] filepath A string that stores the relative location of a file for the game object to be generated from.
			 * @param[in] texture An existing bundle of textures for the game object.
			 * @param[in] shader An existing shader for the rendering.
			 * @return A unique pointer to the newly created game object.
			 */
		virtual std::unique_ptr<GameObject> Create(std::string filepath, TextureBundle & texture, Shader & shader) = 0;

			/*
			 * @brief Creates a game object.
			 * @param[in] filepath A string that stores the relative location of a file for the game object to be generated from.
			 * @param[in] texture An existing bundle of textures for the game object.
			 * @param[in] shader An existing shader for the rendering.
			 * @return A unique pointer to the newly created game object.
			 */
		virtual std::unique_ptr<GameObject> Create(TextureBundle & texture, Shader & shader) = 0;

			/*
			 * @brief Creates an object factory.
			 * @param[in] type The selection of the object factory type to produce.
			 * @return A unique pointer to the newly created game object factory.
			 */
		static std::unique_ptr<GameAssetFactory>CreateFactory(AssetType type);
	};
};

/*
typedef unsigned int AssetType;
enum TerrainType { FromImage, FaultLine };
enum ModelType   { Default, Animated };

enum TextureSpecifier { Color, SingleTexture, MultiTexture };
enum NormalSpecifier  { LoadNormals, GenerateNormals };
enum ShaderSpecifier  { DefaultShading, CustomShading };

class GameAssetFactory {
public:
	template <typename Asset>
	std::unique_ptr<GameObject> Create(AssetType type, TextureSpecifier texture, NormalSpecifier normals, ShaderSpecifier shaders) { std::cout << "Error: Unsupported type." << std::endl; }
	
	template <typename Asset, AssetType type>
	std::unique_ptr<GameObject> Create(TextureSpecifier texture) { return nullptr; }

	template <typename Asset, AssetType type>
	std::unique_ptr<GameObject> Create(std::string filepath, TextureSpecifier texture) { return nullptr; }
};

/*
template <>
inline std::unique_ptr<GameObject> GameAssetFactory::Create<Terrain, FaultLine>(TextureSpecifier texture) {
	float x_meters = 1000;
	float z_meters = 1000;
	unsigned int x_vertices = 100;
	unsigned int z_vertices = 100;

	// TODO: handle textures and colors
	// TODO: terrain creation parameters

	if     (texture == TextureSpecifier::Color)			{ return std::unique_ptr<GameObject>(new terrain::FaultLineTerrain(x_meters, z_meters, x_vertices, z_vertices)); }
	else if(texture == TextureSpecifier::SingleTexture) { return std::unique_ptr<GameObject>(new terrain::FaultLineTerrain(x_meters, z_meters, x_vertices, z_vertices)); }
	else if(texture == TextureSpecifier::MultiTexture)  { return std::unique_ptr<GameObject>(new terrain::FaultLineTerrain(x_meters, z_meters, x_vertices, z_vertices)); }
	else { return nullptr; }

	// GenerateNormals is the only option
	// DefaultShading  is the only option
}

template <>
inline std::unique_ptr<GameObject> GameAssetFactory::Create<Terrain, FromImage>(std::string filepath, TextureSpecifier texture) {
	float x_meters = 1000;
	float z_meters = 1000;
	unsigned int x_vertices = 100;
	unsigned int z_vertices = 100;

	// TODO: different handling for textures and colors
	// TODO: terrain creation parameters

	if		(texture == TextureSpecifier::Color)		 { return std::unique_ptr<GameObject>(new terrain::BruteForceTerrain(filepath, x_meters, z_meters, x_vertices, z_vertices)); }
	else if (texture == TextureSpecifier::SingleTexture) { return std::unique_ptr<GameObject>(new terrain::BruteForceTerrain(filepath, x_meters, z_meters, x_vertices, z_vertices)); }
	else if (texture == TextureSpecifier::MultiTexture)  { return std::unique_ptr<GameObject>(new terrain::BruteForceTerrain(filepath, x_meters, z_meters, x_vertices, z_vertices)); }
	else { return nullptr; }

	// GenerateNormals is the only option
	// DefaultShading  is the only option
}

template <>
inline std::unique_ptr<GameObject> GameAssetFactory::Create<Terrain>(AssetType type, TextureSpecifier texture, NormalSpecifier normals, ShaderSpecifier shaders) {
	std::cout << "Asset Type:\tTerrain" << std::endl;
	
	std::cout << "Asset Subtype:\t";
	if		(type == TerrainType::FromImage) { std::cout << "From Image" << std::endl; }
	else if (type == TerrainType::FaultLine) { std::cout << "Fault Line" << std::endl; }

	std::cout << "Texture:\t";
	if		(texture == TextureSpecifier::Color)	     { std::cout << "No Texture"		<< std::endl; }
	else if (texture == TextureSpecifier::SingleTexture) { std::cout << "Single Texture"	<< std::endl; }
	else if (texture == TextureSpecifier::MultiTexture)  { std::cout << "Multiple Textures" << std::endl; }

	std::cout << "Normals:\t";
	if		(normals == NormalSpecifier::LoadNormals)	   { std::cout << "Load Normals"	 << std::endl; }
	else if (normals == NormalSpecifier::GenerateNormals) { std::cout << "Generate Normals" << std::endl; }

	std::cout << "Shaders:\t";
	if		(shaders == ShaderSpecifier::DefaultShading) { std::cout << "Default Shading" << std::endl; }
	else if (shaders == ShaderSpecifier::CustomShading)  { std::cout << "Custom Shading"  << std::endl; }

	if		(type == TerrainType::FromImage) return std::unique_ptr<GameObject>(new terrain::FaultLineTerrain());
	else if (type == TerrainType::FaultLine) return std::unique_ptr<GameObject>(new terrain::FaultLineTerrain());
}
/*
template <>
inline std::unique_ptr<GameObject> GameAssetFactory::Create<Model>(AssetType type, TextureSpecifier texture, NormalSpecifier normals, ShaderSpecifier shaders) {
	std::cout << "Asset Type:\tModel" << std::endl;

	std::cout << "Asset Subtype:\t";
	if		(type == Model::Default)  { std::cout << "Default" << std::endl; }
	else if (type == Model::Animated) { std::cout << "Animated" << std::endl; }

	std::cout << "Texture:\t";
	if		(texture == TextureSpecifier::Color) { std::cout << "No Texture" << std::endl; }
	else if (texture == TextureSpecifier::SingleTexture) { std::cout << "Single Texture" << std::endl; }
	else if (texture == TextureSpecifier::MultiTexture) { std::cout << "Multiple Textures" << std::endl; }

	std::cout << "Normals:\t";
	if		(normals == NormalSpecifier::LoadNormals) { std::cout << "Load Normals" << std::endl; }
	else if (normals == NormalSpecifier::GenerateNormals) { std::cout << "Generate Normals" << std::endl; }

	std::cout << "Shaders:\t";
	if		(shaders == ShaderSpecifier::DefaultShading) { std::cout << "Default Shading" << std::endl; }
	else if (shaders == ShaderSpecifier::CustomShading) { std::cout << "Custom Shading" << std::endl; }

	if		(type == Model::Default)  return Model::Default;
	else if (type == Model::Animated) return Model::Animated;
}
*/
#endif	/* ZENENGINE_CORE_ASSET_FACTORY_GAMEASSETFACTORY_H_ */

