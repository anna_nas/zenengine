//
//  Model_Abstract.h
//  AssetFactory
//
//  Created by Brianna Whitcher
//

#ifndef ZENENGINE_CORE_ASSET_FACTORY_MODEL_DETAIL_MODEL_ABSTRACT_H_
#define ZENENGINE_CORE_ASSET_FACTORY_MODEL_DETAIL_MODEL_ABSTRACT_H_

//#include "GameObject_Abstract.h"
#include "GameObject_Abstract_Test.h" // added by Sava

namespace gameobject {
	class ObjectType;
	class Model : public GameObject {
	protected:
		using GameObject::m_Transform;
			
		//using GameObject::m_PositionData; /// Center of the terrain
		//using GameObject::m_Rotation;
		//using GameObject::m_Scale;

		using GameObject::m_VertexArray;
		using GameObject::m_VertexBuffer;
		using GameObject::m_VertexBufferLayout;
			
		using GameObject::m_Texture;
	
		using GameObject::m_Shader;

		//using GameObject::m_VertexPositionData;

	public:

		using GameObject::Draw;
		using GameObject::DebugDraw;

		//using GameObject::GetModelMatrix;

		//using GameObject::GetPositionData;
		using GameObject::GetTextures;

		using GameObject::GetPosition;
		using GameObject::GetRotation;
		using GameObject::GetScale;

		using GameObject::GetTransform;
		using GameObject::SetTransform;

		using GameObject::SetPosition;
		using GameObject::SetPositionX;
		using GameObject::SetPositionY;
		using GameObject::SetPositionZ;

		using GameObject::SetRotation;
		using GameObject::SetRotationX;
		using GameObject::SetRotationY;
		using GameObject::SetRotationZ;

		using GameObject::SetScale;
		using GameObject::SetScaleX;
		using GameObject::SetScaleY;
		using GameObject::SetScaleZ;
	};
}; /* model */

#endif /* ZENENGINE_CORE_ASSET_FACTORY_MODEL_MODEL_H_ */