#ifndef ZENENGINE_CORE_ASSET_FACTORY_GAMEASSETFACTORY_MODEL_H_
#define ZENENGINE_CORE_ASSET_FACTORY_GAMEASSETFACTORY_MODEL_H_

#include "GameAssetFactory.h"

namespace gameobject {
	class ModelFactory : public GameAssetFactory {
	public:
			/*
			 * @brief Creates a model object.
			 * @param[in] filepath A string that stores the relative location of a file for the model to be generated from.
			 * @param[in] texture An existing bundle of textures for the model.
			 * @param[in] shader An existing shader for the rendering.
			 * @return A unique pointer to the newly created model object.
			 */
		std::unique_ptr<GameObject> Create(std::string filepath, TextureBundle & texture, Shader & shader);

			/*
			 * @brief Creates a model object.
			 * @param[in] texture An existing bundle of textures for the model.
			 * @param[in] shader An existing shader for the rendering.
			 * @return A unique pointer to the newly created model object.
			 * @bug Does not create any model or use any of the provided inputs, and only returns 'nullptr'.
			 */
		std::unique_ptr<GameObject> Create(TextureBundle & texture, Shader & shader) { return nullptr; }
	};
};

#endif