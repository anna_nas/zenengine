//
//  GameObject.h
//  GameAssetDetail
//
//  Created by Brianna Whitcher
//

#ifndef GAMEOBJECT_H_
#define GAMEOBJECT_H_

//#include "GameObject_Model_Animated.h" ->when it exists
#include "GameObject_Model_Default.h"
#include "GameObject_Terrain_FaultLine.h"
#include "GameObject_Terrain_FromImage.h"

#endif	/* GameObject_h */