//
//  Terrain_FromImage.h
//  AssetFactory
//
//  Created by Brianna Whitcher
//

#ifndef ZENENGINE_CORE_ASSET_FACTORY_TERRAIN_DETAIL_TERRAIN_FROMIMAGE_H_
#define ZENENGINE_CORE_ASSET_FACTORY_TERRAIN_DETAIL_TERRAIN_FROMIMAGE_H_
/*
#include "Terrain_Abstract.h"

namespace terrain {
	class BruteForceTerrain : public Terrain {
	protected:
		using Terrain::m_Position;	//TODO: Referring to the center of the terrain
		using Terrain::m_Rotation;
		using Terrain::m_Scale;

		using Terrain::m_IndexBuffer;
		using Terrain::m_VertexBuffer;
		using Terrain::m_VertexBufferLayout;

		using Terrain::m_Vertices;
		using Terrain::m_Indices;

		//TEMP//
		using Terrain::m_VertexArray;

		using Terrain::m_VertexCount_X;
		using Terrain::m_VertexCount_Z;
		using Terrain::m_Step_X;
		using Terrain::m_Step_Z;

	public:
		BruteForceTerrain(std::string filepath, 
			float x_meters = 1000, float z_meters = 1000,
			unsigned int x_verts = 100, unsigned int z_verts = 100) {
			m_VertexCount_X = x_verts;
			m_VertexCount_Z = z_verts;
			m_Step_X = x_meters / static_cast<float>(m_VertexCount_X);
			m_Step_Z = z_meters / static_cast<float>(m_VertexCount_Z);

			GenerateVertexData();
			GenerateHeightData();
			GenerateIndexData();

			m_VertexBuffer.SetBufferData(&m_Vertices.front(), sizeof(Vertex) * m_Vertices.size());
			m_VertexBufferLayout.Push<float>(3);
			m_VertexBufferLayout.Push<float>(2);
			m_VertexBufferLayout.Push<float>(3);

			//TEMP
			m_VertexArray.AddBuffer(m_VertexBuffer, m_VertexBufferLayout);

			m_IndexBuffer.SetBufferData(&m_Indices.front(), m_Indices.size());
		}

		using Terrain::Draw;
		using Terrain::GetHeightAt;

	protected:
		virtual void GenerateHeightData() { }

		virtual void GenerateVertexData() {
			for (unsigned int i = 0; i < m_VertexCount_X * m_VertexCount_Z; i++) {
				m_Vertices.emplace_back(
					m_Position.x + m_Step_X * (i % m_VertexCount_X),
					m_Position.y,
					m_Position.z + m_Step_Z * (i - i % m_VertexCount_X) / m_VertexCount_X,
					0.5f, 0.5f,
					0.0f, 0.0f, 1.0f);
			}
		}

		virtual void GenerateIndexData() {
			int offset = 0;
			for (unsigned int i = 0; i < (m_VertexCount_X + 1) * (m_VertexCount_Z - 1); i++) {
				if (i == (m_VertexCount_X + 1) * (m_VertexCount_Z - 1) - 1) {
					offset++;
					m_Indices.push_back(i + m_VertexCount_X - offset);
					m_Indices.push_back(i + m_VertexCount_X - offset);
				}
				else if (i % (m_VertexCount_X + 1) >= m_VertexCount_X) {
					offset++;
					m_Indices.push_back(i + m_VertexCount_X - offset);
					m_Indices.push_back(i - offset + 1);
				}
				else {
					m_Indices.push_back(i - offset);
					m_Indices.push_back(i + m_VertexCount_X - offset);
				}
			}
		}
	};
}; /* terrain */
#endif /* ZENENGINE_CORE_ASSET_FACTORY_TERRAIN_DETAIL_TERRAIN_FROMIMAGE_H_ */