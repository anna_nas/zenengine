//
//  OBJLoader.h
//
//  Created by Brianna Whitcher on 13/2/18.
//

#ifndef ZENENGINE_CORE_ASSET_FACTORY_OBJLOADER_H_
#define ZENENGINE_CORE_ASSET_FACTORY_OBJLOADER_H_

#if defined(_MSC_VER) && _MSC_VER >= 1400
#define USING_DEPRECIATED_STDIO
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(push)
#pragma warning(disable:4996)
#endif

#include <cstddef>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstring>
#include <vector>
#include <iostream>

#include <fstream>
#include <sstream>
#include <iostream>

#include "../../rendering/render.h"
/*
bool LoadOBJ(std::string filename, std::vector<Vertex> *vertices) {
	size_t size = 0;
	const char * filepath = filename.c_str();
	char *data = nullptr;

	FILE *fp = fopen(filepath, "rb");
	if (!fp) {
		printf("The file could not be opened.");
		return false;
	}

	struct stat filestatus;
	stat(filepath, &filestatus);
	size = filestatus.st_size + 1;

	data = new char[size];
	if (!data) { 
		fclose(fp); 
		return false;
	}

	if (fread(data, size - 1, 1, fp) != 1) {
		free(data);
		data = nullptr;
		printf("The file could not be read.");
		fclose(fp);
		return false;
	}

	fclose(fp);

	std::vector<gem::vec3f> positions(4096);
	std::vector<gem::vec2f> uvs(4096);
	std::vector<gem::vec3f> normals(4096);

	float x, y, z;

	data[size - 1] = '\0';

	char * token = strtok(data, " /\n");
	
	while (token != NULL) {
		if (strcmp("vt", token) == 0) { 
			token = strtok(NULL, " /\n");
			x = atof(token);
			
			token = strtok(NULL, " /\n");
			y = atof(token);

			uvs.emplace_back(x, y); 
		}

		else if (strcmp("vn", token) == 0) {
			x = atof(strtok(NULL, " /\n"));
			y = atof(strtok(NULL, " /\n"));
			z = atof(strtok(NULL, " /\n"));

			normals.emplace_back(x, y, z);
		}

		else if (strcmp("v", token) == 0) {
			token = strtok(NULL, " /\n");
			x = atof(token);

			token = strtok(NULL, " /\n");
			y = atof(token);

			token = strtok(NULL, " /\n");
			z = atof(token);

			positions.emplace_back(x, y, z);
		}

		else if (strcmp("f", token) == 0) {
			vertices->emplace_back(
				positions.at(atoi(strtok(NULL, " /\n"))), 
				uvs.at(atoi(strtok(NULL, " /\n"))), 
				normals.at(atoi(strtok(NULL, " /\n")))
			);

			vertices->emplace_back(
				positions.at(atoi(strtok(NULL, " /\n"))),
				uvs.at(atoi(strtok(NULL, " /\n"))),
				normals.at(atoi(strtok(NULL, " /\n")))
			);

			vertices->emplace_back(
				positions.at(atoi(strtok(NULL, " /\n"))),
				uvs.at(atoi(strtok(NULL, " /\n"))),
				normals.at(atoi(strtok(NULL, " /\n")))
			);
		}

		else {
			printf("%s", token);
		}

		token = strtok(NULL, " /\n");
	}
   
	for (int i = 0; i < positions.size(); i++) {
		printf("%f, ", positions[i].x);
		printf("%f, ", positions[i].y);
		printf("%f, ", positions[i].z);
	}

	free(data);

	return true;
}

*/
enum Verbosity { QUIET, BRIEF, DETAILED };

/*
bool LoadOBJ(std::string filepath, std::vector<Vertex> *vertices, Verbosity verbosity) {
	float x, y, z;
	int line_count = 0;
	unsigned int index[9];

	FILE * fp;
	fopen_s(&fp, filepath.c_str(), "r");

	if(fp == nullptr) {
		std::cout << "Error: Could not open obj file '" << filepath << "'" << std::endl;
		return false;
	}

	char line[128];
	char * word;
	char * remainder;

	std::vector<float> positions;
	std::vector<float> uvs;
	std::vector<float> normals;

	if (verbosity != QUIET) { std::cout << "Reading File: " << filepath << std::endl; }

	while (fgets(line, 128, fp)) {
		line_count++;
		std::cout << line;

		if (verbosity == DETAILED) { std::cout << "\n\t" << line_count << "\t" << line; }

		word = strtok_s(line, " \t\0\n,/", &remainder);
		std::cout << word << "\n\n\n";

		//x = y = z = 0;
		/*
		if (word == "vn") {
			strtok_s(line, " ", &word);
			if (IsNumerical(word)) { x = stof(std::string(word)); }

			strtok_s(line, " ", &word);
			if (IsNumerical(word)) { y = stof(std::string(word)); }

			strtok_s(line, " ", &word);
			if (IsNumerical(word)) { z = stof(std::string(word)); }

			if (verbosity == BRIEF) {
				std::cout << "\t-->" << x << ", " << y << ", " << z;
			}

			normals.push_back(x);
			normals.push_back(y);
			normals.push_back(z);
		}

		else if (word == "vt") {
			strtok_s(line, " ", &word);
			if (IsNumerical(word)) { x = stof(std::string(word)); }

			strtok_s(line, " ", &word);
			if (IsNumerical(word)) { y = stof(std::string(word)); }

			if (verbosity == DETAILED) {
				std::cout << "\t-->\t" << x << ", " << y;
			}

			uvs.push_back(x);
			uvs.push_back(y);
		}

		else if (word == "v") {
			strtok_s(line, " ", &word);
			if (IsNumerical(word)) { x = stof(std::string(word)); }

			strtok_s(line, " ", &word);
			if (IsNumerical(word)) { y = stof(std::string(word)); }

			strtok_s(line, " ", &word);
			if (IsNumerical(word)) { z = stof(std::string(word)); }

			if (verbosity == DETAILED) {
				std::cout << "\t-->\t" << x << ", " << y << ", " << z;
			}

			positions.push_back(x);
			positions.push_back(y);
			positions.push_back(z);
		}

		else if (word == "f") {
			std::regex face_expression("(f)( ([[:digit:]]+(\/)){2}[[:digit:]]+){3}");
			if (!std::regex_match(line, face_expression)) {
				std::cout << "Vertex format unsupported."
					<< "Required format: Position (x, y, z) Texture (u, v) Normal (x, y, z)\n" << std::endl;
				return false;
			}

			strtok_s(line, "/", &word);
			if (IsNumerical(word)) { index[0] = stof(std::string(word)); }

			strtok_s(line, "/", &word);
			if (IsNumerical(word)) { index[1] = stof(std::string(word)); }

			strtok_s(line, " ", &word);
			if (IsNumerical(word)) { index[2] = stof(std::string(word)); }

			strtok_s(line, "/", &word);
			if (IsNumerical(word)) { index[3] = stof(std::string(word)); }

			strtok_s(line, "/", &word);
			if (IsNumerical(word)) { index[4] = stof(std::string(word)); }

			strtok_s(line, " ", &word);
			if (IsNumerical(word)) { index[5] = stof(std::string(word)); }

			strtok_s(line, "/", &word);
			if (IsNumerical(word)) { index[6] = stof(std::string(word)); }

			strtok_s(line, "/", &word);
			if (IsNumerical(word)) { index[7] = stof(std::string(word)); }

			strtok_s(line, "\n", &word);
			if (IsNumerical(word)) { index[8] = stof(std::string(word)); }

			for (int i = 0; i < 9; i += 3) {
				vertices->emplace_back(
					positions[3 * (index[i] - 1) + 0],
					positions[3 * (index[i] - 1) + 1],
					positions[3 * (index[i] - 1) + 2],
					uvs[2 * (index[i + 1] - 1) + 0],
					uvs[2 * (index[i + 1] - 1) + 1],
					normals[3 * (index[i + 2] - 1) + 0],
					normals[3 * (index[i + 2] - 1) + 1],
					normals[3 * (index[i + 2] - 1) + 2]);
			}
		}

		else if (word == "mtllib") { }
		else if (word == "usemtl") { }
		else { }

	}

	if (verbosity != QUIET) {
		std::cout << "File Summary: " << filepath	 << "\n"
			<< "  * Lines:\t"	 << line_count		 << "\n"
			<< "  * Vertices:\t" << positions.size() << "\n"
			<< "  * UVs:\t"		 << uvs.size()		 << "\n"
			<< "  * Normals:\t"  << normals.size()	 << "\n";
	}

	return true;
}
*/
	/*!
		* @brief Attempts to load in an obj from a file.
		* @param[in] filepath A string declaration of the obj file location relative to the application.
		* @param[out] vertices A pointer to a vector of vertices, where the obj data is stored.
		* @param[in] verbosity A selection of the feedback/logging regarding the data reading/storage.
		* @return Returns the success as a boolean. (true = successful, false = unsuccessful)
		*/
bool LoadOBJ(std::string filepath, std::vector<Vertex> *vertices, Verbosity verbosity) {
	float x, y, z;
	int line_count = 0;
	unsigned int index[9] = { 0 };

	std::ifstream stream(filepath);
	std::string line, word;
	std::stringstream ss;

	std::vector<float> positions;
	std::vector<float> uvs;
	std::vector<float> normals;


	if (!stream.is_open()) {
		std::cout << "Error: Could not open obj file '" << filepath << "'" << std::endl;
		return false;
	}

	if (verbosity != QUIET) { std::cout << "Reading File: " << filepath << std::endl; }
	
	while (getline(stream, line)) {
		line_count++;
		ss << line;

		if (verbosity == DETAILED) { std::cout << "\n\t" << line_count << "\t" << line; }

		while (getline(ss, word, ' ')) {
			x = y = z = 0;

			if (word == "vn") {
				getline(ss, word, ' ');
				x = stof(word);

				getline(ss, word, ' ');
				y = stof(word);

				getline(ss, word, ' ');
				z = stof(word);

				if (verbosity == DETAILED) {
					std::cout << "\t-->" << x << ", " << y << ", " << z;
				}

				normals.push_back(x);
				normals.push_back(y);
				normals.push_back(z);
			}

			else if (word == "vt") {
				getline(ss, word, ' ');
				x = stof(word);

				getline(ss, word, ' ');
				y = stof(word);

				if (verbosity == DETAILED) {
					std::cout << "\t-->\t" << x << ", " << y;
				}

				uvs.push_back(x);
				uvs.push_back(y);
			}

			else if (word == "v") {
				getline(ss, word, ' ');
				x = stof(word);

				getline(ss, word, ' ');
				y = stof(word);

				getline(ss, word, ' ');
				z = stof(word);

				if (verbosity == DETAILED) {
					std::cout << "\t-->\t" << x << ", " << y << ", " << z;
				}

				positions.push_back(x);
				positions.push_back(y);
				positions.push_back(z);
			}

			else if (word == "f") {
				getline(ss, word, '/');
				index[0] = stoi(word);

				getline(ss, word, '/');
				index[1] = stoi(word); 

				getline(ss, word, ' ');
				index[2] = stoi(word);

				getline(ss, word, '/');
				index[3] = stoi(word);

				getline(ss, word, '/');
				index[4] = stoi(word);

				getline(ss, word, ' ');
				index[5] = stoi(word);

				getline(ss, word, '/');
				index[6] = stoi(word);

				getline(ss, word, '/');
				index[7] = stoi(word);

				getline(ss, word, '\n');
				index[8] = stoi(word);

				for (int i = 0; i < 9; i += 3) {
					vertices->emplace_back(
						positions[3 * (index[i] - 1) + 0],
						positions[3 * (index[i] - 1) + 1],
						positions[3 * (index[i] - 1) + 2],
						uvs[2 * (index[i + 1] - 1) + 0],
						uvs[2 * (index[i + 1] - 1) + 1],
						normals[3 * (index[i + 2] - 1) + 0],
						normals[3 * (index[i + 2] - 1) + 1],
						normals[3 * (index[i + 2] - 1) + 2]);
				}
			}

			else if (word == "mtllib") {}
			else if (word == "usemtl") {}
			else {}
		}


		ss.clear();
	}

	if (verbosity != QUIET) {
		std::cout << "File Summary: " << filepath << "\n"
			<< "  * Lines:\t" << line_count << "\n"
			<< "  * Vertices:\t" << positions.size() << "\n"
			<< "  * UVs:\t" << uvs.size() << "\n"
			<< "  * Normals:\t" << normals.size() << "\n";
	}

	return true;
}

#ifdef USING_DEPRECIATED_STDIO
#pragma warning(pop)
#endif

#endif /* ZENENGINE_CORE_ASSET_FACTORY_OBJLOADER_H_ */