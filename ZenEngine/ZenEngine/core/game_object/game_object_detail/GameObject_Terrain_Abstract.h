//
//  Terrain_Abstract.h
//  AssetFactory
//
//  Created by Brianna Whitcher
//

#ifndef ZENENGINE_CORE_ASSET_FACTORY_TERRAIN_DETAIL_TERRAIN_ABSTRACT_H_
#define ZENENGINE_CORE_ASSET_FACTORY_TERRAIN_DETAIL_TERRAIN_ABSTRACT_H_

//#include "GameObject_Abstract.h"
#include "GameObject_Abstract_Test.h" // added by Sava

#include <vector>

namespace gameobject {
	class ObjectType;
	class Terrain : public GameObject {
	protected:
		using GameObject::m_Transform;

		//using GameObject::m_PositionData; /// Center of the terrain
		//using GameObject::m_Rotation;
		//using GameObject::m_Scale;

		using GameObject::m_VertexArray;
		using GameObject::m_VertexBuffer;
		using GameObject::m_VertexBufferLayout;		
		using GameObject::m_Texture;
		using GameObject::m_Shader;

		//using GameObject::m_VertexPositionData;

		IndexBuffer m_IndexBuffer;	

		float m_Length;
		float m_Step;
		float m_MinHeight, m_MaxHeight;
		unsigned int m_VertexCount;

		std::vector<unsigned char> m_HeightField;
		std::vector<float> m_HeightFieldf;
		float m_HeightScale;

	public:
		using GameObject::Draw;
		using GameObject::DebugDraw;
		//using GameObject::GetModelMatrix;

		//using GameObject::GetPositionData;
		using GameObject::GetTextures;

		using GameObject::GetPosition;
		using GameObject::GetRotation;
		using GameObject::GetScale;

		using GameObject::GetTransform;
		using GameObject::SetTransform;

		using GameObject::SetPosition;
		using GameObject::SetPositionX;
		using GameObject::SetPositionY;
		using GameObject::SetPositionZ;

		using GameObject::SetRotation;
		using GameObject::SetRotationX;
		using GameObject::SetRotationY;
		using GameObject::SetRotationZ;

		using GameObject::SetScale;
		using GameObject::SetScaleX;
		using GameObject::SetScaleY;
		using GameObject::SetScaleZ;

			/*!
			 * @brief Returns the relative height of a position to the terrain.
			 * @param[in] position A position (3 axis).
			 * @return A float representation difference in height from the tarrain to the provided position.
			 */
		virtual float GetHeightAt(gem::vec3f position) = 0;

	protected:
			/*!
			 * @brief Generates the vertex buffer for the object rendering.
			 * @return N/a.
			 */
		virtual void GenerateVertexBuffer() = 0;

			/*!
			 * @brief Generates the index buffer for the object rendering.
			 * @return N/a.
			 */
		virtual void GenerateIndexBuffer()  = 0;
	};
}; /* terrain */

#endif /* ZENENGINE_CORE_ASSET_FACTORY_TERRAIN_DETAIL_TERRAIN_ABSTRACT_H_ */