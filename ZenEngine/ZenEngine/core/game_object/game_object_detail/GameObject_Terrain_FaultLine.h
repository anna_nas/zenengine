//
//  Terrain_FaultLine.h
//  GameAssetFactory
//
//  Created by Brianna Whitcher
//

/**
 * @todo Make use of scale instead fo m_Length and m_HeightScale
 */

#ifndef Terrain_FaultLine_h
#define Terrain_FaultLine_h

#include "GameObject_Terrain_Abstract.h"

namespace gameobject {
	class FaultLineTerrain : public Terrain {
	protected:
		using Terrain::m_Transform;

		// using Terrain::m_Position;	//TODO: Referring to the center of the terrain
		// using Terrain::m_Rotation;
		// using Terrain::m_Scale;

		using Terrain::m_VertexArray;
		using Terrain::m_VertexBuffer;
		using Terrain::m_VertexBufferLayout;
		using Terrain::m_IndexBuffer;
		using Terrain::m_Texture;
		using Terrain::m_Shader;
		//using Terrain::m_VertexPositionData;

		using Terrain::m_Length;
		using Terrain::m_Step;
		using Terrain::m_VertexCount;

		using Terrain::m_HeightField;
		using Terrain::m_HeightScale;

	public:
		virtual void CreatePhysicsBody();
			/*!
			 * @brief Class constructor, requiring user-provided initialization values.
			 * @param[in] texture Existing texture-bundle object for the dynamic texturing of the terrain, passed by reference.
			 * @param[in] shader Existing shader for the rendering, passed by reference.
			 * @param[in] height_scale Ratio/scaling of the terrain's height.
			 * @param[in] length_meters The length of the x & z axis of the terrain.
			 * @param[in] steps The amount of vertexes to be generated/stored.
			 * @return N/a.
			 */
		FaultLineTerrain(TextureBundle & texture, Shader & shader,
			float height_scale = 1, float length_meters = 1000, unsigned int steps = 128);
		
			/*!
			 * @brief Renders the terrain object.
			 * @param[in] Projection A glm::mat4 object.
			 * @param[in] View A glm::mat4 object.
			 * @return N/a.
			 */
		virtual void Draw(glm::mat4 Projection, glm::mat4 View, gem::vec3f camera);

			/*!
			 * @brief Renders the terrain in a debug mode (wireframe).
			 * @param[in] Projection A glm::mat4 object.
			 * @param[in] View A glm::mat4 object.
			 * @return N/a.
			 */
		virtual void DebugDraw(glm::mat4 Projection, glm::mat4 View, gem::vec3f camera);

			/*!
			 * @brief Returns the relative height of a position to the terrain.
			 * @param[in] position A position (3 axis).
			 * @return A float representation difference in height from the tarrain to the provided position.
			 */
		virtual float GetHeightAt(gem::vec3f position);

			/*!
			 * @brief Returns the position of the terrain.
			 * @return Returns the terrain location relative to the environment (in a vec3).
			 */
		gem::vec3f GetPosition();

	protected:
			/*!
			 * @brief Generates the vertex buffer for the object rendering.
			 * @return N/a.
			 */
		virtual void GenerateVertexBuffer();

			/*!
			 * @brief Generates the index buffer for the object rendering.
			 * @return N/a.
			 */
		virtual void GenerateIndexBuffer();	

			/*!
			 * @brief Generates the positions of the terrain vertices.
			 * @param[in] positions A vector of vertice positions.
			 * @return N/a.
			 * @bug Currently commented out and does nothing.
			 */
		virtual void GeneratePositions(std::vector<gem::vec3f> & positions);

			/*!
			 * @brief Generates the terrain object's UV mappings.
			 * @param[in] uvs A vector of vec2's.
			 * @return N/a.
			 * @bug Currently lacks implementation.
			 */
		virtual void GenerateUVs(std::vector<gem::vec2f> & uvs);
			
			/*!
			 * @brief Generates the terrain object's normal mappings.
			 * @param[in] normals A vector of vec3's.
			 * @return N/a.
			 * @bug Currently lacks implementation.
			 */
		virtual void GenerateNormals(std::vector<gem::vec3f> & normals);

			/*!
			 * @brief Generates the texture of the terrain.
			 * @return N/a.
			 * @bug Currently lacks implementation.
			 */
		virtual void GenerateTexture();

			/*!
			 * @brief Generates the height-field of the terrain.
			 * @return N/a.
			 */
		void GenerateHeightField();

			/*!
			 * @brief Processes the height-field, providing a 'sample' of a generated height-field at a provided point.
			 * @param[in] x X-axis position to sample.
			 * @param[in] z Z-axis position to sample.
			 * @return A height 'sample' at the requested heightmap location.
			 */
		float SampleHeightField(unsigned int x, unsigned int z);
		
			/*!
			 * @brief Normalizes a height-map provided.
			 * @param[in, out] raw_height_data A height-map to be normalized.
			 * @return N/a.
			 */
		void NormaliseRawHeightData(std::vector<float> & raw_height_data);
		
			/*!
			 * @brief Filters a height-map provided.
			 * @param[in, out] raw_height_data A height-map to apply a filter to.
			 * @param[in] weight The weighting of the filter to the height-map.
			 * @return N/a.
			 */
		void AddFilter(std::vector<float> & raw_height_data, float weight);
	};
};
#endif	/* Terrain_FaultLine_h */