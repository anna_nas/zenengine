#ifndef GAMEOBJECT_ABSTRACT_TEST_H_
#define GAMEOBJECT_ABSTRACT_TEST_H_

#include "../../rendering/render.h"
#include "../../physics/BulletPhysics.h"
#include "../../physics/ZenTransform.h"
#include "../../physics/RigidBody.h"
#include "..\..\AI\ZenAI.h"

namespace gameobject {
	class GameObject {
	protected:
		physics::ZenTransform m_Transform;
		physics::RigidBody* m_Body;

		gem::vec3f velocity;

		zenai::Movement movement;
		zenai::StateMachine<GameObject>* stateMachine;
		float currentAITime;

		VertexArray		    m_VertexArray;
		VertexBuffer	    m_VertexBuffer;
		VertexBufferLayout  m_VertexBufferLayout;

		TextureBundle m_Texture;
		Shader m_Shader;

		//std::vector<glm::vec3>  m_VertexPositionData;

	public:

		/*
		* @brief Constructor
		*/
		GameObject() {
			m_Transform.SetPosition(gem::vec3f(0.0f));
			m_Transform.SetRotation(gem::vec3f(0.0f));
			m_Transform.SetScale(gem::vec3f(1.0f));
			velocity = gem::vec3f(1.0f, 0.0f, 0.0f);
			currentAITime = 0.0f;

			stateMachine = new zenai::StateMachine<GameObject>(this);
		}


		~GameObject() {
			delete stateMachine;
		}

		void DoAI(float time) {
			currentAITime = time;
			stateMachine->Update();
		}

		void SetState(const std::string& stateName) {
			stateMachine->ChangeState(stateName);
		}

		void MoveTo(const gem::vec3f& target) {
			gem::vec3f tempPos = m_Transform.GetPosition();
			zenai::Movement::MoveTo(tempPos, velocity, target, currentAITime, 0);
			m_Transform.SetPosition(tempPos);
		}

		void SetPatrol(const gem::vec3f& startingPos, float xLen, float yLen) {
			movement.SetPatrolPath(startingPos, xLen, yLen);
		}

		void Patrol() {
			gem::vec3f tempPos = m_Transform.GetPosition();
			movement.Patrol(tempPos, velocity, currentAITime);
			m_Transform.SetPosition(tempPos);
		}

		virtual void CreatePhysicsBody() = 0;

		virtual void RegisterPhysicsBody(physics::BulletPhysics* world) { 
			world->AddPhysicsBody(m_Body, this); 
		};

		/*
		* @brief Renders the object.
		* @param[in] Projection A projection matrix for the rendering.
		* @param[in] View A view matrix for the rendering.
		* @return N/a.
		*/
		virtual void Draw(glm::mat4 Projection, glm::mat4 View, gem::vec3f camera) = 0;

		/*
		* @brief Renders the object in debug mode. (wireframe)
		* @param[in] Projection A projection matrix for the rendering.
		* @param[in] View A view matrix for the rendering.
		* @return N/a.
		*/
		virtual void DebugDraw(glm::mat4 Projection, glm::mat4 View, gem::vec3f camera) = 0;

		//virtual glm::mat4 GetModelMatrix() = 0;

		/*
		* @brief Returns the VERTEX position of the object.
		* @return A vec3 representing the object's position in space.
		*/
		//virtual std::vector<gem::vec3f> & GetPositionData() { return m_VertexPositionData; }

		/*
		* @brief Returns the object's textures.
		* @return A collection of textures (TextureBundle).
		*/
		virtual TextureBundle & GetTextures() { return m_Texture; }

		/*
		* @brief Returns the transformation of the object as a whole.
		* @return A ZenTransform representing the object's position, rotation, and scale.
		*/
		virtual physics::ZenTransform GetTransform() { return m_Transform; }

		/*
		* @brief Returns the position of the object as a whole.
		* @return A vec3 representing the object's position in space.
		*/
		virtual gem::vec3f GetPosition() { return m_Transform.GetPosition(); }

		/*
		* @brief Returns the object's rotation.
		* @return A vec3 representing the object's rotation in space.
		*/
		virtual gem::vec3f GetRotation() { return m_Transform.GetRotation(); }

		/*
		* @brief Returns the object's size scaling.
		* @return A collection of the scaling for each axis of the object.
		*/
		virtual gem::vec3f GetScale() { return m_Transform.GetScale(); }

		/*
		* @brief Sets the transform of the object
		* @param[in] trasnform A new transform that the object will take.
		* @return N/a.
		*/
		virtual void SetTransform(const physics::ZenTransform& transform) { m_Transform = transform; }

		/*
		* @brief Sets the position of the object
		* @param[in] positon A new position that the object will take.
		* @return N/a.
		*/
		virtual void SetPosition(gem::vec3f position) { m_Transform.SetPosition(position); }

		/*
		* @brief Sets the position of the object, without the use of a vec3 object.
		* @param[in] x A positon for the the object to take along the x-axis.
		* @param[in] y A positon for the the object to take along the y-axis.
		* @param[in] z A positon for the the object to take along the z-axis.
		* @return N/a.
		*/
		virtual void SetPosition(float x, float y, float z) { m_Transform.SetPosition(gem::vec3f(x, y, z)); }

		/*
		* @brief Sets the position of the object along the x axis.
		* @param[in] x A positon for the the object to take along the x-axis.
		* @return N/a.
		*/
		virtual void SetPositionX(float x) { m_Transform.GetPosition().x = x; }

		/*
		* @brief Sets the position of the object along the x axis.
		* @param[in] y A positon for the the object to take along the y-axis.
		* @return N/a.
		*/
		virtual void SetPositionY(float y) { 
			gem::vec3f temp(m_Transform.GetPosition());
			temp.y = y;
			m_Transform.SetPosition(temp); 
		}

		/*
		* @brief Sets the position of the object along the x axis.
		* @param[in] z A positon for the the object to take along the z-axis.
		* @return N/a.
		*/
		virtual void SetPositionZ(float z) { m_Transform.GetPosition().z = z; }

		/*
		* @brief Sets the rotation of an object.
		* @param[in] rotation A vec3 that stores the rotations to be set.
		* @return N/a
		*/
		virtual void SetRotation(gem::vec3f rotation) { m_Transform.SetRotation(rotation); }

		/*
		* @brief Sets the rotation of the object, without the use of a vec3 object.
		* @param[in] x A rotation for the the object to take along the x-axis.
		* @param[in] y A rotation for the the object to take along the y-axis.
		* @param[in] z A rotation for the the object to take along the z-axis.
		* @return N/a.
		*/
		virtual void SetRotation(float x, float y, float z) { m_Transform.SetRotation(gem::vec3f(x, y, z)); }

		/*
		* @brief Sets the rotation of the object along the x axis.
		* @param[in] x A rotation for the the object to take along the x-axis.
		* @return N/a.
		*/
		virtual void SetRotationX(float x) { m_Transform.GetRotation().x = x; }

		/*
		* @brief Sets the rotation of the object along the y axis.
		* @param[in] y A rotation for the the object to take along the y-axis.
		* @return N/a.
		*/
		virtual void SetRotationY(float y) { m_Transform.GetRotation().y = y; }

		/*
		* @brief Sets the rotation of the object along the z axis.
		* @param[in] z A rotation for the the object to take along the z-axis.
		* @return N/a.
		*/
		virtual void SetRotationZ(float z) { m_Transform.GetRotation().z = z; }

		/*
		* @brief Sets the scaling of an object.
		* @param[in] scale A vec3 that stores the scalings to be set.
		* @return N/a
		*/
		virtual void SetScale(gem::vec3f scale) { m_Transform.SetScale(scale); }

		/*
		* @brief Sets the scaling of the object, without the use of a vec3 object.
		* @param[in] x A scaling for the the object to take along the x-axis.
		* @param[in] y A scaling for the the object to take along the y-axis.
		* @param[in] z A scaling for the the object to take along the z-axis.
		* @return N/a.
		*/
		virtual void SetScale(float x, float y, float z) { m_Transform.SetScale(gem::vec3f(x, y, z)); }

		/*
		* @brief Sets the scaling of the object along the x axis.
		* @param[in] x A scaling for the the object to take along the x-axis.
		* @return N/a.
		*/
		virtual void SetScaleX(float x) { m_Transform.GetScale().x = x; }

		/*
		* @brief Sets the scaling of the object along the y axis.
		* @param[in] y A scaling for the the object to take along the y-axis.
		* @return N/a.
		*/
		virtual void SetScaleY(float y) { m_Transform.GetScale().y = y; }

		/*
		* @brief Sets the scaling of the object along the z axis.
		* @param[in] z A scaling for the the object to take along the z-axis.
		* @return N/a.
		*/
		virtual void SetScaleZ(float z) { m_Transform.GetScale().z = z; }

		/*
		* @brief Returns the height of an object.
		* @param[in] position A vec3 position in 3 dimensional space.
		* @return A float representation of the height.
		* @note This is temporary!
		*/
		virtual float GetHeightAt(gem::vec3f position) = 0;

		// Physics Interface here
	};
};
#endif	/* GAMEOBJECT_ABSTRACT_TEST_H_ */