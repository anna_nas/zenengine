//
// LoadedModelCache.h
// GameObject Stuff
//
// Created by Brianna Whitcher
//

#ifndef LOADED_MODEL_CACHE_H_
#define LOADED_MODEL_CACHE_H_

#include <unordered_map>
#include <vector>
#include "../rendering/render.h"

namespace gameobject {
	class ModelAndImageryCache {
	protected:
		static std::unordered_map<std::string, std::vector<Vertex> > s_ModelCache;
		static std::unordered_map<std::string, unsigned int> s_TextureCache;
	
	public:
			/**
			 * @brief		Default constructor. Currently does nothing.
			 */
		ModelAndImageryCache() { }

			/**
			 * @brief		Gets the vertex data of a model from the cache.
			 * @param[in]	filepath	The file path of the model.
			 * @return		the vertex data of the model specified.
			 */
		const std::vector<Vertex> * GetModel(std::string filepath);
		
			/**
			 * @brief		Gets a texture from the cache.
			 * @param[in]	filepath	The file path of the texture.
			 * @param[out]	width		The width of the texture image.
			 * @param[out]	height		The height of the texture image.
			 * @param[out]	channels	The number of channels used to colout the texture.
			 * @return		The texture ID for the texture requested.
			 */
		unsigned int GetTexture(std::string filepath, int * width, int * height, int * channels);
	};
};

typedef gameobject::ModelAndImageryCache ModelCache;
typedef gameobject::ModelAndImageryCache TextureCache;

#endif