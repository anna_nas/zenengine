#ifndef ZENENGINE_CORE_GAMEOBJECT_OBJECTMANAGER_H_
#define ZENENGINE_CORE_GAMEOBJECT_OBJECTMANAGER_H_

#include "game_object_detail/GameAssetFactory.h"
#include "..\scripting\scripting_detail\LuaInterface.h"

namespace gameobject {

	class GameObjectManager {
	public:
		const std::vector<std::unique_ptr<GameObject>>* GetObjects() const { return &objs; } // added by Sava 

		GameObjectManager(std::string basic_shader_filepath, std::string texture_shader_filepath);
			/**
			 * @brief		Draws all objects.
			 * @param[in]	Projection	Current projection matrix of the camera.
			 * @param[in]	View		Current view matrix of the camera.
			 */
		void DrawObjs(glm::mat4 Projection, glm::mat4 View, gem::vec3f camera);

			/*!
			 * @brief Draws all objects in a wireframe mode. Otherwise identical to DrawObjs.
			 * @param[in]	Projection	Current projection matrix of the camera.
			 * @param[in]	View		Current view matrix of the camera.
			 */
		void DebugDrawObjs(glm::mat4 Projection, glm::mat4 View, gem::vec3f camera);

			/**
			 * @brief		Creates objects from a list of object details.
			 * @param[in]	objDetails	List of object detils to create objects from.
			 */
		void CreateObjects(std::vector<scripting::obj>& objDetails);

			/**
			 * @brief		Creates a single object from an object detail.
			 * @param[in]	objDetail	A set of details of one object that is to be created.
			 */
		void CreateObject(scripting::obj& objDetail);

		void DoAI();
		
			/// This is a temporary fix to get the terrain object out for the camera
		float GetTerrainHeightAt(gem::vec3f position) { return objs[0]->GetHeightAt(position); }

	private:
			/// The shader used for the terrain
		Shader terrain_shader;
			/// The shader used for the models
		Shader basic_shader;
			/// List of all GameObjects
		std::vector < std::unique_ptr<GameObject> > objs;

	};

} // namespace gameobject

#endif /* ZENENGINE_CORE_GAMEOBJECT_OBJECTMANAGER_H_ */
