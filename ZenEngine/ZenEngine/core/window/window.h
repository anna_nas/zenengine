//
//  Window.h
//  FolderName
//
//  Created by Brianna Whitcher
//
/*
#ifndef Window_h
#define Window_h

#define GLEW_STATIC
#include "glew.h"
#include "glfw3.h"
#include <iostream>

class Window {
private:
	GLFWwindow * m_Window;
	int m_frameBufferWidth;
	int m_frameBufferHeight;

public:
	Window(int width, int height, const char * title);
	~Window() { glfwTerminate(); }

	bool IsOpen();
	void SwapBuffers();
	void PollEvents();
	void CloseWindow();

	float GetAspectRatio() const { return (float)m_frameBufferWidth / (float)m_frameBufferHeight; }
};

inline Window::Window(int width, int height, const char * title)
{
	if (!glfwInit()) {
		std::cout << "Could not initialise GLFW" << std::endl;
		__debugbreak();
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); //For mac

	m_Window = glfwCreateWindow(width, height, title, nullptr, nullptr);

	if (!m_Window) {
		std::cout << "Failed to create window" << std::endl;
		glfwTerminate();
		__debugbreak();
	}

	glfwMakeContextCurrent(m_Window);

	glfwGetFramebufferSize(m_Window, &m_frameBufferWidth, &m_frameBufferHeight);

	glewExperimental = GL_TRUE;

	if (GLEW_OK != glewInit()) {
		std::cout << "Failed to initialise GLEW" << std::endl;
		__debugbreak();
	}

	glViewport(0, 0, m_frameBufferWidth, m_frameBufferHeight);
}

bool Window::IsOpen() { return !glfwWindowShouldClose(m_Window); }
void Window::SwapBuffers() { glfwSwapBuffers(m_Window); }
void Window::PollEvents() { glfwPollEvents(); }
void Window::CloseWindow() { glfwSetWindowShouldClose(m_Window, GL_TRUE); }
#endif /* Window_h */













//
//  Window.hpp
//  RenderingEngine
//
//  Created by Brianna Whitcher on 1/4/18.
//  Copyright � 2018 Brianna Whitcher. All rights reserved.
//

#ifndef Window_hpp
#define Window_hpp

//GLEW
#define GLEW_STATIC
#include "glew.h"

//GLFW
#include "glfw3.h"

#include "../camera/camera.h"

// !!! FOR TEMP USE ONLY - REPLACE ASAP !!!
#include "../input/header/Input_DemoHack.hpp"
float mouseSensitivity = 0.3f; // Change this to have more/less sensitive mouse input

Camera camera((float)2048 / (float)1536, gem::vec3f(0.0f, 1.8f, 0.0f));

float lastX = 1024 / 2, lastY = 768 / 2;
bool keys[1024];
bool firstMouse = true;
float deltaTime = 0.0f;
float lastFrame = 0.0f;

// !!! FOR TEMP USE ONLY - REPLACE ASAP !!!
unsigned char TEMPUSE_renderingTask;
bool TEMPUSE_wireframeStatus;

void KeyCallback(GLFWwindow *window, int key, int scancode, int action, int mode) {
	// !!! FOR TEMP USE ONLY - REPLACE ASAP !!!
	
	/*
		// Commented out for replacement

		if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) { glfwSetWindowShouldClose(window, GL_TRUE); }
	*/
	
	// Update the key tracking
	if (key >= 0 && key < 1024) {
		if (action == GLFW_PRESS) { keys[key] = true; }
		else
			if (action == GLFW_RELEASE) { keys[key] = false; }
	}

	// Prevent the exit key from doing bad stuff if it were to overlap with the controls display
	if (TEMPUSE_renderingTask == renderingMethod::Game_Demo)
	{
		// Manage the exit key, specifically for the provided window's functionality
		// Only allow the action to occur on button press, not hold
		if (key == GLFW_KEY_X && action == GLFW_PRESS) {
			TEMPUSE_renderingTask = renderingMethod::Screen_Splash;
		}
	}

	// Prevent the controls display from doing bad stuff if it were to overlap with the exit key
	if ((TEMPUSE_renderingTask == renderingMethod::Game_Demo) || (TEMPUSE_renderingTask == renderingMethod::Screen_Help))
	{
		// Manage the controls display key
		// Only allow the action to occur on button press, not hold
		if (key == GLFW_KEY_M && action == GLFW_PRESS) {
			// Check which rendering method to toggle
			if (TEMPUSE_renderingTask == renderingMethod::Game_Demo)
			{
				TEMPUSE_renderingTask = renderingMethod::Screen_Help;
			}
			else
			{
				TEMPUSE_renderingTask = renderingMethod::Game_Demo;
			}
		}
	}

	// Manage the wireframe rendering key
	// Only allow the action to occur on button press, not hold
	if (key == GLFW_KEY_K && action == GLFW_PRESS) {
		// Toggle the boolean
		if (TEMPUSE_wireframeStatus)
		{
			TEMPUSE_wireframeStatus = false;
		}
		else
		{
			TEMPUSE_wireframeStatus = true;
		}
	}
}

// !!! FOR TEMP USE ONLY - REPLACE ASAP !!!
void doMovement()
{
	// Only allow movement if the window allows it
	if (TEMPUSE_renderingTask == renderingMethod::Game_Demo)
	{
		// Manage the movement keys
		if (keys[GLFW_KEY_W] || keys[GLFW_KEY_UP]) { camera.Move(FORWARD, deltaTime); }
		if (keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN]) { camera.Move(BACKWARD, deltaTime); }
		if (keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT]) { camera.Move(LEFT, deltaTime); }
		if (keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT]) { camera.Move(RIGHT, deltaTime); }
	}
}

void MouseCallback(GLFWwindow *window, double xPos, double yPos) {
	// !!! FOR TEMP USE ONLY - REPLACE ASAP !!!
	
	// Prevent mouse motions if anything other than the environment is being displayed
	if (TEMPUSE_renderingTask == renderingMethod::Game_Demo)
	{
		if (firstMouse) {
			lastX = static_cast<float>(xPos * mouseSensitivity);
			lastY = static_cast<float>(yPos * mouseSensitivity);
			firstMouse = false;
		}

		float xOffset = static_cast<float>(xPos * mouseSensitivity) - lastX;
		float yOffset = lastY - static_cast<float>(yPos * mouseSensitivity);  // Reversed since y-coordinates go from bottom to left

		lastX = static_cast<float>(xPos * mouseSensitivity);
		lastY = static_cast<float>(yPos * mouseSensitivity);

		camera.Rotate(xOffset, yOffset);
	}
}

void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
	// !!! FOR TEMP USE ONLY - REPLACE ASAP !!!
	
	// Only allow the program to quit on program exit
	if (TEMPUSE_renderingTask == renderingMethod::Screen_Splash)
	{
		if ((button == GLFW_MOUSE_BUTTON_LEFT) && (action == GLFW_PRESS))
		{
			// Make the window close
			glfwSetWindowShouldClose(window, GL_TRUE);
		}
	}
}

void ScrollCallback(GLFWwindow *window, double xOffset, double yOffset) {
	// !!! FOR TEMP USE ONLY - REPLACE ASAP !!!
	
	/*
		// Camera zooming is currently 'broken'.

		camera.Zoom(static_cast<float>(yOffset));
	*/
}

class Window {
private:
	GLFWwindow * m_Window;
	int m_frameBufferWidth;
	int m_frameBufferHeight;

public:
		/**
		 * @brief		Constructor. Sets up a window with the specified width, height, and title.
		 * @param[in]	width	The desired width of the window.
		 * @param[in]	height	The desired height of the window.
		 * @param[in]	title	The title of the window.
		 */
	Window(int width, int height, const char * title);

		/**
		 * @brief		Destructor. Cleans up when window is closed.
		 */
	~Window() { glfwTerminate(); }

		/**
		 * @brief		Returns whether the window is currently open.
		 * @return		True if the window is open, else false.
		 */
	bool IsOpen();

		/**
		 * @brief		Swaps buffers, rendering contents on screen.
		 */
	void SwapBuffers();

		/**
		 * @brief		Polls mouse and key events.
		 */
	void PollEvents();

		/**
		 * @brief		Closes the window.
		 */
	void CloseWindow();

	/*
	void SetKeyboardFunc(void (*f)(GLFWwindow*, int, int, int, int));
	void SetMouseFunc(void (*f)(GLFWwindow*, double, double));
	void SetScrollFunc(void (*f)(GLFWwindow*, double, double));
	*/

		/**
		 * @brief		Get the current aspect ratio of the window.
		 * @return		The aspect ratio.
		 */
	float GetAspectRatio() const { return (float)m_frameBufferWidth / (float)m_frameBufferHeight; }
};

inline Window::Window(int width, int height, const char * title)
{
	if (!glfwInit()) {
		std::cout << "Could not initialise GLFW" << std::endl;
		__debugbreak();
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); //For mac

	m_Window = glfwCreateWindow(width, height, title, nullptr, nullptr);

	if (!m_Window) {
		std::cout << "Failed to create window" << std::endl;
		glfwTerminate();
		__debugbreak();
	}

	glfwMakeContextCurrent(m_Window);

	glfwGetFramebufferSize(m_Window, &m_frameBufferWidth, &m_frameBufferHeight);

	glfwSetInputMode(m_Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glewExperimental = GL_TRUE;

	if (GLEW_OK != glewInit()) {
		std::cout << "Failed to initialise GLEW" << std::endl;
		__debugbreak();
	}

	glViewport(0, 0, m_frameBufferWidth, m_frameBufferHeight);

	glfwSetKeyCallback(m_Window, KeyCallback);
	glfwSetCursorPosCallback(m_Window, MouseCallback);
	glfwSetScrollCallback(m_Window, ScrollCallback);
	glfwSetMouseButtonCallback(m_Window, MouseButtonCallback);

	// Always start the demo with the normal rendering method
	TEMPUSE_renderingTask = renderingMethod::Game_Demo;

	// Always start the demo without the wireframe mode enabled
	TEMPUSE_wireframeStatus = wireframeRendering::None;
}

bool Window::IsOpen() { return !glfwWindowShouldClose(m_Window); }
void Window::SwapBuffers() { glfwSwapBuffers(m_Window); }
void Window::PollEvents() {
	float currentFrame = static_cast<float>(glfwGetTime());
	deltaTime = currentFrame - lastFrame;
	lastFrame = currentFrame;

	glfwPollEvents();
	doMovement();
}

void Window::CloseWindow() { glfwSetWindowShouldClose(m_Window, GL_TRUE); }

/*
void Window::SetKeyboardFunc(void (*f)(GLFWwindow*, int, int, int, int)) { glfwSetKeyCallback(m_Window, (*f)); }
void Window::SetMouseFunc(void (*f)(GLFWwindow*, double, double))        { glfwSetCursorPosCallback(m_Window, (*f)); }
void Window::SetScrollFunc(void (*f)(GLFWwindow*, double, double))       { glfwSetScrollCallback(m_Window, (*f)); }
*/

#endif /* Window_hpp */
