#ifndef ZENENGINE_CORE_AI_AIDETAIL_STATEMACHINE_H_
#define ZENENGINE_CORE_AI_AIDETAIL_STATEMACHINE_H_

#include "StateList.h"

namespace zenai {

	template <class T>
	class StateMachine {
	public:
		StateMachine(T* owner);
		~StateMachine();

		void Update() const;
		void ChangeState(const std::string& name);
		void RevertToPrevious();

		void SetGlobalState(const std::string& name);

		static void RegisterState(const std::string& name, State& state);
	private:
		State* currentState;
		State* previousState;
		State* globalState;

		T* owner;
		static StateList states;
	}; // class StateMachine

	template<class T>
	StateList StateMachine<T>::states;

	template<class T>
	StateMachine<T>::StateMachine(T* owner) {
		this->owner = owner;
		currentState = nullptr;
		previousState = nullptr;
		globalState = nullptr;

	}

	template<class T>
	StateMachine<T>::~StateMachine() {
		owner = nullptr;
	}

	template<class T>
	void StateMachine<T>::Update() const {
		if (currentState != nullptr) {
			currentState->Execute(owner);
		}
	}

	template<class T>
	void StateMachine<T>::ChangeState(const std::string& name) {
		if (states.GetState(name) != nullptr) {
			previousState = currentState;
			if (previousState != nullptr) {
				previousState->Exit(owner); //exit previous state
			}
			currentState = states.GetState(name);
			if (currentState != nullptr) {
				currentState->Enter(owner); //enter new state
			}
		}
	}

	template<class T>
	void StateMachine<T>::RevertToPrevious() {
		currentState = previousState;
	}

	template<class T>
	void StateMachine<T>::SetGlobalState(const std::string& name) {
		if (states.GetState(name) != nullptr) {
			globalState = states.GetState(name);
		}
	}

	template<class T>
	void StateMachine<T>::RegisterState(const std::string & name, State& state) {
		states.AddState(name, state);
	}

} // namespace zenai

#endif