#include "..\StateList.h"
#include <iostream>

namespace zenai {

	StateList::StateList() {

	}

	State* StateList::GetState(const std::string& name) {
		if (states.find(name) != states.end()) {
			return &states[name];
		}
		return nullptr;
	}

	void StateList::AddState(const std::string& name, State state){
		states.emplace(name, state);
		//states[name].Enter();
	}

} // namespace zenai