#include "..\AIUtils.h"

#include <chrono>
#include <random>

namespace zenai {
	int Random(int min, int max) {
		std::random_device rd;
		std::mt19937 mt(rd());
		std::uniform_int_distribution<> range(min, max);

		return range(mt);
	}

	float Random(float min, float max) {
		std::random_device rd;
		std::mt19937 mt(rd());
		std::uniform_real_distribution<float> range(min, max);

		return range(mt);
	}

	void Convert3fTo2f(gem::vec2f & vec, const gem::vec3f& srcVec) {
		vec.x = srcVec.x;
		vec.y = srcVec.z;
	}

	void Convert2fTo3f(gem::vec3f & vec, const gem::vec2f& srcVec) {
		vec.x = srcVec.x;
		vec.z = srcVec.y;
	}

	gem::vec2f Convert3fTo2f(const gem::vec3f & vec){
		return gem::vec2f(vec.x, vec.z);
	}

	gem::vec3f Convert2fTo3f(const gem::vec2f & vec){
		return gem::vec3f(vec.x, 0.0f, vec.y);
	}
}