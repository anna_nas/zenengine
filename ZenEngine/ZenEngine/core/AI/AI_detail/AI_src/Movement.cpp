#include "..\Movement.h"
#include "..\AIUtils.h"

namespace zenai {
	const float Movement::PI = 3.14159f;

	bool Movement::MoveTo(gem::vec3f& pos, gem::vec3f& vel, const gem::vec3f& target, float time, float offset) {
		bool isX = false, isZ = false;
		gem::vec2f newPos2D;
		gem::vec2f targetPos2D = Convert3fTo2f(target);
		gem::vec2f toTarget2D = targetPos2D - Convert3fTo2f(pos);

		if (gem::magnitudeSq(toTarget2D) > 0.0000f) { //any vector that fails this is either 0 or assumed to be an insignificant distance
			toTarget2D = gem::normalize(toTarget2D); //cant call this on a 0 vector
			targetPos2D += -(toTarget2D * offset);
		}
		if (Convert3fTo2f(pos) == targetPos2D) {
			return true; //already at target
		}
		vel = Convert2fTo3f(toTarget2D) * gem::magnitude(vel);
		newPos2D = Convert3fTo2f(vel) * time;

		//check if it has reached/will reach the x value
		if (pos.x == targetPos2D.x) {
			isX = true;
		}
		else {
			if (pos.x < targetPos2D.x) {
				if (pos.x + newPos2D.x >= targetPos2D.x) {
					isX = true;
				}
			}
			if (pos.x > targetPos2D.x) {
				if (pos.x + newPos2D.x <= targetPos2D.x) {
					isX = true;
				}
			}
		}

		//check if it has reached/will reach the z value
		if (pos.z == targetPos2D.y) {
			isZ = true;
		}
		else {
			if (pos.z < targetPos2D.y) {
				if (pos.z + newPos2D.y >= targetPos2D.y) {
					isZ = true;
				}
			}
			if (pos.z > targetPos2D.y) {
				if (pos.z + newPos2D.y <= targetPos2D.y) {
					isZ = true;
				}
			}
		}

		if (isX && isZ) {
			pos = Convert2fTo3f(targetPos2D);
			return true;
		}
		pos += Convert2fTo3f(newPos2D);
		return false;
	}

	bool Movement::Chase(gem::vec3f& pos, gem::vec3f& vel, const gem::vec3f& evader, float time, float offset) {
		return MoveTo(pos, vel, evader, time, offset);
	}

	bool Movement::Pursue(gem::vec3f& pos, gem::vec3f& vel, const gem::vec3f& evader, const gem::vec3f& evaderVel, float time, float offset) {
		gem::vec2f toEvader = Convert3fTo2f(evader - pos);
		gem::vec2f pHeading = gem::normalize(Convert3fTo2f(vel));
		gem::vec2f eHeading = gem::normalize(Convert3fTo2f(evaderVel));

		if (((gem::dot(toEvader, pHeading) > 0) && (gem::dot(pHeading, eHeading) < -0.95)) || gem::magnitude(evaderVel) == 1) {
			return MoveTo(pos, vel, evader, time, offset);
		}
		else {
			float lookAheadTime = gem::magnitude(toEvader) / (gem::magnitude(evaderVel) + gem::magnitude(vel));
			return MoveTo(pos, vel, evader + evaderVel * lookAheadTime, time, offset);
		}
	}

	bool Movement::Flee(gem::vec3f& pos, gem::vec3f& vel, float panicDist, const gem::vec3f& pursuer, float time) {
		gem::vec2f fromPursuer = Convert3fTo2f(pos - pursuer);

		if (gem::magnitudeSq(fromPursuer) > panicDist * panicDist) {
			return false;
		}

		fromPursuer = gem::normalize(fromPursuer);
		vel = Convert2fTo3f(fromPursuer) * gem::magnitude(vel);
		pos += vel * time;
		return true;
	}

	bool Movement::Evade(gem::vec3f& pos, gem::vec3f& vel, float panicDist, const gem::vec3f& pursuer, const gem::vec3f& pursuerVel, float time) {
		gem::vec2f toPursuer = Convert3fTo2f(pursuer - pos);

		float lookaheadtime = gem::magnitude(toPursuer) / (gem::magnitude(vel) + gem::magnitude(pursuerVel));
		return Flee(pos, vel, panicDist, pursuer + pursuerVel * lookaheadtime, time);
	}

	bool Movement::SeeTarget(const gem::vec3f& pos, const gem::vec3f& vel, const gem::vec3f& target, float viewDist, float fov) {
		gem::vec3f toTarget = target - pos;

		if (gem::magnitudeSq(toTarget) > viewDist * viewDist) {
			return false;
		}
		toTarget = gem::normalize(toTarget);
		gem::vec3f pHeading = gem::normalize(vel);
		float angle = gem::dot(pHeading, toTarget);
		if (angle > 1.0f) {
			angle = 1.0f;
		}
		else {
			if (angle < -1.0f) {
				angle = -1.0f;
			}
		}
		angle = acos(angle) * (180 / PI);

		if (angle <= (fov / 2)) {
			return true;
		}

		return false;
	}

	void Movement::SetWander(float radius, float distance, float jitter) {
		this->wanderRadius = radius;
		this->wanderDistance = distance;
		this->wanderJitter = jitter;
		this->wanderTarget = gem::normalize(gem::vec2f(zenai::Random(-1.0f, 1.0f), zenai::Random(-1.0f, 1.0f)));
	}

	bool Movement::Wander(gem::vec3f & pos, gem::vec3f & vel, float time) {
		float jitter = wanderJitter * time;

		wanderTarget += gem::vec2f(zenai::Random(-1.0f, 1.0f) * jitter, zenai::Random(-1.0f, 1.0f) * jitter);
		wanderTarget = gem::normalize(wanderTarget);

		gem::vec2f heading = gem::normalize(zenai::Convert3fTo2f(vel));
		gem::vec2f circleCentre = zenai::Convert3fTo2f(pos) + (heading * wanderDistance);

		gem::vec2f target = circleCentre + (wanderTarget * wanderRadius);

		zenai::Movement::MoveTo(pos, vel, zenai::Convert2fTo3f(target), time, 0);
		return true;
	}

	void Movement::SetPatrolPath(const gem::vec3f& start, float xLength, float zLength) {
		currentWaypoint = 0;
		waypoints.clear();
		waypoints.emplace_back(start);
		waypoints.emplace_back(start.x + xLength, 0.0f, start.z);
		waypoints.emplace_back(start.x + xLength, 0.0f, start.z + zLength);
		waypoints.emplace_back(start.x, 0.0f, start.z + zLength);
	}

	bool Movement::Patrol(gem::vec3f& pos, gem::vec3f& vel, float time) {
		if (MoveTo(pos, vel, waypoints[currentWaypoint], time, 0)) {
			currentWaypoint = (currentWaypoint + 1) % waypoints.size();
		}
		return true;
	}

}