#ifndef ZENENGINE_CORE_SCRIPTING_DETAIL_MESSAGEDISPATCH_H_
#define ZENENGINE_CORE_SCRIPTING_DETAIL_MESSAGEDISPATCH_H_

#include <set>
#include "Message.h"

namespace zenai {
	class GameObject;

	class MessageDispatch {
	public:
		static MessageDispatch& GetInstance();

		void DispatchMsg(int reciever, int sender, int msg, float delay, sol::object);
		void DispatchDelayedMessages();

		MessageDispatch(const MessageDispatch& other) = delete;
		void operator=(const MessageDispatch& other) = delete;
	private:
		MessageDispatch() {}

		std::set<Message> queue;

		void Discharge(GameObject* receiver, const Message& msg);
	};

}

#endif