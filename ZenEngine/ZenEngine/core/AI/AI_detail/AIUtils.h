#ifndef ZENENGINE_CORE_AI_AIDETAIL_AIUTILS_H_
#define ZENENGINE_CORE_AI_AIDETAIL_AIUTILS_H_

#include "gem.h"

namespace zenai {
	int Random(int min = 1, int max = 100);
	float Random(float min = 0.0, float max = 1.0);

	void Convert3fTo2f(gem::vec2f& vec, const gem::vec3f& srcVec);
	void Convert2fTo3f(gem::vec3f& vec, const gem::vec2f& srcVec);


	gem::vec2f Convert3fTo2f(const gem::vec3f& vec);
	gem::vec3f Convert2fTo3f(const gem::vec2f& vec);
}

#endif // ZENENGINE_CORE_AI_AIDETAIL_AIUTILS_H_