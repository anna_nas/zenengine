#ifndef ZENENGINE_CORE_AI_AIDETAIL_STATE_H_
#define ZENENGINE_CORE_AI_AIDETAIL_STATE_H_

#include <sol.hpp>

namespace zenai {

	class State {
	public:
		sol::function Enter;
		sol::function Execute;
		sol::function Exit;
		sol::function OnMessage;
	}; //class State

} //namespace zenai

#endif //ZENENGINE_CORE_AI_AIDETAIL_STATE_H_