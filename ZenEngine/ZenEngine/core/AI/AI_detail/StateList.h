#ifndef ZENENGINE_CORE_AI_AIDETAIL_STATELIST_H_
#define ZENENGINE_CORE_AI_AIDETAIL_STATELIST_H_

#include <string>
#include <unordered_map>

#include "State.h"

namespace zenai {

	class StateList {
	public:
		StateList();

		State* GetState(const std::string& name);

		void AddState(const std::string& name, State state);
	private:
		std::unordered_map<std::string, State> states;
	}; // StateList

} // namespace zenai

#endif //ZENENGINE_CORE_AI_AIDETAIL_STATELIST_H_