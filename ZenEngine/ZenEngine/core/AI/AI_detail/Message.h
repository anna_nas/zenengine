#ifndef ZENENGINE_CORE_SCRIPTING_DETAIL_MESSAGE_H_
#define ZENENGINE_CORE_SCRIPTING_DETAIL_MESSAGE_H_

#include "sol.hpp"

namespace zenai {

	struct Message {
		int to;
		int from;
		int msg;
		float dispatchTime;
		sol::object extraInfo;

		Message() : to(-1), from(-1), msg(-1), dispatchTime(-1), extraInfo(1) {}
		Message(int t, int f, int m, float time, sol::object extra) : to(t), from(f), msg(m), dispatchTime(time), extraInfo(extra) {}
	};

	inline bool operator==(const Message& m1, const Message& m2) {
		return ((fabs(m1.dispatchTime - m2.dispatchTime) > 0.25f) &&
			(m1.to == m2.to) && (m1.from == m2.from) && (m1.msg == m2.msg));
	}

	inline bool operator<(const Message& m1, const Message& m2) {
		if (m1 == m2) {
			return false;
		}
		return (m1.dispatchTime < m2.dispatchTime);
	}

}

#endif