#ifndef ZENENGINE_CORE_AI_AIDETAIL_MOVEMENT_H_
#define ZENENGINE_CORE_AI_AIDETAIL_MOVEMENT_H_

#include <memory>
#include <vector>
#include "gem.h"

namespace zenai {
	
	class Movement {
	public:
		static bool MoveTo(gem::vec3f& pos, gem::vec3f& vel, const gem::vec3f& target, float time, float offset = 0);

		static bool Chase(gem::vec3f& pos, gem::vec3f& vel, const gem::vec3f& evader, float time, float offset);
		static bool Pursue(gem::vec3f& pos, gem::vec3f& vel, const gem::vec3f& evader, const gem::vec3f& evaderVel, float time, float offset);

		static bool Flee(gem::vec3f& pos, gem::vec3f& vel, float panicDist, const gem::vec3f& pursuer, float time);
		static bool Evade(gem::vec3f& pos, gem::vec3f& vel, float panicDist, const gem::vec3f& pursuer, const gem::vec3f& pursuerVel, float time);

		static bool SeeTarget(const gem::vec3f& pos, const gem::vec3f& vel, const gem::vec3f& target, float viewDist, float fov);

		void SetWander(float radius, float distance, float jitter);
		bool Wander(gem::vec3f& pos, gem::vec3f& vel, float time);

		void SetPatrolPath(const gem::vec3f& start, float xLength, float yLength);
		bool Patrol(gem::vec3f& pos, gem::vec3f& vel, float time);
	private:
		static const float PI;

		gem::vec2f wanderTarget;
		float wanderRadius;
		float wanderDistance;
		float wanderJitter;

		std::vector<gem::vec3f> waypoints;
		int currentWaypoint;
	};
} // namespace zenai

#endif // ZENENGINE_CORE_AI_AIDETAIL_MOVEMENT_H_