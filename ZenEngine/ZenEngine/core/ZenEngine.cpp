// use for testing, will be moved to separate project later

#include <iostream>
#include <vector>
#include <memory>

#include "ZenEngine.h"

//! For temp use only! Make sure to replace with proper input system when finally fixed.
#include "input/header/Input_DemoHack.hpp"

/*
REFACTOR:
	Rendering Parts
	ObjectLoader
	Window
	Camera
	GLM -> GEM
	Object Manager
	Asset Factory

TODO:
	BruteForce Terrain
	Animated Model
	Input
*/

int main() {
	Window window(1024, 768, "ZenEngine");	// TODO: Update window framework
	Renderer renderer;

	ScriptManager scripter;
	gameobject::GameObjectManager objManager("resources/default.shader", "resources/default_terrain.shader");
	objManager.CreateObjects(scripter.CreateObjects()); //create objects
	camera.m_GameObjs = const_cast<std::vector<std::unique_ptr<gameobject::GameObject>>*>(objManager.GetObjects());

//	physics::BulletPhysics* physics = new physics::BulletPhysics();
//	camera.RegisterPhysicsWorld(physics);
//	for(auto& obj : objManager.GetObjects()) {
//	}

	float player_eye_height = 1.8f;
		
	renderer.ClearColor(0.5f, 0.5f, 0.6f, 1.0f);

	//------------------ End Screen / Menu Screen ------------------//
	Texture menuTexture("resources/HelpMenu.png");
	Texture splashScreenTexture("resources/SplashScreen.png");
	Shader demoOverlayScreenShader("resources/demoOverlay.shader");
	demoOverlayScreenShader.Unbind();

	float vertices[] = {
		-0.8f,  0.7f, 0.0f, 0.0f, 1.0f, 
		-0.8f, -0.9f, 0.0f, 0.0f, 0.0f,
		 0.8f,  0.7f, 0.0f, 1.0f, 1.0f,

		 0.8f,  0.7f, 0.0f, 1.0f, 1.0f,
		-0.8f, -0.9f, 0.0f, 0.0f, 0.0f,
		 0.8f, -0.9f, 0.0f, 1.0f, 0.0f,
	};

	VertexArray VA;
	VA.Bind();
	VertexBuffer VB(vertices, sizeof(vertices));
	VertexBufferLayout VBL;
	VBL.Push<float>(3);
	VBL.Push<float>(2);
	VA.AddBuffer(VB, VBL);
	VB.Unbind();
	VA.Unbind();

	camera.m_position = gem::vec3f(500.0f, objManager.GetTerrainHeightAt(camera.m_position) + player_eye_height, 500.0f);

	while (window.IsOpen()) {
		window.PollEvents();
		renderer.Clear();

//		physics->Update(1.0f/60.0f);

		// Manage the camera heights
		// Determine the player's eye height
		player_eye_height = 1.8f; // Set a baseline of the eye height to be 1.8m from the ground
		if (camera.GetPitch() < -45.01) // Prevent float comparison accuracy from being a significant issue
		{
			/*
			* Set the eye height relative to the pitch
			* -- EXPLANATION --
			* Base height += Constant * (Scalar relative to the Camera Pitch)
			* (Scalar relative to the Camera Pitch) = (Camera Pitch[max range of 90] / Non-linear Motion)
			* Non-linear Motion = Scalar for height limit modification * Square-root of the difference between the max and current camera pitch
			*/
			player_eye_height += 2 * player_eye_height * ((camera.GetPitch() + 45.0f) / (-0.4f * sqrt(camera.GetPitch() / -90.0f)));
		}
		// Manage the camera's height
		camera.m_position.y = objManager.GetTerrainHeightAt(camera.m_position) + player_eye_height;
		
		if (TEMPUSE_renderingTask == renderingMethod::Screen_Splash) {
			demoOverlayScreenShader.Bind();
			splashScreenTexture.Bind();
			//demoOverlayScreenShader.SetUniform1i("u_Texture0", splashScreenTexture.GetID());
			VA.Bind();
			VB.Draw();
			VA.Unbind();
			splashScreenTexture.Unbind();
			demoOverlayScreenShader.Unbind();
		}

		else if(TEMPUSE_renderingTask == renderingMethod::Screen_Help) {
			demoOverlayScreenShader.Bind();
			menuTexture.Bind();
			//demoOverlayScreenShader.SetUniform1i("u_Texture0", menuTexture.GetID());
			VA.Bind();
			VB.Draw();
			VA.Unbind();
			menuTexture.Unbind();
			demoOverlayScreenShader.Unbind();
		}
		
		TEMPUSE_wireframeStatus == wireframeRendering::Wireframe ? 
			objManager.DebugDrawObjs(camera.GetProjectionMatrix(), camera.GetViewMatrix(), camera.m_position) : 
			objManager.DrawObjs(camera.GetProjectionMatrix(), camera.GetViewMatrix(), camera.m_position);
			objManager.DoAI();

		window.SwapBuffers();
	}
//	physics->Finalize();
	return 0;
}