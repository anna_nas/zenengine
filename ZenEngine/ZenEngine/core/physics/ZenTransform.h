#ifndef ZENENGINE_CORE_PHYSICS_TRANSFORM_H_
#define ZENENGINE_CORE_PHYSICS_TRANSFORM_H_

#include "gem.h"

namespace physics
{
	class ZenTransform
	{
	public:
		/*
		 * @brief Default constructor
		 */
		ZenTransform() 
			: m_position(0.0f),
			m_rotation(0.0f),
			m_scale(1.0f) 
		{}

		/*
		 * @brief Vector constructor
		 */
		explicit ZenTransform(const gem::vec3f& pos, const gem::vec3f& rot, const gem::vec3f& scale = gem::vec3f(1.0f))
			: m_position(pos),
			m_rotation(rot),
			m_scale(scale)
		{}

		/*
		 * @brief Copy constructor
		 */
		ZenTransform(const ZenTransform& other)
			: m_position(other.m_position),
			m_rotation(other.m_rotation),
			m_scale(other.m_scale)
		{}

		/*
		* @brief Assignment operator
		*/
		ZenTransform& operator=(const ZenTransform& other)
		{
			m_position = other.m_position;
			m_rotation = other.m_rotation;
			m_scale = other.m_scale;
			return *this;
		}

		/*
		 * @brief Destructor
		 */
		~ZenTransform() {}

		/** @brief Return the vector for the position */
		gem::vec3f& GetPosition() { return m_position; }
		/** @brief Return the vector for the position */
		const gem::vec3f& GetPosition() const { return m_position; }

		/** @brief Return the vector for the rotation */
		gem::vec3f& GetRotation() { return m_rotation; }
		/** @brief Return the vector for the rotation */
		const gem::vec3f& GetRotation() const { return m_rotation; }

		/** @brief Return the vector for the scale */
		gem::vec3f& GetScale() { return m_scale; }
		/** @brief Return the vector for the scale */
		const gem::vec3f& GetScale() const { return m_scale; }

		/** @brief Set the position by a vector */
		void SetPosition(const gem::vec3f& pos) { m_position = pos; }

		/** @brief Set the rotation by a vector */
		void SetRotation(const gem::vec3f& rot) { m_rotation = rot; }

		/** @brief Set the scale by a vector */
		void SetScale(const gem::vec3f& scale) { m_scale = scale; }

		/** @brief Set the position by a vector */
		void SetIdentity()
		{
			m_position = gem::vec3f(0.0f);
			m_rotation = gem::vec3f(0.0f);
			m_scale = gem::vec3f(0.0f);
		}

	private:
		gem::vec3f m_position;
		gem::vec3f m_rotation; // currently Euler, should change to Quaternion
		gem::vec3f m_scale;
	};

} // namespace physics

#endif // ZENENGINE_CORE_PHYSICS_TRANSFORM_H_