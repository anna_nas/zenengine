#include "BulletPhysics.h"
#include "BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h"

#include "RigidBody.h"
#include "../game_object/game_object_detail/GameObject.h"
#include "colliders/ZenCollider.h"

#include <iostream>
#include <cassert>

namespace physics
{
	BulletPhysics::BulletPhysics()
	{
		Init();
	}

	BulletPhysics::~BulletPhysics()
	{
		Finalize();
	}

	void BulletPhysics::Init()
	{
		// Build the broadphase
		m_overlappingPairCache = new btDbvtBroadphase();

		// Set up the collision configuration and dispatcher
		m_collisionConfiguration = new btDefaultCollisionConfiguration();
		m_dispatcher = new btCollisionDispatcher(m_collisionConfiguration);

		// The actual physics solver
		m_solver = new btSequentialImpulseConstraintSolver();

		// The world
		m_dynamicsWorld = new btDiscreteDynamicsWorld(m_dispatcher, m_overlappingPairCache, m_solver, m_collisionConfiguration);

		// Set gravity
		m_dynamicsWorld->setGravity(btVector3(0, -9.8f, 0));
	}

	void BulletPhysics::Update(float dt)
	{
		(btScalar)dt;
		if (m_dynamicsWorld) {  
			m_dynamicsWorld->stepSimulation(dt, 0);
		}
	}

	void BulletPhysics::AddPhysicsBody(RigidBody* body, void* userPtr)
	{
		if (!body) {
			std::cerr << "BulletPhysics.cpp Warning: RigidBody is not initialized." << std::endl;
			return;
		}

		m_dynamicsWorld->addRigidBody(body->GetRigidBody());
		body->GetRigidBody()->setUserPointer(userPtr);
	}

	btDynamicsWorld* BulletPhysics::GetPhysicsWorld() const
	{
		return m_dynamicsWorld;
	}

	/*
	bool BulletPhysics::CheckPointCollisions(gem::vec3f point) {
		return false;
	}
	*/

	void BulletPhysics::Finalize()
	{
		// Remove the rigidbodies from the dynamics world and delete them
		for (int i = m_dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
		{
			btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[i];
			btRigidBody* body = btRigidBody::upcast(obj);
			if (body && body->getMotionState())
			{
				delete body->getMotionState();
			}
			m_dynamicsWorld->removeCollisionObject(obj);
			delete obj;
		}

		// Delete collision shapes
		for (int j = 0; j < m_collisionShapes.size(); j++)
		{
			btCollisionShape* shape = m_collisionShapes[j];
			delete shape;
		}

		m_collisionShapes.clear();

		delete m_dynamicsWorld;
		delete m_solver;
		delete m_dispatcher;
		delete m_collisionConfiguration;
		delete m_overlappingPairCache;
	}
}