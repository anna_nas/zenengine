#ifndef ZENENGINE_CORE_PHYSICS_COLLIDERS_COMPOUND_H_
#define ZENENGINE_CORE_PHYSICS_COLLIDERS_COMPOUND_H_

#include "ZenCollider.h"

namespace physics
{
	class CompoundCollider : public ZenCollider
	{
	public:
		CompoundCollider() {}
		virtual ~CompoundCollider() {}
	};

} // namespace physics

#endif // ZENENGINE_CORE_PHYSICS_COLLIDERS_COMPOUND_H_