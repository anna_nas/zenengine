#ifndef ZENENGINE_CORE_PHYSICS_COLLIDERS_ZENCOLLIDER_H_
#define ZENENGINE_CORE_PHYSICS_COLLIDERS_ZENCOLLIDER_H_

#include <btBulletDynamicsCommon.h>
#include "../BulletConversions.h"

namespace physics
{
	enum class ColliderType
	{
		// Polyhedral Convex Colliders
		// Defined explicitly by finitely many half-spaces, or as the convex hull of finitely many points.
		CONVEX_HULL_COLLIDER,
		BOX_COLLIDER,
		CUSTOM_POLYHEDRAL_COLLIDER,

		// Implicit Convex Colliders
		// Defined by linear constraints
		SPHERE_COLLIDER,
		CAPSULE_COLLIDER,
		CONE_COLLIDER,
		CYLINDER_COLLIDER,
		CUSTOM_CONVEX_COLLIDER,

		// Concave Colliders
		TRIANGLE_MESH_COLLIDER,
		SCALED_TRIANGLE_MESH_COLLIDER,
		TERRAIN_COLLIDER,
		STATIC_PLANE_COLLIDER,

		COMPOUND_COLLIDER,

		INVALID_COLLIDER
	};

	class ZenCollider
	{
	public:
		ZenCollider() : m_type(ColliderType::INVALID_COLLIDER) {}

		virtual ~ZenCollider() 
		{
			if (m_collider)
				delete m_collider;
		}

		virtual btCollisionShape* GetCollider() { return m_collider; }

	protected:
		btCollisionShape* m_collider;
		ColliderType m_type;
	};

} // namespace physics

#endif // ZENENGINE_CORE_PHYSICS_COLLIDERS_ZENCOLLIDER_H_