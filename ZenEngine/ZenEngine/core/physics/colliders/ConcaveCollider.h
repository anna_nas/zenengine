#ifndef ZENENGINE_CORE_PHYSICS_COLLIDERS_CONCAVE_H_
#define ZENENGINE_CORE_PHYSICS_COLLIDERS_CONCAVE_H_

#include "ZenCollider.h"

namespace physics
{
	enum class ScalarType 
	{
		FLOAT,
		DOUBLE,
		INTEGER,
		SHORT,
		FIXEDPOINT88,
		UCHAR
	};

	class ConcaveCollider : public ZenCollider
	{
	public:
		ConcaveCollider() {}
		virtual ~ConcaveCollider() {}

	protected:
		PHY_ScalarType zen_to_btScalarType(ScalarType type);
	};

} // namespace physics

#endif // ZENENGINE_CORE_PHYSICS_COLLIDERS_CONCAVE_H_