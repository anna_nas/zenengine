#ifndef ZENENGINE_CORE_PHYSICS_COLLIDERS_PLANE_H_
#define ZENENGINE_CORE_PHYSICS_COLLIDERS_PLANE_H_

#include "ZenCollider.h"
#include "gem.h"

namespace physics
{
	class StaticPlaneCollider : public ZenCollider
	{
	public:
		StaticPlaneCollider(gem::vec3f normal, float constant)
		{
			m_type = ColliderType::STATIC_PLANE_COLLIDER;
			m_collider = new btStaticPlaneShape(vec3f_to_btVector3(normal), constant);
		}

		virtual ~StaticPlaneCollider() {}
	};

} // namespace physics

#endif // ZENENGINE_CORE_PHYSICS_COLLIDERS_PLANE_H_