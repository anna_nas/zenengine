#include "ConcaveCollider.h"

namespace physics
{
	PHY_ScalarType ConcaveCollider::zen_to_btScalarType(ScalarType type)
	{
		switch(type)
		{
		case ScalarType::FLOAT:			return PHY_FLOAT;
		case ScalarType::DOUBLE:		return PHY_DOUBLE;
		case ScalarType::INTEGER:		return PHY_INTEGER;
		case ScalarType::SHORT:			return PHY_SHORT;
		case ScalarType::FIXEDPOINT88:	return PHY_FIXEDPOINT88;
		case ScalarType::UCHAR:			
		default:						return PHY_UCHAR;
		}
	}

} // namespace physics