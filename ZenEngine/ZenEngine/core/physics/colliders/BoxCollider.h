#ifndef ZENENGINE_CORE_PHYSICS_COLLIDERS_BOX_H_
#define ZENENGINE_CORE_PHYSICS_COLLIDERS_BOX_H_

#include "ConvexCollider.h"
#include "gem.h"

namespace physics
{
	class BoxCollider : public ConvexCollider
	{
		btBoxShape *m_boxCollider;
	public:
		BoxCollider(gem::vec3f halfExtents) 
		{
			m_type = ColliderType::BOX_COLLIDER;
			m_boxCollider = new btBoxShape(vec3f_to_btVector3(halfExtents));
		}

		virtual bool isInside(const gem::vec3f & point, float margin) {
			return (m_boxCollider)->isInside(vec3f_to_btVector3(point), (btScalar) margin);
		}

		virtual ~BoxCollider()
		{
			if (m_boxCollider)
				delete m_boxCollider;
		}

		virtual btCollisionShape* GetCollider() { return m_boxCollider; }
	};

} // namespace physics

#endif // ZENENGINE_CORE_PHYSICS_COLLIDERS_BOX_H_