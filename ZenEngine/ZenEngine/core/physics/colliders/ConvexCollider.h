#ifndef ZENENGINE_CORE_PHYSICS_COLLIDERS_CONVEX_H_
#define ZENENGINE_CORE_PHYSICS_COLLIDERS_CONVEX_H_

#include "ZenCollider.h"

namespace physics
{
	class ConvexCollider : public ZenCollider
	{
	public:
		ConvexCollider() {}
		virtual ~ConvexCollider() {}
	};

} // namespace physics

#endif // ZENENGINE_CORE_PHYSICS_COLLIDERS_CONVEX_H_