#ifndef ZENENGINE_CORE_PHYSICS_COLLIDERS_HEIGHTFIELD_H_
#define ZENENGINE_CORE_PHYSICS_COLLIDERS_HEIGHTFIELD_H_

#include "BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h"
#include "ConcaveCollider.h"

namespace physics
{
	class HeightfieldCollider : public ConcaveCollider
	{
	public:
		HeightfieldCollider(
			int heightStickWidth, int heightStickLength, const void * heightfieldData,
			float heightScale, float minHeight, float maxHeight,
			int upAxis, ScalarType heightDataType, bool flipQuadEdges = false)
		{
			m_type = ColliderType::TERRAIN_COLLIDER;

			btHeightfieldTerrainShape* heightField = new btHeightfieldTerrainShape(
				heightStickWidth, heightStickLength, heightfieldData, 
				heightScale, minHeight, maxHeight, 
				upAxis, zen_to_btScalarType(heightDataType), flipQuadEdges);

			btVector3 mmin, mmax;
			heightField->getAabb(btTransform::getIdentity(), mmin, mmax);

			heightField->setUseDiamondSubdivision(true);

			m_collider = heightField;

			btVector3 localScaling(100, 1, 100);
			localScaling[upAxis] = 1.f;
			m_collider->setLocalScaling(localScaling);
		}

		virtual ~HeightfieldCollider() {}

		virtual btCollisionShape* GetCollider() { return (btHeightfieldTerrainShape*)m_collider; }
	};

} // namespace physics

#endif // ZENENGINE_CORE_PHYSICS_COLLIDERS_HEIGHTFIELD_H_