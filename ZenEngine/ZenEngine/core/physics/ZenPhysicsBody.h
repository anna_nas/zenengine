#ifndef ZENENGINE_CORE_PHYSICS_ZENPHYSICSBODY_H_
#define ZENENGINE_CORE_PHYSICS_ZENPHYSICSBODY_H_

#include <cassert>

#include <btBulletDynamicsCommon.h>
#include <gtc/type_ptr.hpp>
#include "gem.h"
#include "ZenTransform.h"

namespace physics
{
	enum class ObjectType
	{
		DYNAMIC, // positive mass, every simulation frame the dynamics will update its world transform
		STATIC, // zero mass, cannot move but just collide
		KINEMATIC // zero mass, can be animated by the user: dynamic objects will be pushed away but there is no influence from dynamics objects
	};

	class ZenCollider;

	class ZenPhysicsBody
	{
	public:
		ZenPhysicsBody() : m_objType(ObjectType::STATIC), m_mass(0.0f), m_collider(0)
		{
			m_initTransform.SetIdentity();
		}
		// ZenPhysicsBody(gem::vec3f pos, gem::vec3f rot, gem::vec3f scale, float mass, ZenCollider* shape, ObjectType type);
		virtual ~ZenPhysicsBody() {}

		virtual ZenCollider* GetCollider() { return m_collider; }
		virtual const ZenCollider* GetCollider() const { return m_collider; }

		virtual glm::mat4 GetModelMatrix() const = 0;
		virtual void SetWorldTransform(const ZenTransform& worldTrans) = 0;

	protected:
		ObjectType m_objType;
		float m_mass;
		ZenCollider* m_collider;
		ZenTransform m_initTransform;
	};

} // namespace Physics

#endif // ZENENGINE_CORE_PHYSICS_ZENPHYSICSBODY_H_