
#include <iostream>
#include <cassert>

#include "RigidBody.h"
#include "MotionState.h"
#include "colliders\ZenCollider.h"
#include "BulletConversions.h"
#include "../game_object/game_object_detail/GameObject.h"

namespace physics
{
	RigidBody::RigidBody(ZenTransform initTransform, float mass, ZenCollider* col, ObjectType type)
	{
		assert((!col->GetCollider() || col->GetCollider()->getShapeType() != INVALID_SHAPE_PROXYTYPE));

		if (type == ObjectType::STATIC && mass != 0.f) {
			std::cerr << "physics::RigidBody Warning [line 18]: trying to initialize static object with non-zero mass." << std::endl;
			mass = 0.0f;
		}

		if (type == ObjectType::DYNAMIC && mass == 0.f) {
			std::cerr << "physics::RigidBody Warning [line 23]: trying to initialize dynamic object with zero mass." << std::endl;
			mass = 1.0f;
		}
			
		bool isDynamic = (mass != 0.f);

		m_initTransform.SetIdentity();
		m_initTransform = initTransform;

		m_mass = mass;
		m_collider = col;
		m_objType = type;

		btVector3 localInertia(0, 0, 0);
		if (isDynamic)
			m_collider->GetCollider()->calculateLocalInertia(m_mass, localInertia);

		btMotionState* motionState = new MotionState(zen_to_btTransform(m_initTransform));

		btRigidBody::btRigidBodyConstructionInfo cInfo(m_mass, motionState, m_collider->GetCollider(), localInertia);
		m_body = new btRigidBody(cInfo);

		m_body->setContactProcessingThreshold(BT_LARGE_FLOAT);

		if (m_objType == ObjectType::KINEMATIC) {
			m_body->setCollisionFlags(m_body->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
			m_body->setActivationState(DISABLE_DEACTIVATION);
		}
	}

	glm::mat4 RigidBody::GetModelMatrix() const {
		if (!m_body || !m_body->getMotionState()) {
			std::cerr << "physics::RigidBody Error [line 58]: RigidBody is not initialized." << std::endl;
			return glm::mat4(); // change function termination method
		}

		//MotionState* modelMatrix = (MotionState*)m_body->getMotionState();
		//return modelMatrix->getModelMatrix();

		float m[16];
		btTransform worldTrans;

		m_body->getMotionState()->getWorldTransform(worldTrans);
		worldTrans.getOpenGLMatrix(m);

		return glm::make_mat4(m);
	}

	void RigidBody::SetWorldTransform(const ZenTransform& worldTrans) {
		if (!m_body || !m_body->getMotionState()) {
			std::cerr << "physics::RigidBody Error [line 76]: RigidBody is not initialized." << std::endl;
			return;
		}

		if (m_objType == ObjectType::STATIC) {
			std::cerr << "physics::RigidBody Warning [line 81]: moving static object." << std::endl;
		}

		MotionState* modelMatrix = (MotionState*)m_body->getMotionState();
		modelMatrix->setKinematicTransform(zen_to_btTransform(worldTrans));
	}

	btRigidBody* RigidBody::GetRigidBody() const {
		if (m_body) {
			return m_body;
		}
		return nullptr;	//added for "not all cotrol paths....." - Anna
	}

} // namespace Physics