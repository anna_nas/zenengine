#ifndef ZENENGINE_CORE_PHYSICS_BULLETPHYSICS_H_
#define ZENENGINE_CORE_PHYSICS_BULLETPHYSICS_H_

#include <btBulletDynamicsCommon.h>


namespace physics
{
	class RigidBody;
	class GameObject;

	class BulletPhysics
	{
	public:
		BulletPhysics();
		virtual ~BulletPhysics();

		void Init();
		void Update(float dt = (1.0f / 180.0f));
		void AddPhysicsBody(RigidBody* body, void* userPtr);
		void Finalize();
		//bool CheckPointCollisions(gem::vec3f point);
		btDynamicsWorld* GetPhysicsWorld() const;

	private:
		btAlignedObjectArray<btCollisionShape*> m_collisionShapes;
		btBroadphaseInterface* m_overlappingPairCache;
		btDefaultCollisionConfiguration* m_collisionConfiguration;
		btCollisionDispatcher* m_dispatcher;
		btSequentialImpulseConstraintSolver* m_solver;
		btDiscreteDynamicsWorld* m_dynamicsWorld;
	};
}

#endif // ZENENGINE_CORE_PHYSICS_BULLETPHYSICS_H_
