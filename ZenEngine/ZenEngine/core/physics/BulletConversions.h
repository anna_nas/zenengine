#ifndef ZENENGINE_CORE_PHYSICS_BULLETCONVERSIONS_H_
#define ZENENGINE_CORE_PHYSICS_BULLETCONVERSIONS_H_

#include <btBulletCollisionCommon.h>
#include "gem.h"
#include "ZenTransform.h"

namespace physics
{
	inline gem::vec3f btVector3_to_vec3f(const btVector3& v)
	{
		return gem::vec3f(v.x(), v.y(), v.z());
	}

	inline btVector3 vec3f_to_btVector3(const gem::vec3f& v)
	{
		return btVector3(v.x, v.y, v.z);
	}

	inline btQuaternion euler_to_btQuaternion(const gem::vec3f& v)
	{
		return btQuaternion(v.x, v.y, v.z);
	}

	inline btQuaternion euler_to_btQuaternion(float yaw, float pitch, float roll)
	{
		return btQuaternion(yaw, pitch, roll);
	}

	inline btTransform zen_to_btTransform(const ZenTransform& trans)
	{
		return btTransform(euler_to_btQuaternion(trans.GetRotation()), vec3f_to_btVector3(trans.GetPosition()));
	}

} // namespace physics

#endif // ZENENGINE_CORE_PHYSICS_BULLETCONVERSIONS_H_