#ifndef ZENENGINE_CORE_PHYSICS_RIGIDBODY_H_
#define ZENENGINE_CORE_PHYSICS_RIGIDBODY_H_

#include <btBulletDynamicsCommon.h>
#include "ZenPhysicsBody.h"

namespace physics
{
	class GameObject;

	class RigidBody : public ZenPhysicsBody
	{
	public:
		RigidBody() { }
		// gem::vec3f pos, gem::vec3f rot, gem::vec3f scale
		RigidBody(ZenTransform initTransform, float mass, ZenCollider* shape, ObjectType type);
		virtual ~RigidBody() { if (m_body) delete m_body; }

		virtual glm::mat4 GetModelMatrix() const;
		virtual void SetWorldTransform(const ZenTransform& worldTrans);

		btRigidBody* GetRigidBody() const;

	private:
		btRigidBody* m_body;
	};

} // namespace Physics

#endif // ZENENGINE_CORE_PHYSICS_RIGIDBODY_H_