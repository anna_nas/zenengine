#ifndef ZENENGINE_CORE_PHYSICS_MOTIONSTATE_H_
#define ZENENGINE_CORE_PHYSICS_MOTIONSTATE_H_

#include <btBulletDynamicsCommon.h>
#include <gtc/type_ptr.hpp>

namespace physics
{
	class MotionState : public btMotionState
	{
	public:
		MotionState(const btTransform& initTransform = btTransform::getIdentity()) 
			: m_modelMatrix(initTransform) {}

		virtual ~MotionState() {}

		virtual void getWorldTransform(btTransform& worldTrans) const override {
			worldTrans = m_modelMatrix;
		}

		virtual void setWorldTransform(const btTransform& worldTrans) override {
			m_modelMatrix = worldTrans;
		}

		void setKinematicTransform(const btTransform& worldTrans) {
			m_modelMatrix = worldTrans;
		}

		glm::mat4 getModelMatrix() const {
			float m[16];
			btTransform worldTrans;

			getWorldTransform(worldTrans);
			worldTrans.getOpenGLMatrix(m);

			return glm::make_mat4(m);
		}

	private:
		btTransform m_modelMatrix;
	};

} // namespace Physics

#endif // ZENENGINE_CORE_PHYSICS_MOTIONSTATE_H_