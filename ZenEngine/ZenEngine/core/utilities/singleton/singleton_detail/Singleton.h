#ifndef ZENENGINE_CORE_UTILITIES_SINGLETON_SINGLETONDETAIL_H_
#define ZENENGINE_CORE_UTILITIES_SINGLETON_SINGLETONDETAIL_H_

namespace singleton {
	template <class T>
	class Singleton {
	public:
			/**
			 * @brief		Returns the instance of the singleton.
			 * @return		Instance of the singleton.
			 */
		static T& GetInstance() {
			static T instance;
			return instance;
		}

		/// Assignment Operator
		Singleton<T>& operator=(const Singleton<T>& other) = delete;

		/// Copy Constructor
		Singleton(const Singleton<T>& other) = delete;
	private:
		/// Hidden Default Constructor
		Singleton() {}
		
		/// Hidden Destructor
		~Singleton() {}
	};
}

#endif // ZENENGINE_CORE_UTILITIES_SINGLETON_SINGLETONDETAIL_H_
