/**
* @file	Script.h
* @author	Ben Ward
* @version	1.0
* @brief Stores a script.
*/

#ifndef ZENENGINE_CORE_SCRIPTING_DETAIL_SCRIPT_H_
#define ZENENGINE_CORE_SCRIPTING_DETAIL_SCRIPT_H_

#include <string>

#include "sol.hpp"

namespace scripting {

	class Script {
	public:
			/**
			 * @brief		Constructor that sets the Script file path.
			 * @param[in]	fName	The file name/path of the Script.
			 */
		Script(const std::string& fName);

			/**
			 * @brief		Runs the Script.
			 * @param[in]	lua	Lua interpreter.
			 */
		void DoFile(sol::state& lua) const;

	private:
			/// File path of the script.
		std::string fileName;
	}; // Script

} // namespace scripting

#endif // ZENENGINE_CORE_SCRIPTING_DETAIL_SCRIPT_H_
