#ifndef ZENENGINE_CORE_SCRIPTING_DETAIL_REGISTERSTRUCTURES_H_
#define ZENENGINE_CORE_SCRIPTING_DETAIL_REGISTERSTRUCTURES_H_

#include "sol.hpp"

namespace scripting {

	void Register(sol::state& lua);

	namespace reg {
		void RegisterVector3(sol::state& lua);
		void RegisterGameObject(sol::state& lua);
		void RegisterAIState(sol::state& lua);
	}

}

#endif // ZENENGINE_CORE_SCRIPTING_DETAIL_REGISTERSTRUCTURES_H_
