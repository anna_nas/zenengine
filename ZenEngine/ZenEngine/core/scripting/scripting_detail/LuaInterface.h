#ifndef ZENENGINE_CORE_SCRIPTING_DETAIL_LUAINTERFACE_H_
#define ZENENGINE_CORE_SCRIPTING_DETAIL_LUAINTERFACE_H_

#include <string>

namespace scripting {
	struct obj {
		std::string type;
		std::string subType;
		std::string texture;
		std::string position;
		std::string rotation;
		std::string scale;
		std::string AI;

			/**
			 * @brief		Constructor to set all values.
			 * @param[in]	ty		Type of the object.
			 * @param[in]	sub		Subtype of the object.
			 * @param[in]	tex		Texture of the object.
			 * @param[in]	pos		Position of the object.
			 * @param[in]	rot		Rotation of the object.
			 * @param[in]	scal	Scale of the object.
			 * @param[in]	ai		Starting state of object.
			 */
		obj(const std::string& ty, const std::string& sub, const std::string& tex, const std::string& pos, const std::string& rot, const std::string& scal, const std::string& ai) : type(ty), subType(sub), texture(tex), position(pos), rotation(rot), scale(scal), AI(ai) {}
	};
}

#endif //ZENENGINE_CORE_SCRIPTING_DETAIL_LUAINTERFACE_H_