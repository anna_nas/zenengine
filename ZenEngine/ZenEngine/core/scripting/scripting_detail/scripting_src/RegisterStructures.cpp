#include "..\RegisterStructures.h"

#include "..\..\..\game_object\game_object_detail\GameObject_Abstract_Test.h"
#include "..\..\..\AI\ZenAI.h"
#include "gem.h"

namespace scripting {
	void Register(sol::state& lua) {
		reg::RegisterVector3(lua);
		reg::RegisterGameObject(lua);
		reg::RegisterAIState(lua);
	}

	namespace reg {
		void RegisterVector3(sol::state& lua) {
			lua.new_usertype<gem::vec3f>("Vector3", 
				sol::constructors<gem::vec3f(), gem::vec3f(float, float, float), gem::vec3f(gem::vec3f)>(),
				"x", &gem::vec3f::x,
				"y", &gem::vec3f::y,
				"z", &gem::vec3f::z
			);
		}

		void RegisterGameObject(sol::state& lua) {
			lua.new_usertype<gameobject::GameObject>("GameObject",
				"Position", &gameobject::GameObject::GetPosition,
				"ChangeState", &gameobject::GameObject::SetState,
				"MoveTo", &gameobject::GameObject::MoveTo,
				"SetPatrol", &gameobject::GameObject::SetPatrol,
				"Patrol", &gameobject::GameObject::Patrol
			);

		}

		void RegisterAIState(sol::state& lua) {
			lua.set_function("RegisterState", &zenai::StateMachine<gameobject::GameObject>::RegisterState);
			lua.new_usertype<zenai::State>("State", 
				"Enter", &zenai::State::Enter, 
				"Execute", &zenai::State::Execute, 
				"Exit", &zenai::State::Exit, 
				"OnMessage", &zenai::State::OnMessage);
		}
	}
}