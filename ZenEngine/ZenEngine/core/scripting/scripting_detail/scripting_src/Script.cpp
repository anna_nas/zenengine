#include "..\Script.h"

namespace scripting {

	Script::Script(const std::string& fName) : fileName(fName) {}

	void Script::DoFile(sol::state& lua) const {
		lua.do_file(fileName);
	}

} //namespace scripting