#include "..\..\ScriptManager.h"
#include "..\RegisterStructures.h"

#include <iostream>

ScriptManager::ScriptManager() {
	CommonConstructor("resources/scripts/init.lua");
}

ScriptManager::ScriptManager(const std::string& init) {
	CommonConstructor(init);
}

void ScriptManager::CommonConstructor(const std::string& init) {
	lua.open_libraries(sol::lib::base);

	lua.set_function("RegisterScript", &ScriptManager::RegisterScript, this);
	scripting::Register(lua);

	scripting::Script initScript(init);
	initScript.DoFile(lua);

	RegisterAIStates();
}

void ScriptManager::RegisterScript(const std::string& type, std::string& path) {
	scripts[type].emplace_back(scripting::Script(path));
}

std::vector<scripting::obj> ScriptManager::CreateObjects() {
	std::vector<scripting::obj> objects;
	for (unsigned i = 0; i < scripts.at("object").size(); i++) {
		scripts["object"][i].DoFile(lua);
		objects.emplace_back(GetString("Type"), GetString("SubType"), GetString("Texture"), GetString("Position"), GetString("Rotation"), GetString("Scale"), GetString("AI"));
	}
	return objects;
}

// -------------------Private-------------------
void ScriptManager::DoFunction(const std::string& funcName) const {
	try {
		sol::function f = lua[funcName.c_str()];
	}
	catch (sol::error e) {
		return;
	}
}

int ScriptManager::GetInt(const std::string& varName) const {
	try {
		return lua[varName.c_str()];
	}
	catch (sol::error e) {
		return 0;
	}
}

float ScriptManager::GetFloat(const std::string& varName) const {
	try {
		return lua[varName.c_str()];
	}
	catch (sol::error e) {
		return 0.0f;
	}
}

double ScriptManager::GetDouble(const std::string& varName) const
{
	try {
		return lua[varName.c_str()];
	}
	catch (sol::error e) {
		return 0.0;
	}
}

std::string ScriptManager::GetString(const std::string& varName) const {
	try {
		return lua[varName.c_str()];
	}
	catch (sol::error e) {
		return "";
	}
}

bool ScriptManager::GetBool(const std::string& varName) const{
	try {
		return lua[varName.c_str()];
	}
	catch (sol::error e) {
		return false;
	}
}

void ScriptManager::RegisterAIStates() {
	std::cout << "AI states: " << scripts["ai"].size() << std::endl;
	for (unsigned i = 0; i < scripts["ai"].size(); i++) {
		scripts["ai"][i].DoFile(lua);
	}
}
