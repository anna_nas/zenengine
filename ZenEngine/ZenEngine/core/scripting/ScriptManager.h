/**
* @file	ScriptManager.h
* @author	Ben Ward
* @version	1.0
* @brief Manages a collecion of scripts.
*/

#ifndef ZENENGINE_CORE_SCRIPTING_SCRIPTMANAGER_H_
#define ZENENGINE_CORE_SCRIPTING_SCRIPTMANAGER_H_

#include <vector>
#include <map>

#include "scripting_detail\LuaInterface.h"
#include "scripting_detail\Script.h"
#include "..\camera\camera.h"

class ScriptManager {
public:
		/**
		 * @brief		Default Constructor.
		 */
	ScriptManager();

		/**
		 * @brief		Constructor.
		 * @param[in]	init	File name of the initialization script
		 */
	ScriptManager(const std::string& init);

		/**
		 * @brief		Registers a script with the ScriptManager.
		 * @param[in]	type	Type of script that is being registered.
		 * @param[in]	path	File name/path of script to register.
		 */
	void RegisterScript(const std::string& type, std::string& path);

		/**
		 * @brief		Gets a list of object details from all the scripts registered.
		 * @return		A vector of object details.
		 */
	std::vector<scripting::obj> CreateObjects();

private:
		/// Lua interpreter.
	sol::state lua;

		/// Map of registerd scripts.
	std::unordered_map<std::string, std::vector<scripting::Script>> scripts;

		/// The common constructor that both constructers have to call.
	void CommonConstructor(const std::string& init);

		/// Calls the function funcName in a script.
	void DoFunction(const std::string& funcName) const;

		/// Returns the int value of varName from a script.
	int GetInt(const std::string& varName) const;

		/// Returns the float value of varName from a script.
	float GetFloat(const std::string& varName) const;

		/// Returns the double value of varName from a script.
	double GetDouble(const std::string& varName) const;

		/// Returns the string value of varName from a script.
	std::string GetString(const std::string& varName) const;
			
		/// Returns the bool value of varName from a script.
	bool GetBool(const std::string& varName) const;

		/// Registers all AI states from scripts.
	void RegisterAIStates();

}; //ScriptManager

#endif // ZENENGINE_CORE_SCRIPTING_SCRIPTMANAGER_H_
