//
//  Camera.h
//  OpenGLPlayground
//
//  Created by Brianna Whitcher on 13/2/18.
//  Copyright � 2018 Brianna Whitcher. All rights reserved.
//

#ifndef Camera_h
#define Camera_h

#include "glm.hpp"
#include "gtc/matrix_transform.hpp"

#include "gem.h"
//#include "../physics/RigidBody.h"
//#include "../physics/BulletPhysics.h"
#include "../game_object/ObjectManager.h"

#define default_yaw             -90.0f
#define default_pitch             0.0f
#define default_speed             6.0f
#define default_sensitivity      0.25f
#define default_zoom             45.0f


/* TO DO
* Projection - orthographic and perspective and changing between the two
* change mouse to rotation sensitivity
* no-clip mode for debugging
*/

enum Direction  { FORWARD, BACKWARD, LEFT, RIGHT };
enum Projection { PERSPECTIVE, ORTHOGRAPHIC };

class Camera
{
public:
		/// Position of the camera.
	gem::vec3f m_position;
		/// Front vector of the camera.
	gem::vec3f m_front;
		/// Up vector of the camera.
	gem::vec3f m_up;
		/// The vector to the right of the camera.
	gem::vec3f m_right;
		/// The world up vector.
	gem::vec3f m_worldUp;

		/// Rotation of the camera around the y-axis
	float m_yaw;
		/// Vertical rotation of the camera.
	float m_pitch;

		/// Movement speed of the camera.
	float m_moveSpeed;
		/// Sensitivity of the mouse when moving the camera,
	float m_mouseSensitivity;
		/// Current zoom multiplier.
	float m_zoom;
		/// Aspect ratio of the camera.
	float m_aspect;

	physics::BulletPhysics* m_World;

	std::vector<std::unique_ptr<gameobject::GameObject>>* m_GameObjs;

public:
		/**
		 * @brief		Constructor that sets the position and up vector as vectors.
		 * @param[in]	aspect		Aspect ratio.
		 * @param[in]	position	Initial position of the camera.
		 * @param[in]	up			The initial up vector of the camera.
		 * @param[in]	yaw			The initial yaw of the camera.
		 * @param[in]	pitch		The inital pitch of the camera.
		 * @param[in]	speed		The movement speed of the camera.
		 * @param[in]	sensitivity	The sensitivy of the mouse.
		 * @param[in]	zoom		The initial zoom of the camera.
		 */
	Camera(float aspect,
		gem::vec3f position = gem::vec3f(0.0f, 0.0f, 0.0f),
		gem::vec3f up = gem::vec3f(0.0f, 1.0f, 0.0f),
		float yaw = default_yaw,
		float pitch = default_pitch,
		float speed = default_speed,
		float sensitivity = default_sensitivity,
		float zoom = default_zoom);

		/**
		 * @brief		Constructor that sets the position and up vector as vectors.
		 * @param[in]	aspect		Aspect ratio.
		 * @param[in]	x_pos		The x value of the initial position.
		 * @param[in]	y_pos		The y value of the initial position.
		 * @param[in]	z_pos		The z value of the initial position.
		 * @param[in]	x_up		The x value of the initial up vector.
		 * @param[in]	y_up		The y value of the initial up vector.
		 * @param[in]	z_up		The z value of the initial up vector.
		 * @param[in]	yaw			The initial yaw of the camera.
		 * @param[in]	pitch		The inital pitch of the camera.
		 * @param[in]	speed		The movement speed of the camera.
		 * @param[in]	sensitivity	The sensitivy of the mouse.
		 * @param[in]	zoom		The initial zoom of the camera.
		 */
	Camera(float aspect,
		float x_pos, float y_pos, float z_pos,
		float x_up, float y_up, float z_up,
		float yaw, float pitch, float speed, float sensitivity, float zoom);

		/**
		 * @brief		Get the current view matrix of the camera.
		 * @return		The current view matrix of the camera.
		 */
	glm::mat4 GetViewMatrix() const { return glm::lookAt(glm::vec3(m_position.x, m_position.y, m_position.z), glm::vec3(m_position.x, m_position.y, m_position.z) + glm::vec3(m_front.x, m_front.y, m_front.z), glm::vec3(m_up.x, m_up.y, m_up.z)); }
	
		/**
		 * @brief		Get the current projection matrix of the camera.
		 * @return		The current projection matrix of the camera.
		 */
	glm::mat4 GetProjectionMatrix() const { return glm::perspective(m_zoom, m_aspect, 0.1f, 1000.0f); }
	
		/**
		 * @brief		Get the current zoom of the camera.
		 * @return		The current zoom value of the camera.
		 */
	float GetZoom() const { return m_zoom; }

		/**
		 * @brief		Get the current pitch of the camera.
		 * @return		The current pitch value of the camera.
		 */
	float GetPitch() const { return m_pitch; }

		/**
		 * @brief		Moves the camera in a direction specified.
		 * @param[in]	direction	The direction to move the camera in.
		 * @param[in]	deltaTime	The time difference since the last frame.
		 */
	void Move(Direction direction, float deltaTime);

		/**
		 * @brief		Rotates the camera based on mouse movement.
		 * @param[in]	x_offset		The x offset of the mouse.
		 * @param[in]	y_offset		The y offset of the mouse.
		 * @param[in]	constrain_pitch	Whether to constrain pitch (-89 < pitch < 89).
		 */
	void Rotate(float x_offset, float y_offset, bool constrain_pitch = true);

		/**
		 * @brief		Zooms in the camera.
		 * @param[in]	y_offset	The amount to zoom in by.
		 */
	void Zoom(float y_offset);

	/**
	* @brief		Gets pointer to world physics.
	* @param[in]	world	The world physics object.
	*/
	virtual void RegisterPhysicsWorld(physics::BulletPhysics* world) { m_World = world; };

private:
		/// Updates the front, right and up vectors of the camera.
	void UpdateVectors();
};

#endif /* Camera_h */
