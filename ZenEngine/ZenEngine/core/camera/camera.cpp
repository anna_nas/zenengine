//
//  Camera.cpp
//  OpenGLPlayground
//
//  Created by Brianna Whitcher on 13/2/18.
//  Copyright � 2018 Brianna Whitcher. All rights reserved.
//

#include "Camera.h"
#include <iostream>
//#include "../physics/BulletConversions.h"
//#include "../physics/colliders/BoxCollider.h"

Camera::Camera(float aspect,
	gem::vec3f position,
	gem::vec3f up,
	float yaw, float pitch, float speed, float sensitivity, float zoom) :
	m_position(position),
	m_worldUp(up),
	m_yaw(yaw),
	m_pitch(pitch),
	m_moveSpeed(speed),
	m_mouseSensitivity(sensitivity),
	m_zoom(zoom),
	m_aspect(aspect)
{
	UpdateVectors();
}

Camera::Camera(float aspect,
	float x_pos, float y_pos, float z_pos,
	float x_up, float y_up, float z_up,
	float yaw, float pitch, float speed, float sensitivity, float zoom) :
	m_position(gem::vec3f(x_pos, y_pos, z_pos)),
	m_worldUp(gem::vec3f(x_up, y_up, z_up)),
	m_yaw(yaw),
	m_pitch(pitch),
	m_moveSpeed(speed),
	m_mouseSensitivity(sensitivity),
	m_zoom(zoom),
	m_aspect(aspect)
{
	UpdateVectors();
}

void Camera::Move(Direction direction, float deltaTime)
{
	//no clip also add y component

	gem::vec3f target_position(0.0f, 0.0f, 0.0f);

	float velocity = m_moveSpeed * deltaTime;

	if (direction == FORWARD) {
		target_position.x += m_front.x;
		target_position.z += m_front.z;
	}

	if (direction == BACKWARD) {
		target_position.x -= m_front.x;
		target_position.z -= m_front.z;

	}

	if (direction == LEFT) {
		target_position.x -= m_right.x;
		target_position.z -= m_right.z;
	}

	if (direction == RIGHT) {
		target_position.x += m_right.x;
		target_position.z += m_right.z;
	}

	target_position = m_position + (gem::normalize(target_position) * velocity);
	bool isValidMove = true;
	for (unsigned int i = 1; i < m_GameObjs->size() && isValidMove == true; i++) {
		if (gem::distance(m_GameObjs->at(i)->GetPosition(), target_position) < 3.3f) {
			isValidMove = false;
		}
	}
	if (isValidMove) {
		m_position = target_position;
	}
}

void Camera::Rotate(float x_offset, float y_offset, bool constrain_pitch)
{
	m_yaw += (x_offset * m_mouseSensitivity);
	m_pitch += (y_offset * m_mouseSensitivity);

	if (constrain_pitch)
	{
		if (m_pitch >  79.0f) { m_pitch = 79.0f; }
		if (m_pitch < -79.0f) { m_pitch = -79.0f; }
	}

	UpdateVectors();
}

void Camera::Zoom(float y_offset)
{
	if (1.0f <= m_zoom && m_zoom >= 45.0f) { m_zoom -= y_offset; }
	if (m_zoom <  1.0f) { m_zoom = 1.0f; }
	if (m_zoom > 45.0f) { m_zoom = 45.0f; }
}

void Camera::UpdateVectors()
{
	gem::vec3f front(cos(glm::radians(m_yaw)) * cos(glm::radians(m_pitch)),
		sin(glm::radians(m_pitch)),
		sin(glm::radians(m_yaw)) * cos(glm::radians(m_pitch)));

	m_front = gem::normalize(front);
	m_right = gem::normalize(gem::cross(m_front, m_worldUp));
	m_up = gem::normalize(gem::cross(m_right, m_front));
}

/*
void Camera::CreatePhysicsBody() {
	physics::ZenCollider* shape = new physics::BoxCollider(gem::vec3f(4.0f));
	physics::ZenTransform transform(m_position, gem::vec3f(m_yaw, m_pitch, 0.0f));
	m_Body = new physics::RigidBody(transform, 0.0f, shape, physics::ObjectType::KINEMATIC);// DYNAMIC);
}
*/
