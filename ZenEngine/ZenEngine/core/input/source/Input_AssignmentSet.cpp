/*#include "../header/Input_AssignmentSet.hpp"

namespace zenInput
{
	Assignment::Assignment(ZEN_ALIASKEY aliasReference, ZEN_TRIGGER triggerReference)
	{
		// Copy in the user-provided values
		aliasOfReference = aliasReference;
		triggerOfReference = triggerReference;
	}

	Assignment::Assignment(ZEN_STRING assignmentAsString)
	{
		// Copy in via a string
		setViaString(assignmentAsString);
	}

	ZEN_BOOL Assignment::setViaString(ZEN_STRING assignmentAsString)
	{

	}

	ZEN_BOOL Assignment::operator==(const Assignment &otherAssignment)
	{
		// Just compare their basic values
		return ((this->aliasOfReference == otherAssignment.aliasOfReference) && (this->triggerOfReference == otherAssignment.triggerOfReference));
	}

	AssignmentSet::AssignmentSet()
	{
		// N/a
	}

	AssignmentSet::AssignmentSet(ZEN_STRING &fileLocation, AliasSet &aliasVerification)
	{
		// Copy in via a file
		setAssignmentsViaFile(fileLocation, aliasVerification);
	}

	AssignmentSet::~AssignmentSet()
	{
		// Empty the assignment storage
		assignmentStorage.clear();

		// Empty the key storage
		assignmentMapKeys.clear();

		// Empty the keys buffer
		clearKeysBuffer(triggeredAliasKeys);

		// Empty the data buffer
		clearDataBuffer(triggeredAliasData);
	}

	void AssignmentSet::setAssignment(ZEN_ALIASKEY index, ZEN_TRIGGER trigger, AliasSet &aliasVerification)
	{
		// Keep track of the condition that the user's input is valid
		ZEN_BOOL inputIsValid = true;
		
		// Firstly verify that the key index is valid
		if (!aliasVerification.isAlias(index))
		{
			// Catch invalid alias keys
			inputIsValid = false;
		}

		// Secondly verify that the trigger is valie
		if (trigger > inputTriggers::ForwardSlash_Release)
		{
			// Catch invalid trigger keys
			inputIsValid = false;
		}

		// Finally add the assignment if it is valid
		if (inputIsValid)
		{
			// Create an assignment to store
			Assignment toStore(index, trigger);

			// Create (and store) a key to store the assignment with
			if (assignmentMapKeys.size() == 0)
			{
				// Start back at 0
				assignmentMapKeys.push_back(0);
			}
			else
			{
				// Guarantee the value is unique
				assignmentMapKeys.push_back(assignmentMapKeys.back + 1);
			}

			// Store the assignment
			assignmentStorage.emplace(assignmentMapKeys.back, toStore);
		}
	}

	void AssignmentSet::setAssignmentsViaFile(ZEN_STRING &fileLocation, AliasSet &aliasVerification)
	{

	}

	void AssignmentSet::clearAssignment(ZEN_UINT index, ZEN_KEYSELECTION typeToDelete)
	{
		// Create a temp vector to store the keys of assignments that are to be deleted
		ZEN_ASSIGNMENTKEYSTORAGE toDelete;
		
		// Loop through all of the stored keys
		ZEN_UINT loopCount = 0;
		while (loopCount < assignmentMapKeys.size())
		{
			ZEN_ASSIGNMENTMAP_CONSTITERATOR temp = assignmentStorage.find(assignmentMapKeys[loopCount]);

			// Traverse the map
			if (temp == assignmentStorage.end())
			{
				// Catch invalid map content
			}
			else
			{
				// Determine the type of data to remove
				if (!typeToDelete)
				{
					// For aliases
					if (temp->second.aliasOfReference == index)
					{
						// Add the key to the list for deletion
						toDelete.push_back(index);
					}
				}
				else
				{
					// For triggers
					if (temp->second.triggerOfReference == index)
					{
						// Add the key to the list for deletion
						toDelete.push_back(index);
					}
				}
			}

			// Increment the loop counter
			loopCount++;
		}

		// Loop through and delete all assignments for deletion
		loopCount = 0;
		while (loopCount < toDelete.size())
		{
			// Delete the assignment set at the provided key
			assignmentStorage.erase(toDelete[loopCount]);

			// Increment the loop counter
			loopCount++;
		}

		// Loop through and delete all of the stored keys for deletion
		loopCount = 0;
		while (loopCount < toDelete.size())
		{
			// Traverse through the stored map keys
			ZEN_UINT keyLoopCount = 0;
			ZEN_BOOL hasNotBeenFoundYet = true;
			while ((keyLoopCount < assignmentMapKeys.size()) && hasNotBeenFoundYet)
			{
				// Only delete it if its suppossed to be deleted
				if (assignmentMapKeys[keyLoopCount] == toDelete[loopCount])
				{
					// Delete the stored key
					assignmentMapKeys.erase(assignmentMapKeys.begin() + keyLoopCount);

					// Set the flag to stop traversal
					hasNotBeenFoundYet = false;
				}
			}
		}

		// Clear the temp vector
		toDelete.clear();
	}

	ZEN_STRING AssignmentSet::listAssignments()
	{

	}

	ZEN_STRING AssignmentSet::listAssignments(ZEN_UINT index, ZEN_KEYSELECTION typeToList)
	{

	}

	void AssignmentSet::triggerAssignments(ZEN_TRIGGER trigger, ZEN_EVENTDATA data)
	{
		// Loop through all of the stored keys
		ZEN_UINT loopCount = 0;
		while (loopCount < assignmentMapKeys.size())
		{
			ZEN_ASSIGNMENTMAP_CONSTITERATOR temp = assignmentStorage.find(assignmentMapKeys[loopCount]);

			// Traverse the map
			if (temp == assignmentStorage.end())
			{
				// Catch invalid map content
			}
			else if (temp->second.triggerOfReference == trigger)
			{
				// Add the alias key to the buffer
				triggeredAliasKeys.push(temp->second.aliasOfReference);

				// Add the event data to the buffer
				triggeredAliasData.push(data);
			}

			// Increment the loop counter
			loopCount++;
		}
	}

	ZEN_ALIASKEY AssignmentSet::pollEventKeysBuffer()
	{
		// Catch an empty buffer
		if (isEventKeysBufferEmpty())
		{
			return NULL;
		}
		else
		{
			// Pop the data from the end of the buffer
			ZEN_ALIASKEY temp = triggeredAliasKeys.front();
			triggeredAliasKeys.pop();

			// Return the data
			return temp;
		}
	}

	ZEN_BOOL AssignmentSet::isEventKeysBufferEmpty()
	{
		return triggeredAliasKeys.empty();
	}

	ZEN_EVENTDATA AssignmentSet::pollEventDataBuffer()
	{
		// Catch an empty buffer
		if (isEventKeysBufferEmpty())
		{
			return NULL;
		}
		else
		{
			// Pop the data from the end of the buffer
			ZEN_EVENTDATA temp = triggeredAliasKeys.front();
			triggeredAliasKeys.pop();

			// Return the data
			return temp;
		}
	}

	ZEN_BOOL AssignmentSet::isEventDataBufferEmpty()
	{
		return triggeredAliasData.empty();
	}

	void AssignmentSet::clearKeysBuffer(ZEN_EVENTKEYSBUFFER &bufferToClear)
	{
		// Create a temp empty buffer for clearing purposes
		ZEN_EVENTKEYSBUFFER empty;
		std::swap(bufferToClear, empty);
	}

	void AssignmentSet::clearDataBuffer(ZEN_EVENTDATABUFFER &bufferToClear)
	{
		// Create a temp empty buffer for clearing purposes
		ZEN_EVENTDATABUFFER empty;
		std::swap(bufferToClear, empty);
	}
}*/