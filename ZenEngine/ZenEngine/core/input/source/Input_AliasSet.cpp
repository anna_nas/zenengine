/*#include "../header/Input_AliasSet.hpp"

namespace zenInput
{
	AliasSet::AliasSet()
	{
		// N/a
	}

	AliasSet::AliasSet(ZEN_STRING fileLocation)
	{
		// No prior construction required
		
		addAliasSetViaFile(fileLocation);
	}

	AliasSet::~AliasSet()
	{
		// Empty the map of all data
		aliasStorage.clear();
	}

	void AliasSet::addAlias(ZEN_ALIAS newAlias)
	{
		// Prevent empty or whitespace-containing aliases
		ZEN_BOOL validAlias = true;
		ZEN_UINT loopCounter = 0;
		while (loopCounter < newAlias.size() && validAlias)
		{
			// Check if each character is a whitespace value
			if (isspace(newAlias[loopCounter]))
			{
				// Report if its a whitespace characer
				validAlias = false;
			}

			// Increment the loop counter
			loopCounter++;
		}
		// Catch empty strings
		if (newAlias.size() == 0)
		{
			// Report if its an invalid string
			validAlias = false;
		}

		// Only continue if the alias is valid
		if (validAlias)
		{
			// Attempt to find if the alias already is stored
			loopCounter = 0;
			ZEN_BOOL duplicateExists = false;
			while (loopCounter < aliasStorage.size())
			{
				// Search through the map
				ZEN_ALIASMAP_CONSTITERATOR temp = aliasStorage.find(loopCounter);
				if (temp == aliasStorage.end())
				{
					// Catch invalid keys
				}
				else
				{
					// Compare the data
					if (temp->second.compare(newAlias))
					{
						// Mark the input as a duplicate
						duplicateExists = true;
					}
				}

				// Iterate the counter
				loopCounter++;
			}

			// Only add the new alias if it isn't a duplicate
			if (!duplicateExists)
			{
				// 'Append' the new alias 
				aliasStorage.emplace(aliasStorage.size(), newAlias);
			}
		}
	}

	void AliasSet::addAliasSetViaFile(ZEN_STRING fileLocation)
	{
		
	}

	ZEN_STRING AliasSet::getAlias(ZEN_ALIASKEY index)
	{
		// Search through the map
		ZEN_ALIASMAP_CONSTITERATOR temp = aliasStorage.find(index);
		if (temp == aliasStorage.end())
		{
			// Catch invalid keys, and return an empty string (impossible to store) to indicate as such
			return "";
		}
		else
		{
			// Return the alias
			return temp->second;
		}
	}

	ZEN_UINT AliasSet::size()
	{
		// Return the size of the map
		return aliasStorage.size();
	}

	ZEN_BOOL AliasSet::isAlias(ZEN_ALIASKEY index)
	{
		// Search through the map
		ZEN_ALIASMAP_CONSTITERATOR temp = aliasStorage.find(index);
		if (temp == aliasStorage.end())
		{
			return false;
		}
		else // The alias exists
		{
			return true;
		}
	}
}*/