/*#include "../header/Input_SetManagement.hpp"

namespace zenInput
{
	InputSetManagement::InputSetManagement()
	{
		// Initialize the set manager to not store an 'active' set by default
		activeAssignmentSet = NULL;
	}

	InputSetManagement::~InputSetManagement()
	{
		// Clear the assignment sets
		assignmentSets.clear();

		// Clear the collection of assignment set keys
		assignmentSetKeys.clear();

		// Remove the 'active' assignment set key
		activeAssignmentSet = NULL;
	}

	void InputSetManagement::createAssignmentSet(ZEN_ASSIGNMENTSETKEY newKey)
	{
		// Only create a new set with the provided key if it is unique
		if (!isAssignmentSet(newKey))
		{
			// Insert a new (empty) set at the provided
			ZEN_ASSIGNMENTSETKEY tempSet;
			assignmentSets.emplace(newKey, tempSet);

			// Create (and store) a key to store the assignment set with
			if (assignmentSetKeys.size() == 0)
			{
				// Start back at 0
				assignmentSetKeys.push_back(0);
			}
			else
			{
				// Guarantee the value is unique
				assignmentSetKeys.push_back(assignmentSetKeys[assignmentSetKeys.size()] + 1);
			}
		}
	}

	void InputSetManagement::createAssignmentSet(ZEN_ASSIGNMENTSETKEY newKey, ZEN_STRING fileLocation)
	{

	}

	void InputSetManagement::deleteAssignmentSet(ZEN_ASSIGNMENTSETKEY existingKey)
	{
		// Confirm that the key is valid
		ZEN_ASSIGNMENTSETMAP_CONSTITERATOR temp = assignmentSets.find(assignmentSetKeys[existingKey]);

		// Traverse the map
		if (temp == assignmentSets.end())
		{
			// Catch invalid map content
		}
		else
		{
			// If the set to delete is currently active
			if (activeAssignmentSet == existingKey)
			{
				// Set the set management active set to NULL
				activeAssignmentSet = NULL;
			}
			
			// Delete the stored key
			ZEN_UINT loopCount = 0;
			ZEN_BOOL isDeleted = false;
			while ((loopCount == 0) && (!isDeleted))
			{

			}

			// Delete the stored set
		}
	}

	ZEN_STRING InputSetManagement::listAssignmentSetKeys()
	{

	}

	ZEN_BOOL InputSetManagement::isAssignmentSet(ZEN_ASSIGNMENTSETKEY keyIn)
	{
		// Confirm that the key is valid
		ZEN_ASSIGNMENTSETMAP_CONSTITERATOR temp = assignmentSets.find(assignmentSetKeys[keyIn]);

		// Traverse the map
		if (temp == assignmentSets.end())
		{
			// Catch invalid map content
			return false;
		}
		else
		{
			// Alert that it is valid
			return true;
		}
	}

	void InputSetManagement::setActiveAssignmentSet(ZEN_ASSIGNMENTSETKEY newActiveSetKey)
	{
		// Only allow the set key to be used if its valid
		if (isAssignmentSet(newActiveSetKey))
		{
			// Set the active key to the user defined value
			activeAssignmentSet = newActiveSetKey;
		}
	}

	ZEN_ASSIGNMENTSETKEY InputSetManagement::getActiveAssignmentSet()
	{
		return activeAssignmentSet;
	}

	ZEN_ALIASKEY InputSetManagement::pollGlobalEventKeysBuffer()
	{
		// Catch an empty buffer
		if (isGlobalEventKeysBufferEmpty())
		{
			return NULL;
		}
		else
		{
			// Pop the data from the end of the buffer
			ZEN_ALIASKEY temp = globalKeysBuffer.front();
			globalKeysBuffer.pop();

			// Return the data
			return temp;
		}
	}

	ZEN_BOOL InputSetManagement::isGlobalEventKeysBufferEmpty()
	{
		return globalKeysBuffer.empty();
	}

	ZEN_EVENTDATA InputSetManagement::pollGlobalEventDataBuffer()
	{
		// Catch an empty buffer
		if (isGlobalEventKeysBufferEmpty())
		{
			return NULL;
		}
		else
		{
			// Pop the data from the end of the buffer
			ZEN_EVENTDATA temp = globalDataBuffer.front();
			globalDataBuffer.pop();

			// Return the data
			return temp;
		}
	}

	ZEN_BOOL InputSetManagement::isGlobalEventDataBufferEmpty()
	{
		return globalDataBuffer.empty();
	}

	void InputSetManagement::setAssignment(ZEN_ALIASKEY index, ZEN_TRIGGER trigger)
	{
		// Prevent an invalid NULL set from being used
		if (activeAssignmentSet != NULL)
		{
			assignmentSets[activeAssignmentSet].setAssignment(index, trigger, applicationAliases);
		}
	}

	void InputSetManagement::setAssignmentsViaFile(ZEN_STRING fileLocation)
	{
		// Prevent an invalid NULL set from being used
		if (activeAssignmentSet != NULL)
		{
			assignmentSets[activeAssignmentSet].setAssignmentsViaFile(fileLocation, applicationAliases);
		}
	}

	void InputSetManagement::clearAssignment(ZEN_ALIASKEY index, ZEN_KEYSELECTION typeToDelete)
	{
		// Prevent an invalid NULL set from being used
		if (activeAssignmentSet != NULL)
		{
			assignmentSets[activeAssignmentSet].clearAssignment(index, typeToDelete);
		}
	}

	ZEN_STRING InputSetManagement::listAssignments()
	{
		// Prevent an invalid NULL set from being used
		if (activeAssignmentSet != NULL)
		{
			return assignmentSets[activeAssignmentSet].listAssignments();
		}
	}

	ZEN_STRING InputSetManagement::listAssignments(ZEN_UINT index, ZEN_KEYSELECTION typeToList)
	{
		// Prevent an invalid NULL set from being used
		if (activeAssignmentSet != NULL)
		{
			return assignmentSets[activeAssignmentSet].listAssignments(index, typeToList);
		}
	}

	void InputSetManagement::triggerAssignments(ZEN_TRIGGER trigger, ZEN_EVENTDATA data)
	{
		// Prevent an invalid NULL set from being used
		if (activeAssignmentSet != NULL)
		{
			// Trigger the active assignment set
			assignmentSets[activeAssignmentSet].triggerAssignments(trigger, data);

			// Copy the values over into the global buffer
			while ((!assignmentSets[activeAssignmentSet].isEventDataBufferEmpty()) && (!assignmentSets[activeAssignmentSet].isEventKeysBufferEmpty()))
			{
				// Copy the keys
				globalKeysBuffer.push(assignmentSets[activeAssignmentSet].pollEventKeysBuffer());

				// Copy the data
				globalDataBuffer.push(assignmentSets[activeAssignmentSet].pollEventDataBuffer());
			}
		}
	}

	void InputSetManagement::addAlias(ZEN_ALIAS newAlias)
	{
		applicationAliases.addAlias(newAlias);
	}

	void InputSetManagement::addAliasSetViaFile(ZEN_STRING fileLocation)
	{
		applicationAliases.addAliasSetViaFile(fileLocation);
	}

	ZEN_STRING InputSetManagement::getAlias(ZEN_ALIASKEY index)
	{
		return applicationAliases.getAlias(index);
	}

	ZEN_UINT InputSetManagement::getAliasAmount()
	{
		return applicationAliases.size();
	}

	ZEN_BOOL InputSetManagement::isAlias(ZEN_ALIASKEY index)
	{
		return applicationAliases.isAlias(index);
	}
}*/