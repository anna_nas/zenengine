/*#ifndef ZENENGINE_INPUTSYSTEM_INPUT_TYPES_H_
#define ZENENGINE_INPUTSYSTEM_INPUT_TYPES_H_

#include <string>
#include <vector>
#include <queue>
#include <unordered_map>

	//! Copyright William Vlahos 2018
namespace zenInput
{
	//-- Basic Type Definitions to Limit Cross-Compatibility Issues --//

		//! Type definition for strings
	using ZEN_STRING = std::string;
		//! Type definition for integers
	using ZEN_INT = int;
		//! Type definition for unsigned integers
	using ZEN_UINT = unsigned int;
		//! Type definition for characters
	using ZEN_CHAR = char;
		//! Type definition for unsigned characters
	using ZEN_UCHAR = unsigned char;
		//! Type definition for booleans
	using ZEN_BOOL = bool;


	//-- Alias Set Unique Types --//

		//! Type definition for the alias
	using ZEN_ALIAS = ZEN_STRING;
		//! Type definition for the alias key
	using ZEN_ALIASKEY = ZEN_UINT;


	//-- Assignment Set Unique Types --//

		//! Type definition for the data created in an input event
	using ZEN_EVENTDATA = ZEN_UINT;
		//! Type definition for the assignment key
	using ZEN_ASSIGNMENTKEY = ZEN_UINT;
		//! Type definition for determing what key to be using
	using ZEN_KEYSELECTION = ZEN_BOOL;
		//! Type definition for the storage of assignment keys
	using ZEN_ASSIGNMENTKEYSTORAGE = std::vector<ZEN_ASSIGNMENTKEY>;
		//! Type definition for a buffer containing the keys of triggered aliases as a result of an input event
	using ZEN_EVENTKEYSBUFFER = std::queue<ZEN_ALIASKEY>;
		//! Type definition for a buffer containing the the data relative to the keys of an input event
	using ZEN_EVENTDATABUFFER = std::queue<ZEN_UINT>;


	//-- Set Management Unique Types --//

		//! Type definition for the assignment set key
	using ZEN_ASSIGNMENTSETKEY = ZEN_UINT;
		//! Type definition for the assignment key storage
	using ZEN_ASSIGNMENTSETKEYSTORAGE = std::vector<ZEN_ASSIGNMENTSETKEY>;


	//-- Input Trigger Unique Types --//

		//! Type definition for the trigger key
	using ZEN_TRIGGER = ZEN_UCHAR;
		//! Enum to define the id/key of each trigger
	enum inputTriggers
	{
			// Mouse Buttons
		Mouse1_Press,
		Mouse1_Release,
		Mouse2_Press,
		Mouse2_Release,
		Mouse3_Press,
		Mouse3_Release,

			// Mouse Scroll Wheel
		MouseScrollX_Up,
		MouseScrollX_Down,
		MouseScrollY_Up,
		MouseScrollY_Down,

			// Mouse Movement
		MouseMovX_Up,
		MouseMovX_Down,
		MouseMovY_Up,
		MouseMovY_Down,

			// Keyboard Character Buttons
		Q_Press,
		Q_Release,
		W_Press,
		W_Release,
		E_Press,
		E_Release,
		R_Press,
		R_Release,
		T_Press,
		T_Release,
		Y_Press,
		Y_Release,
		U_Press,
		U_Release,
		I_Press,
		I_Release,
		O_Press,
		O_Release,
		P_Press,
		P_Release,
		A_Press,
		A_Release,
		S_Press,
		S_Release,
		D_Press,
		D_Release,
		F_Press,
		F_Release,
		G_Press,
		G_Release,
		H_Press,
		H_Release,
		J_Press,
		J_Release,
		K_Press,
		K_Release,
		L_Press,
		L_Release,
		Z_Press,
		Z_Release,
		X_Press,
		X_Release,
		C_Press,
		C_Release,
		V_Press,
		V_Release,
		B_Press,
		B_Release,
		N_Press,
		N_Release,
		M_Press,
		M_Release,

			// Keyboard Number Buttons
		NUM0_Press,
		NUM0_Release,
		NUM1_Press,
		NUM1_Release,
		NUM2_Press,
		NUM2_Release,
		NUM3_Press,
		NUM3_Release,
		NUM4_Press,
		NUM4_Release,
		NUM5_Press,
		NUM5_Release,
		NUM6_Press,
		NUM6_Release,
		NUM7_Press,
		NUM7_Release,
		NUM8_Press,
		NUM8_Release,
		NUM9_Press,
		NUM9_Release,

			// Keyboard Function Buttons
		FN1_Press,
		FN1_Release,
		FN2_Press,
		FN2_Release,
		FN3_Press,
		FN3_Release,
		FN4_Press,
		FN4_Release,
		FN5_Press,
		FN5_Release,
		FN6_Press,
		FN6_Release,
		FN7_Press,
		FN7_Release,
		FN8_Press,
		FN8_Release,
		FN9_Press,
		FN9_Release,
		FN10_Press,
		FN10_Release,
		FN11_Press,
		FN11_Release,
		FN12_Press,
		FN12_Release,

			// Keyboard Modifier Buttons
		Esc_Press,
		Esc_Release,
		Space_Press,
		Space_Release,
		Enter_Press,
		Enter_Release,
		Tab_Press,
		Tab_Release,
		LockCaps_Press,
		LookCaps_Release,
		LockNum_Press,
		LockNum_Release,
		ShiftL_Press,
		ShiftL_Release,
		ShiftR_Press,
		ShiftR_Release,
		CtrlL_Press,
		CtrlL_Release,
		CtrlR_Press,
		CtrlR_Release,
		AltL_Press,
		AltL_Release,
		AltR_Press,
		AltR_Release,
		Insert_Press,
		Insert_Release,
		Delete_Press,
		Delete_Release,
		Home_Press,
		Home_Release,
		End_Press,
		End_Release,
		PageUp_Press,
		PageUp_Release,
		PageDown_Press,
		PageDown_Release,

			// Keyboard Symbol Buttons
		BackQuote_Press, // `
		BackQuote_Release,
		Minus_Press, // -
		Minus_Release,
		Plus_Press, // +
		Plus_Release,
		BracketOpen_Press, // [
		BrackerOpen_Release,
		BracketClose_Press, // ]
		BracketClose_Release,
		BackSlash_Press, //  \
		BackSlash_Release,
		SemiColon_Press, // ;
		SemiColon_Release,
		Apostrophe_Press, // '
		Apostrophe_Release,
		Comma_Press, // ,
		Comma_Release,
		FullStop_Press, // .
		FullStop_Release,
		ForwardSlash_Press, // /
		ForwardSlash_Release,
	};
}

#endif // ZENENGINE_INPUTSYSTEM_INPUT_TYPES_H_*/