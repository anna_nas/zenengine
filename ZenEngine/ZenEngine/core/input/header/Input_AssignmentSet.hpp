/*#ifndef ZENENGINE_INPUTSYSTEM_INPUT_ASSIGNMENTSET_H_
#define ZENENGINE_INPUTSYSTEM_INPUT_ASSIGNMENTSET_H_

#include "Input_AliasSet.hpp"

	//! Copyright William Vlahos 2018
namespace zenInput
{
		//! Struct definition for the assignment of an event alias to a trigger
	struct Assignment
	{
			//! Alias key that references a stored alias
		ZEN_ALIASKEY aliasOfReference;

			//! Trigger key that references a valid trigger
		ZEN_TRIGGER triggerOfReference;

			//! Assignment creation requires a key and trigger, but validation not required
		Assignment(ZEN_ALIASKEY aliasReference, ZEN_TRIGGER triggerReference);

			//! Assignment creation with a string, with the key and trigger being seperated by a comma
		Assignment(ZEN_STRING assignmentAsString);

			//! Setting of values with a string, with the key and trigger being seperated by a comma
		ZEN_BOOL setViaString(ZEN_STRING assignmentAsString);

			//! Equality comparison operator overloading
		ZEN_BOOL operator==(const Assignment &otherAssignment);
	};

		//! Type definition for the assignment map
	using ZEN_ASSIGNMENTMAP = std::unordered_map<ZEN_ASSIGNMENTKEY, Assignment>;

		//! Type definition for the const assignment map iterator
	using ZEN_ASSIGNMENTMAP_CONSTITERATOR = std::unordered_map<ZEN_ASSIGNMENTKEY, Assignment>::const_iterator;

	class AssignmentSet
	{
	protected:
			// Stores an unordered map of assignments
		ZEN_ASSIGNMENTMAP assignmentStorage;

			// Stores a vector of the map keys, used exclusively to traverse through the map
		ZEN_ASSIGNMENTKEYSTORAGE assignmentMapKeys;

			//! Stores the aliases that have been triggered in a buffer
		ZEN_EVENTKEYSBUFFER triggeredAliasKeys;

			//! Stores the aliases that have been triggered in a buffer
		ZEN_EVENTDATABUFFER triggeredAliasData;

			//! Clears a ZEN_EVENTKEYSBUFFER
		void clearKeysBuffer(ZEN_EVENTKEYSBUFFER &bufferToClear);

			//! Clears a ZEN_EVENTDATABUFFER
		void clearDataBuffer(ZEN_EVENTDATABUFFER &bufferToClear);

	public:
			//! Default constructor
		AssignmentSet();

			//! Constructor that adds assignments from a file
		AssignmentSet(ZEN_STRING &fileLocation, AliasSet &aliasVerification);

			//! Default destructor
		~AssignmentSet();

			//! Define a trigger for a stored game event alias
		void setAssignment(ZEN_ALIASKEY index, ZEN_TRIGGER trigger, AliasSet &aliasVerification);

			//! Create and add assignments
		void setAssignmentsViaFile(ZEN_STRING &fileLocation, AliasSet &aliasVerification);

			/*!
			 * Remove all assignments that contain the provided alias/trigger
			 * typeToDelete - True = alias key, False = trigger key
			 */ /*
		void clearAssignment(ZEN_UINT index, ZEN_KEYSELECTION typeToDelete);

			//! Lists each assignment, with each line being a different assignment
		ZEN_STRING listAssignments();

			/*!
			 * Lists each assignment with the alias/trigger provided, with each index being on a seperate line
			 * @param typeToDelete - True = alias key, False = trigger key
			 */ /*
		ZEN_STRING listAssignments(ZEN_UINT index, ZEN_KEYSELECTION typeToList);

			//! Updates the alias buffer to represent the newly triggered events
		void triggerAssignments(ZEN_TRIGGER trigger, ZEN_EVENTDATA data);

			//! Returns the top of the buffer of triggered aliases (more specifically their keys), or NULL
		ZEN_ALIASKEY pollEventKeysBuffer();

			//! Returns TRUE if the triggered alias buffer contains 1 or more elements
		ZEN_BOOL isEventKeysBufferEmpty();

			//! Returns the top of the buffer of the trigger data, or NULL
		ZEN_EVENTDATA pollEventDataBuffer();

			//! Returns TRUE if the trigger data buffer contains 1 or more elements
		ZEN_BOOL isEventDataBufferEmpty();
	};
}

#endif // ZENENGINE_INPUTSYSTEM_INPUT_ASSIGNMENTSET_H_*/