#ifndef INPUT_DEMOHACK
#define INPUT_DEMOHACK

// !!! FOR TEMP USE ONLY - REPLACE ASAP !!!
extern unsigned char TEMPUSE_renderingTask;
extern bool TEMPUSE_wireframeStatus;

// Defines the object/process to render
// !!! FOR TEMP USE ONLY - REPLACE ASAP !!!
enum renderingMethod
{
	Game_Demo = 0,
	Screen_Help = 1,
	Screen_Splash = 2
};
// Defines how to render the current process
// !!! FOR TEMP USE ONLY - REPLACE ASAP !!!
enum wireframeRendering
{
	None = false,
	Wireframe = true
};

#endif // INPUT_DEMOHACK