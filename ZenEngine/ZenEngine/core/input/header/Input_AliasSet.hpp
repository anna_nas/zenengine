/*#ifndef ZENENGINE_INPUTSYSTEM_INPUT_ALIASSET_H_
#define ZENENGINE_INPUTSYSTEM_INPUT_ALIASSET_H_

#include "Input_Types.hpp"

	//! Copyright William Vlahos 2018
namespace zenInput
{
		//! Type definition for the alias map
	using ZEN_ALIASMAP = std::unordered_map<ZEN_ALIASKEY, ZEN_ALIAS>;

		//! Type definition for the constant alias map iterator
	using ZEN_ALIASMAP_CONSTITERATOR = std::unordered_map<ZEN_ALIASKEY, ZEN_ALIAS>::const_iterator;

	class AliasSet
	{
	protected:
		ZEN_ALIASMAP aliasStorage;

	public:
			//! Default constructor - no initial values
		AliasSet();

			//! Location to file where event aliases are stored
		AliasSet(ZEN_STRING fileLocation);

			//! Default destructor
		~AliasSet();

			//! Add an alias for a game event, only unique aliases will be stored
		void addAlias(ZEN_ALIAS newAlias);

			//! Add a list of aliases for storage from a file
		void addAliasSetViaFile(ZEN_STRING fileLocation);

			//! Get an event alias at the provided index
		ZEN_ALIAS getAlias(ZEN_ALIASKEY index);

			//! Get the amount of aliases stored
		ZEN_UINT size();

			//! Checks if the index provided represents a stored alias
		ZEN_BOOL isAlias(ZEN_ALIASKEY index);
	};
}

#endif // ZENENGINE_INPUTSYSTEM_INPUT_ALIASSET_H_*/