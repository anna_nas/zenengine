/*#ifndef ZENENGINE_INPUTSYSTEM_INPUT_SYSTEMINTERFACE_H_
#define ZENENGINE_INPUTSYSTEM_INPUT_SYSTEMINTERFACE_H_

#include "Input_SetManagement.hpp"

	//! Copyright William Vlahos 2018
namespace zenInput
{
	/*
	 * All of the following would be connected to the scripting system to allow for modification on the fly
	 * Event aliases cannot be deleted at any point during runtime except for when closing the program
	 * Defining and deleting aliases/assignments can be expensive - do it in menus or at start-up ONLY!
	 * A definition of the triggers for the ZenEngine can be found in "ZenInput_Types.hpp"
	 */ /*

		//! Creates a new empty assignment set
	void createAssignmentSet(ZEN_ASSIGNMENTSETKEY newKey);

		//! Creates a new assignment set, filling it via a file
	void createAssignmentSet(ZEN_ASSIGNMENTSETKEY newKey, ZEN_STRING fileLocation);

		//! Deletes a stored assignment set (if it exists)
	void deleteAssignmentSet(ZEN_ASSIGNMENTSETKEY existingKey);

		//! Returns a string of assignment set keys, with each key being on a new line
	ZEN_STRING listAssignmentSetKeys();

		//! Returns true if the assignment set key is valid/stored
	ZEN_BOOL isAssignmentSet(ZEN_ASSIGNMENTSETKEY keyIn);

		//! Sets the active assignment set to that which is represented by the user-provided key (if it is valid)
	void setActiveAssignmentSet(ZEN_ASSIGNMENTSETKEY newActiveSetKey);

		//! Returns the key-value of the active assignment set
	ZEN_ASSIGNMENTSETKEY getActiveAssignmentSet();

		//! Returns the top of the buffer of triggered aliases (more specifically their keys), or NULL
	ZEN_ALIASKEY pollGlobalEventKeysBuffer();

		//! Returns TRUE if the triggered alias buffer contains 1 or more elements
	ZEN_BOOL isGlobalEventKeysBufferEmpty();

		//! Returns the top of the buffer of the trigger data, or NULL
	ZEN_EVENTDATA pollGlobalEventDataBuffer();

		//! Returns TRUE if the trigger data buffer contains 1 or more elements
	ZEN_BOOL isGlobalEventDataBufferEmpty();

		//! Define a trigger for a stored game event alias in the active assigment set
	void setAssignment(ZEN_ALIASKEY index, ZEN_TRIGGER trigger);

		//! Create and add assignments in the active assigment set
	void setAssignmentsViaFile(ZEN_STRING fileLocation);

		/*!
		 * Remove all assignments that contain the provided alias/trigger in the active assignment set
		 * @param typeToDelete - True = alias key, False = trigger key
		 */ /*
	void clearAssignment(ZEN_UINT index, ZEN_KEYSELECTION typeToDelete);

		//! Lists each assignment, with each line being a different assignment in the active assigment set
	ZEN_STRING listAssignments();

		/*!
		 * Lists each assignment with the alias/trigger provided, with each index being on a seperate line from the active assigment set
		 * @param typeToDelete - True = alias key, False = trigger key
		 */ /*
	ZEN_STRING listAssignments(ZEN_UINT index, ZEN_KEYSELECTION typeToList);

		//! Add an alias for a game event, only unique aliases will be stored
	void addAlias(ZEN_ALIAS newAlias);

		//! Add a list of aliases for storage from a file
	void addAliasSetViaFile(ZEN_STRING fileLocation);

		//! Get an event alias at the provided index
	ZEN_ALIAS getAlias(ZEN_ALIASKEY index);

		//! Get the amount of aliases stored
	ZEN_UINT getAliasAmount();

		//! Checks if the index provided represents a stored alias
	ZEN_BOOL isAlias(ZEN_ALIASKEY index);


	/*
	 * The following is detail for the implementation to function.
	 * Note that these still require a wrapper for the conversion from raw data to the system trigger information.
	 * I.e. a wrapper for SDL2 or GLFW to convert it's inputs into trigger keys.
	 */ /*

		//! Stores and manages the program's input sets (aliases and assignment)
	InputSetManagement zenInputManagement;
}

#endif // ZENENGINE_INPUTSYSTEM_INPUT_SYSTEMINTERFACE_H_*/