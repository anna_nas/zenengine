//
//  RenderCommon.h
//  RenderingEngine
//
//  Created by Brianna Whitcher
//

#ifndef ZENENGINE_CORE_RENDERING_RENDERDETAIL_RENDERCOMMON_H_
#define ZENENGINE_CORE_RENDERING_RENDERDETAIL_RENDERCOMMON_H_

#define GLEW_STATIC
#include "glew.h"

#include "gem.h"
#include "glm.hpp"

#endif /* ZENENGINE_CORE_RENDERING_RENDERDETAIL_RENDERCOMMON_H_ */